#!/usr/bin/env python
# -*- coding: utf-8 -*-
# send file to printer bots like a human

import sys
from time import sleep
from telethon import TelegramClient, events, sync


# logger.add(sys.stderr, format="{time} {level} {message}", filter="telethon_client", level="INFO")
# logger.add("logs\\file_telethon_{time}.log")
# logger.add("logs\\file_telethon_X.log", retention="10 days")  # Cleanup after some time


def send_message_telethon(geo_staff, doc):
    try:
        # telethon
        api_id = 2895372
        api_hash = "a6311721eb3d2b25872337df774fd336"
        client_telethon = TelegramClient('session_name', api_id, api_hash)
        client_telethon.start()

    except Exception as e:
        raise e

    if geo_staff == 'kolomna':
        client_telethon.send_file('printer_kolomna_bot', doc)
    elif geo_staff == 'bronnicy':
        client_telethon.send_file('printer_bronnicy_bot', doc)
        client_telethon.send_file(247926132, doc)
    else:
        client_telethon.send_message('printer_bronnicy_bot', 'hello')
        # client_telethon.send_file('printer_bronnicy_bot', doc)
    return None


def main():
    print(sys.argv[1], sys.argv[2])
    try:
        print("printed doc")
        # send_message_telethon(sys.argv[1], sys.argv[2])
    except Exception as e:
        print(e)
    else:
        return True


if __name__ == '__main__':
    while not main():
        main()
