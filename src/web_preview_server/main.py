import os

from fastapi import FastAPI
from fastapi import Request
from fastapi.responses import FileResponse
from fastapi.templating import Jinja2Templates

app = FastAPI()
templates = Jinja2Templates(directory="templates")

# photo_path = os.environ.get('PHOTOS')
photo_path = "D:\\bot_manager_spektr_\\download\\orders\\photos"

ip = "http://77.51.210.22/"
ip_1 = "http://127.0.0.1/"


@app.get("/img/{file_path}")
async def file(file_path: str, request: Request):
    file_url = f"{ip}{file_path}"
    return templates.TemplateResponse("photo.html", {"request": request, "photo_path": file_url})


@app.get("/{file_path}")
async def telegram(file_path: str, request: Request):
    path = f"{photo_path}\\{file_path}"

    if os.path.exists(path):
        return FileResponse(path)

    else:
        return {"error": f"{photo_path}"}
