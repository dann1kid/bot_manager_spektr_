from User import User


class Administrator(User):
    """
    Класс админа.
    Наследует класс User
    """

    def __init__(self, name, client_id):
        User.__init__(self, name, 'administrator')
        self.client_id = client_id

    def __repr__(self):
        print(f'{self.client_id}, {self.name}, {self.status}')
