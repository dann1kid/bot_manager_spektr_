def handler_start(update, context):
    """
        /start
        """
    chat_id = update.message.chat_id
    try:
        client = Client.get(Client.chat_id == chat_id)
    except Exception as e:
        hello = Hello(update, context)
        hello.start()
    else:
        print("callled start")
        start_command(update, context)


command_handler = CommandHandler('start', handler_start,
                                 pass_args=True, )
