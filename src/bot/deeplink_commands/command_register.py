from src.bot.models.pw_models import Client, QR
from src.bot.resources.strings import txt_successful_registration, \
    txt_unsuccessful_registration
from src.bot.utils.message import send_message


def command_register(update, context):
    try:
        command_args = context.args
    except:
        raise
    match command_args[0]:
        case "register":
            # collect data
            command_args = command_args[0].split('-')
            client_id = command_args[1]
            order_id = command_args[2]
            # parse and save data
            client = Client.get_by_id(client_id)
            client.chat_id = update.message.chat_id
            client.save()
            qr_link = QR.get(QR.item_id == order_id)
            qr_link.expired = True
            qr_link.save()
            # answer after registration
            send_message(update, context, message=txt_successful_registration)
        case _:
            send_message(update, context, message=txt_unsuccessful_registration)

    return None
