from pprint import pprint

from peewee import DoesNotExist
from telegram import ReplyKeyboardRemove, ParseMode

from src.bot.Enumerators.Deeplink import DeeplinkEnum, Deeplink
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.MessageBuilder.OrderMessageBuilder import OrderMessageBuilder
from src.bot.entry_points.CallbackEntry import CallbackParser
from src.bot.hello import Hello
from src.bot.keyboards.TG_keyboards import hub_keyboard, make_reply_keyboard
from src.bot.models.pw_models import Client, Order, Detail, QR
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.message import send_message, notify_staff, send_message_to_target


def register_user_by_type(update, context, type_name: DeeplinkEnum = None,
                          type_id: str | int = None) -> None:
    """Регистрирует нового юзера."""

    print("type REGISTER", type_name, type_id)
    qr_link = QR.get(QR.type_id == type_id)

    if qr_link.expired:
        send_message(update, context, message=strings.err_expired_link)
        return

    match type_name:
        case DeeplinkEnum.register_user_order:
            item = Order.get(Order.item_id == type_id)
            pprint(item)

        case DeeplinkEnum.register_user_detail:
            item = Detail.get(Detail.item_id == type_id)
        case _:
            send_message(update, context, message=f"{strings.main_menu}",
                         reply=hub_keyboard(update.message.chat_id))
            return

    client = Client.get(Client.item_id == item.client.item_id)

    # если клиент новый и чат айди не зарегистрирован, то чат айди сохраняется
    if client.chat_id == 0:
        client.chat_id = update.message.chat_id
        client.save()

    # если клиент УЖЕ имеет другой чат ид и он не изменился на предыдущем шаге, то транзакция отменяется
    if client.chat_id != update.message.chat_id:
        send_message(update, context, message=strings.err_access_denied)
        return

    qr_link.update(expired=True).execute()
    item.client = client
    item.save()
    send_message(update, context, message=f"{strings.txt_order_transported_f.format(item.item_id)}",
                 reply=hub_keyboard(update.message.chat_id))

    return None


def transmit_order(order_id, client):
    Order.update(client=client).where(Order.item_id == order_id)


def transmit_detail(detail_id, client):
    Detail.update(client=client).where(Detail.item_id == detail_id)


@access_level_wrapper(staff_status=StaffLevel.receiver)
def extradite_order(update, context, deeplink: Deeplink = None) -> None:
    chat_id = update.message.chat_id
    order = Order.get(Order.item_id == deeplink.type_id)

    if order.status == StatusOrderMenu.fin.name:
        send_message(update, context, message=strings.err_already_extradited, reply=hub_keyboard(chat_id))

        return None

    order.status = StatusOrderMenu.sd.name
    order.save()
    builder = OrderMessageBuilder(order=order,
                                  users_chat_id=chat_id,
                                  mode=OrderMessageBuilder.BuilderMode.status)

    raw_data = CallbackParser.make_paginator_order(order_id=order.item_id,
                                                   status=order.status,
                                                   page=1,
                                                   mode=OrderMessageBuilder.BuilderMode.status.value)
    reply = builder.create_buttons_inline(raw_data)
    send_message(update, context, message=builder.create_message(), reply=reply, parse_mode=ParseMode.HTML, )

    return None


def get_deeplink(update, context) -> Deeplink | bool:
    deeplink = Deeplink()
    pprint(deeplink)
    pprint(context.args)

    if not deeplink.check_args(context.args):
        return False
    elif not deeplink.check_args_length(context.args):
        send_message(update, context,
                     message=strings.txt_err_deeplink_start,
                     reply=hub_keyboard(update.message.chat_id)
                     )

    deeplink.parse_link(list_args=context.args)
    return deeplink


def send_buttons(update, context):
    client = Client.get(Client.chat_id == update.message.chat_id)

    if client.banned:
        send_message(update, context, message=strings.err_banned,
                     reply=ReplyKeyboardRemove())

    elif client.staff_status >= StaffLevel.client:
        send_message(update, context,
                     message=strings.main_menu,
                     reply=hub_keyboard(client.chat_id))

    elif client.current_state == strings.new_client:
        send_message(update, context,
                     message=strings.txt_not_registered,
                     reply=make_reply_keyboard(
                         strings.btn_fulfill_profile_prompt))

    return None


# todo добавить враппер на проверку прав сюда
def start_command(update, context):
    chat_id = update.message.chat_id
    deeplink = get_deeplink(update, context)

    if deeplink is False:
        try:
            client = Client.get(Client.chat_id == chat_id)
        except DoesNotExist as e:
            hello = Hello(update, context)
            hello.start()
        else:
            send_buttons(update, context)
        return None
    try:
        client = Client.get(Client.chat_id == chat_id)
    except DoesNotExist:
        client = None
    else:
        if client.banned:
            send_message(update, context, message=strings.err_banned,
                         reply=ReplyKeyboardRemove())

    match deeplink.enum_type:

        # выдача приемщиком по QR
        case DeeplinkEnum.extradite_order.value:
            extradite_order(update, context, deeplink=deeplink)

        # регистрация через заказы с передачей
        case DeeplinkEnum.register_user_order.value:
            if client:
                transmit_order(order_id=deeplink.type_id, client=client)
            else:
                register_user_by_type(update, context,
                                      type_name=DeeplinkEnum(deeplink.enum_type),
                                      type_id=deeplink.type_id)

        # регистрация через товары с передачей
        case DeeplinkEnum.register_user_detail.value:
            if client:
                transmit_detail(detail_id=deeplink.type_id, client=client)
            else:
                register_user_by_type(update, context,
                                      type_name=DeeplinkEnum(deeplink.enum_type),
                                      type_id=deeplink.type_id)

        # передаче заказа курьеру с мастерской
        case DeeplinkEnum.transmit_order_from_store_courier.value:

            if client.staff_status != StaffLevel.courier.value:
                send_message(update, context, message=strings.err_access_denied, reply=hub_keyboard(chat_id))

            else:
                order = Order.get(Order.item_id == deeplink.type_id)
                if order.status != StatusOrderMenu.dwc.name:
                    send_message(update, context, message=strings.err_access_denied, reply=hub_keyboard(chat_id))

                else:
                    order.status = StatusOrderMenu.dc.name
                    order.courier = client
                    order.save()
                    send_message(update, context, message=strings.note_order_transmitted_to_you, reply=hub_keyboard(chat_id))

        # передаче заказа курьеру от клиента
        case DeeplinkEnum.transmit_order_from_client_courier.value:
            if client.staff_status != StaffLevel.courier.value:
                send_message(update, context, message=strings.err_access_denied, reply=hub_keyboard(chat_id))

            else:
                order = Order.get(Order.item_id == deeplink.type_id)
                if order.status != StatusOrderMenu.wc.name:
                    send_message(update, context, message=strings.err_access_denied, reply=hub_keyboard(chat_id))

                elif order.courier != client:
                    send_message(update, context, message=strings.err_access_denied, reply=hub_keyboard(chat_id))

                else:
                    order.status = StatusOrderMenu.odw.name
                    order.save()
                    send_message(update, context, message=strings.note_order_transmitted_to_you, reply=hub_keyboard(chat_id))

        # передаче заказа мастерской от курьера
        case DeeplinkEnum.transmit_order_from_courier_store.value:
            if client.staff_status != StaffLevel.receiver.value:
                send_message(update, context, message=f"{strings.err_access_denied}. {strings.err_you_not_receiver} ",
                             reply=hub_keyboard(chat_id))

            else:
                order = Order.get(Order.item_id == deeplink.type_id)
                if order.status != StatusOrderMenu.odw.name:
                    send_message(update, context, message=strings.err_access_denied, reply=hub_keyboard(chat_id))

                else:
                    order.status = StatusOrderMenu.iw.name
                    order.save()
                    send_message(update, context, message=strings.txt_order_transmitted_to_store,
                                 reply=hub_keyboard(chat_id))
                    notify_staff(context, item=order, staff_level=StaffLevel.receiver,
                                 message=strings.note_new_order_in_work)

        # передаче заказа клиенту от курьера
        case DeeplinkEnum.transmit_order_from_courier_to_client.value:
            if client.staff_status != StaffLevel.courier.value:
                send_message(update, context, message=strings.err_access_denied,
                             reply=hub_keyboard(chat_id))

            else:
                order = Order.get(Order.item_id == deeplink.type_id)
                if order.status != StatusOrderMenu.dc.name:
                    send_message(update, context, message=strings.err_access_denied,
                                 reply=hub_keyboard(chat_id))

                elif order.courier != client:
                    send_message(update, context, message=strings.err_access_denied,
                                 reply=hub_keyboard(chat_id))

                else:
                    order.status = StatusOrderMenu.wp.name
                    order.save()

                    builder = OrderMessageBuilder(order=order,
                                                  users_chat_id=chat_id,
                                                  mode=OrderMessageBuilder.BuilderMode.status)

                    raw_data = CallbackParser.make_paginator_order(order_id=order.item_id,
                                                                   status=order.status,
                                                                   page=1,
                                                                   mode=OrderMessageBuilder.BuilderMode.status.value)

                    send_message(update, context,
                                 message=f"{strings.note_courier_payment}",)
                    send_message(update, context,
                                 message=builder.create_message(),
                                 reply=builder.create_buttons_inline(raw_data))

        case _:
            send_message(update, context, message=strings.prompt_unknown_args)

    return None
