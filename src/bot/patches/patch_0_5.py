#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src import create_table
import sys
sys.path.append('../..')

if __name__ == '__main__':
    table = "media_pool"
    columns = ["media_id INTEGER UNIQUE", "media_name TEXT ", "file_id TEXT"]
    options = 'CONSTRAINT "media_pool_pk" PRIMARY KEY("media_id" AUTOINCREMENT)'

    try:
        create_table(new_table_name=table, columns=columns, options=options)
    except Exception as e:
        print("Ошибка создания", e)
        pass
    else:
        print("Таблица создана")
