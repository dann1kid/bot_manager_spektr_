#!/usr/bin/env python
# -*- coding: utf-8 -*-
from time import sleep
import sys
sys.path.append('../..')
from src import create_fts, rename_table, get_columns


if __name__ == '__main__':

    table = "orders"
    renamed_table = "orders_old"

    # переименовываем старую таблицу
    try:
        rename_table(table, renamed_table)
        print("Переименовано")
    except Exception as e:
        print("Ошибка переименования", e)
        pass
    sleep(2)
    # создаем фтс на месте старой
    columns = get_columns(table=renamed_table)
    try:
        create_fts(new_table_name=table, columns=columns)
        print("Таблица создана")
    except Exception as e:
        print("Таблица не создана")
        raise e
    sleep(2)
    # копируем данные
    """
    try:
        copy_data_table_to_table(renamed_table, table)
        print("Данные скопированы")
    except Exception as e:
        print("Данные не скопированы", e)
        pass
    """
