#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

sys.path.append('../..')
from src import create_fts

if __name__ == '__main__':
    price_table = "repair_price"
    columns = ["repair_p"
               "rice_id", "repair_price_name", "repair_price_current"]

    try:
        create_fts(new_table_name=price_table, columns=columns)
    except Exception as e:
        print("Ошибка переименования", e)
        pass
    else:
        print("Таблица создана")
