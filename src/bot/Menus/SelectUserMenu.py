from dataclasses import dataclass, field

from TelegramApiClient import Keyboard
from peewee import fn
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from src.bot.entry_points.CallbackEntry import CallbackParser
from src.bot.models.pw_models import Client, ContactClient, Order, Detail
from src.bot.resources import strings


@dataclass
class SelectUserMenu:
    """
    Prototype
    todo: приделать ограничение инлайн кнопок либо приделать пагинацию
    return menu with users named buttons as callback keyboard

    Use: keyboard_menu = SelectUserMenu(formatted_number=PhoneNumber.formatted_number).get_menu()

    """
    formatted_number: str = None
    clients: [Client] = field(default=None)
    buttons: list = field(default_factory=list)
    keyboard: Keyboard = None
    sum_orders_client: int = 0
    sum_payments_client: int = 0

    def get_clients(self):
        """Fulfills clients field with Client objects"""
        # todo пометить дублированных клиентов как забаненных
        self.clients = Client.select().where(Client.banned == False).join(
            ContactClient).distinct().where(
            ContactClient.number == self.formatted_number)

    def make_keyboard(self):
        """Generates inline keyboard with callbacks"""
        for client in self.clients:
            callback = CallbackParser()
            callback = callback.make_userprofile_callback(
                client_id=client.item_id)
            self.count_orders_client(client)
            self.count_payments_client(client)
            self.buttons.append([
                InlineKeyboardButton(
                    text=f'{client.name}\n {strings.txt_orders} '
                         f'{self.sum_orders_client}\n'
                         f'{strings.txt_summary_payment}\n'
                         f'{self.sum_payments_client}',
                    callback_data=callback)])

        self.keyboard = InlineKeyboardMarkup(self.buttons)

    def count_orders_client(self, client):
        """get sum of all orders for one client"""
        self.sum_orders_client = client.orders.count() + client.clients_details.count()

    def count_payments_client(self, client):
        """get sum of payments of all repairs and ordered goods"""
        payments_all_repairs = Order.select(Order.payment).where(
            Order.client == client)
        payments_all_goods = Detail.select(Detail.payment).where(
            Detail.client == client)
        for i in payments_all_repairs:
            self.sum_payments_client += i.payment
        for i in payments_all_goods:
            self.sum_payments_client += i.payment

    def get_menu(self):
        """returns a Keyboard Object"""
        self.get_clients()
        self.make_keyboard()

        return self.keyboard
