# Telegram Questions and answer handler
# test_send.py (inherit)
import random
import re
from time import time

import telegram
from telegram import InlineKeyboardMarkup, error
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater, CallbackQueryHandler
# from vk_api import vk_api
# from vk_api.bot_longpoll import VkBotLongPoll

# from TGUser import TGUser
# TG keyboards
from src.bot.keyboards import reply_kbd


# from logging_bot import logging_exception
# from settings import FUNNY_PICS_FILE_IDS
# from settings import VK_TOKEN, GROUP_ID, TG_TEST_USER_ID
# from users_manager import get_vk_users, get_tg_users, sync_tg_users


def def_bot(proxy, token):
    """make class with bot
    :param proxy: get a sokcs5 proxy (default is tor proxy)
    :param token: telegram bot token
    """
    request_kwargs = telegram.utils.request.Request(
        proxy_url=f'socks5://{proxy}')
    bot = telegram.Bot(token, request=request_kwargs)

    return bot


# override with buttons
def send_question_admin(tg_user, user_vk, user_name, message, bot):
    """
    Отсылает текст (с вк) к юзеру в тг
    :param tg_user: кому (тг)
    :param user_vk: от кого (вк)
    :param user_name: имя для идентификации кнопки
    :param message: текст
    :param bot: обьект класса telegram.bot
    :return: None
    """
    # define inset to message from user to admins
    keyboard = reply_kbd(user_name, user_vk)
    reply_markup = InlineKeyboardMarkup(keyboard)
    random_funny_pic = take_random_pic()
    # end define inset

    # try send pic
    try:
        bot.send_photo(chat_id=tg_user,
                       photo=random_funny_pic,
                       )
    except Exception as e:
        print(f'{e}')
        logging_exception(f'{e}')

    # send message
    finally:
        bot.send_message(chat_id=tg_user,
                         text=message,
                         reply_markup=reply_markup,
                         )

    return None


def take_random_pic():
    max_pics = len(FUNNY_PICS_FILE_IDS)
    random_num = random.randint(0, max_pics - 1)
    random_pic = FUNNY_PICS_FILE_IDS[random_num]

    return random_pic


#######################################################################################################
# connect
#######################################################################################################


def conn(socks5_server, token):
    request_kwargs = {
        'proxy_url': f'socks5://{socks5_server}/',
        # Optional, if you need authentication:
        'urllib3_proxy_kwargs':
            {
                'assert_hostname': 'False',
                'cert_reqs': 'CERT_NONE'
                # 'username': 'user',
                # 'password': 'password',
            }
    }

    # removed request_kwargs=request_kwargs,
    updater = Updater(token=token,
                      use_context=True,

                      )

    print(socks5_server)

    dispatcher = updater.dispatcher

    dispatcher.add_error_handler(error)

    return dispatcher, updater


def load_handlers(socks5_server, token):
    # conn
    dispatcher, updater = conn(socks5_server, token)

    # add handler service block
    echo_handler = MessageHandler(Filters.text, echo)
    dispatcher.add_handler(echo_handler)

    # add buttons with callback via handlers file
    add_button = CallbackQueryHandler(callback_answer_tree)
    dispatcher.add_handler(add_button)

    return dispatcher, updater


#######################################################################################################
# handlers
#######################################################################################################
# noinspection PyUnusedLocal
def callback_answer_tree(update, context):
    try:
        query = update.callback_query
        answering_user_id = query.message.chat.id
        answering_user_id = int(answering_user_id)
        tg_users = get_tg_users()

        # in future make search in query!
        user_info = query.order

        user_id = re.search(r'^.*\s', user_info).group(0)
        user_id = int(user_id)
        user_name = re.search(r'\s.*$', user_info).group(0)

        # if user selected get all user
        vk_users = get_vk_users()
        print(vk_users)

        # just show short toast with selected user
        query.answer(
            text=f"Выбран пользователь: {user_name}, напишите ему ответ!"
        )

        # create obj class with param user_id (vk target)
        tg_user = TGUser(
            user_id=user_id,
            user_name=user_name
        )
        tg_users[answering_user_id] = tg_user
        sync_tg_users(tg_users)

    except Exception as e:
        print(f'{e}')
        logging_exception(f'{e}')

    return None


# echo for any message
def echo(update, context):
    # message from
    answering_user_id = update.message.chat.id
    answering_user_name = update.message.chat.username
    print(answering_user_id)
    # load dict of competent users
    tg_users = get_tg_users()
    print(tg_users)

    if answering_user_id in TG_TEST_USER_ID:
        if tg_users[answering_user_id].user_id is None:
            print('lol')
            context.bot.send_message(
                chat_id=update.message.chat_id,
                text=f"Вы никого не выбрали для ответа!",
            )

        elif tg_users[answering_user_id].user_id is not None:
            print('kek')
            vk_user_id = tg_users[answering_user_id].user_id
            vk_user_name = tg_users[answering_user_id].user_name
            # text from admin
            text = update.message.text
            # send by vk method (uses exist object in server manager)
            try:
                send_msg_vk(send_id=int(vk_user_id),
                            message=f"Ответ от администратора: \n"
                                    f"{text}")
            except Exception as e:
                raise e
            # searching how get and set vk username
            # обнулим)
            tg_users[answering_user_id].user_id = None
            sync_tg_users(tg_users)
            # шлем весточку)
            try:
                context.bot.send_message(
                    chat_id=answering_user_id,
                    text=f"Сообщение отправлено {vk_user_name}!",
                )
            except Exception as e:
                raise e

    else:
        context.bot.send_message(
            chat_id=answering_user_id,
            text=f"{answering_user_name}, "
        )
        print("echo tree answer")
        # место под апдейты

    return None


# Define variables for VK messaging


vk = vk_api.VkApi(token=VK_TOKEN)
vk_api = vk.get_api()
long_poll = VkBotLongPoll(vk, GROUP_ID)


# Define VK functions


def send_msg_vk(send_id, message, keyboard=None):
    """
    Отправка сообщений через метод messages.send c клавиатурой
    :param send_id: vk id пользователя
    :param message: содержимое письма
    :param keyboard: строка с путем к файлу с клавой (json)
    клава для меседжа (например, по дефолту - отправка заявки и ТП,
    но в некоторые моменты требуется однозначный ответ (да или нет),
    или вовсе не требуется

    :raise None: typo into cmd

    :return: None
    """
    # random ints for VK int random id (read about VK send message)
    random_int_0 = random.randint(0, 9999)
    random_int_1 = random.randint(0, 9999)
    random_int_3 = random.randint(0, 9999)
    random_id = int(time())

    # Разделим просто сообщение, там где не нужна клавиатура
    if keyboard is None:
        try:
            vk_api.messages.send(peer_id=send_id,
                                 message=message,
                                 random_id=f'{random_id}{random_int_0}{random_int_1}{random_int_3}',
                                 )
        except Exception as e:
            print(f'{e}')

    return None
