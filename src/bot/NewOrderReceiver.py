#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import subprocess
from datetime import datetime

from docxtpl import DocxTemplate
# from loguru import logger
from telegram import KeyboardButton
from telegram import ReplyKeyboardMarkup
from telegram import ReplyKeyboardRemove
from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telethon import TelegramClient

from src.bot.keyboards import TG_keyboards
from src.rights.checking import access_level
from config_bot import DATABASE_TABLE_ADDRESSES_NAME as table_addresses
from config_bot import DATABASE_TABLE_CLIENTS_NAME as table_clients
from config_bot import DATABASE_TABLE_ORDERS_NAME as table_orders
from config_bot import DB_NAME as db

# conv_handler constants
from src.resources import alert_return_main_menu

entry_phrase = "Добавить заказ"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

# entry fun
STATUS_SELECT_TYPE = "type_question"

# conv_handler states
CURRENT_STATUS_CHECK_DELIVERY = "delivery_question"
CURRENT_STATUS_ASK_BRAND = "ask_brand"
CURRENT_STATUS_ANSWER_BRAND = "answer_brand"
CURRENT_STATUS_MODEL = "model"
CURRENT_STATUS_PROBLEM = "problem"
CURRENT_STATUS_WAIT_COURIER = "await_courier"
CURRENT_STATUS_PHONE_NUMBER = 'phone_number'
STATUS_AWAIT_NUMBER = "await_number"
STATUS_CHECKING_NUMBER = "check_number"

# name
CURRENT_STATUS_NAME = "name"
CURRENT_STATUS_ASK_NAME = "ask_name"
CURRENT_STATUS_SURNAME = "surname"
CURRENT_STATUS_ASK_SURNAME = "ask_surname"
CURRENT_STATUS_MIDDLE_NAME = "middle_name"

# imei
CURRENT_STATUS_IMEI = "imei"

# comment
CURRENT_STATUS_COMMENT = "comment"

# price
CURRENT_STATUS_PRICE = 'price'

# prepayment
CURRENT_STATUS_PREPAYMENT = 'prepayment'

# courier branch states
CURRENT_STATUS_AWAIT_COORDS = "coordinates_await"  # add recheck on empty
CURRENT_STATUS_CHECK_COORDS = "coordinates_check"
CURRENT_STATUS_CHECK_ANOTHER_ADDRESS = "address_return_check"  # if no get another address (yes no answer awaiting)
CURRENT_STATUS_CHECK_COORDS_2ND = "coordinates_check_2ND"
CURRENT_STATUS_SUCCESSFUL_END = 'end_ok'
CURRENT_STATUS_UNSUCCESSFUL_END = 'end'

# detail branch
CURRENT_STATUS_DETAIL_NUMBER = "detail_number"
CURRENT_STATUS_DETAIL_CLIENT_NAME = "detail_client_name"
CURRENT_STATUS_DETAIL_NAME = "detail_name"
CURRENT_STATUS_DETAIL_COMMENTARY = "detail_commentary"
CURRENT_STATUS_DETAIL_PREPAYMENT = "detail_prepayment"

# user control
STATUSES_CHECK = [CURRENT_STATUS_CHECK_DELIVERY,
                  CURRENT_STATUS_ASK_BRAND,
                  CURRENT_STATUS_ANSWER_BRAND,
                  CURRENT_STATUS_MODEL,
                  CURRENT_STATUS_PROBLEM,
                  CURRENT_STATUS_WAIT_COURIER,
                  CURRENT_STATUS_PHONE_NUMBER,
                  CURRENT_STATUS_NAME,
                  CURRENT_STATUS_ASK_NAME,
                  CURRENT_STATUS_ASK_SURNAME,
                  CURRENT_STATUS_SURNAME,
                  CURRENT_STATUS_MIDDLE_NAME,
                  CURRENT_STATUS_IMEI,
                  CURRENT_STATUS_COMMENT,
                  CURRENT_STATUS_PRICE,
                  CURRENT_STATUS_PREPAYMENT,
                  CURRENT_STATUS_AWAIT_COORDS,
                  CURRENT_STATUS_CHECK_COORDS,
                  CURRENT_STATUS_CHECK_ANOTHER_ADDRESS,
                  CURRENT_STATUS_CHECK_COORDS_2ND,
                  ]

# conversation_handler presets
answer_yes = Filters.regex("^Да$")
answer_no = Filters.regex("^Нет$")


#
# logger.add(sys.stderr, format="{time} {level} {message}", filter="telethon_client", level="INFO")
# logger.add("logs\\file_telethon_{time}.log")
# logger.add("logs\\file_telethon_X.log", retention="10 days")  # Cleanup after some time


# @logger.catch
def send_message_telethon(geo_staff, doc):
    try:
        # telethon
        api_id = 2895372
        api_hash = "a6311721eb3d2b25872337df774fd336"
        client_telethon = TelegramClient('session_name', api_id, api_hash)
        client_telethon.start()

    except Exception as e:
        raise e

    if geo_staff == 'colomna':
        client_telethon.send_file('printer_kolomna_bot', doc)
    elif geo_staff == 'bronnicy':
        client_telethon.send_file('printer_bronnicy_bot', doc)
    else:
        client_telethon.send_message('printer_bronnicy_bot', 'hello')
        client_telethon.send_file('printer_bronnicy_bot', doc)
    return None


# funcs
def cancel(update, context, order_id):
    """
        Текущий заказ помечается как завершенный неоконченный.

    :param order_id:
    :param context:
    :type update: object
    """
    chat_id = update.message.chat_id
    update.message.reply_text(
        alert_return_main_menu,
        reply_markup=hub_keyboard(chat_id))

    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    current_order = get_clients_current_order(db, table_orders, client_id)

    save_current_status_cancel(
        order_id=current_order,
        status=CURRENT_STATUS_UNSUCCESSFUL_END,
    )

    return ConversationHandler.END


def delete_unfinished_orders():
    pass


def send_message(update, context, message, reply=None,
                 resize_keyboard=True) -> None:
    """context based send message for any reason
    :param resize_keyboard:
    :type update: object
    :param context:
    :param message:
    :param reply:

    :return None
    """
    if reply == 'remove':
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=ReplyKeyboardRemove(),
            resize_keyboard=resize_keyboard)
    elif reply is None:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
        )
    else:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=reply,
        )

    return None


def search_num(text: str):
    text = text.replace(" ", "")
    return re.search('[\d]{11,12}|'
                     '\+7[\d]{10,13}|'
                     '8 [\d]{3} [\d]{3} [\d]{2} [\d]{2}|'
                     '\+7 [\d]{3} [\d]{3} [\d]{2} [\d]{2}|'
                     '8-[\d]{3}-[\d]{3}-[\d]{2}-[\d]{2}|'
                     '\+7-[\d]{3}-[\d]{3}-[\d]{2}-[\d]{2}|'
                     '[\d]{10}', text)  # СОРЯН РЕГУЛЯР ОЧКА


def format_num(num: str) -> str:
    """

    :type num: object
    """

    # регексп проверяет и пускает только полноценные номера
    if num[0] == "+":
        index = num[2:5]
        prefix = num[5:8]
        num_1 = num[8:10]
        num_2 = num[10:12]
    elif len(num) == 11:
        index = num[1:4]
        prefix = num[4:7]
        num_1 = num[7:9]
        num_2 = num[9:11]
    elif len(num) == 10:
        index = num[0:3]
        prefix = num[3:6]
        num_1 = num[6:8]
        num_2 = num[8:10]

    else:
        return ''  # бля, НА ВСЯКИЙ СЛУЧАЙ

    return f"+7({index}){prefix}-{num_1}-{num_2}"


def print_document(document_path: str) -> None:
    """
        печатает на принтере по умолчанию документ с именем document
    :type document_path: str
    """

    os.startfile(document_path, "print")

    return None


# questions

# credentials
def fun_on_entry(update, context):
    """
        Входная функция выбор

    :param update:
    :param context:
    :return:
    """

    buttons = [
        "Запчасть",
        "Ремонт",
        "Отмена",
    ]
    # проверка на доступ!
    if access_level(update.message.chat_id):
        reply_keyboard = make_reply_keyboard(buttons)
        send_message(update, context, message='ТИП заказа',
                     reply=reply_keyboard)
    else:
        send_message(update, context, message='Неправильная команда')
        return ConversationHandler.END

    return STATUS_SELECT_TYPE


def fun_selected_branch(update, context):
    """
        Проверяет сделанный выбор, и на его основе
        отправляет сообщение с запросом
        на необходимое содержимое от юзера

    :param update:
    :param context:
    :return:
    """
    selected_branch = update.message.text
    chat_id = update.message.chat_id

    if selected_branch.lower() == "запчасть":
        buttons = [
            "Спектр",
            "Отмена",
        ]
        reply_keyboard = make_reply_keyboard(buttons)
        send_message(update, context,
                     message='Номер телефона клиента для связи',
                     reply=reply_keyboard)
        return CURRENT_STATUS_DETAIL_NUMBER

    elif selected_branch.lower() == "ремонт":
        buttons = [
            "Отмена",
        ]
        reply_keyboard = make_reply_keyboard(buttons)
        send_message(update, context,
                     message='Номер телефона клиента для связи',
                     reply=reply_keyboard)
        return STATUS_AWAIT_NUMBER

    elif selected_branch.lower() == "отмена":
        kbd = hub_keyboard(chat_id)
        send_message(update, context, message="Создание заказа отменено",
                     reply=kbd)
        return ConversationHandler.END

    else:
        send_message(update, context, message='Укажите тип заказа')
        return STATUS_SELECT_TYPE


def fun_detail_number(update, context):
    """
        Сохраняет номер в таблице для деталей

    :param update:
    :param context:
    :return:
    """
    number = update.message.text.lower()  # update text what can contains the contact
    chat_id = update.message.chat_id
    # Отсеиваем ошибочные или сторонние состояния
    # на случай если ошибся
    if number == "отмена":
        kbd = hub_keyboard(chat_id)
        send_message(update, context, message="Создание заказа отменено",
                     reply=kbd)
        return ConversationHandler.END
    #
    # для внутреннего пользования
    elif number == "спектр":

        contact_number = "+7(929)905-51-05"

        # получаем идентификатор клиента
        client_id = get_client_id(db, table_clients,
                                  chat_id=update.message.chat_id)

        # cохраняем деталь
        save_phone_number_detail_receiver(client_id=client_id,
                                          phone_number=f"{contact_number}",
                                          status=CURRENT_STATUS_DETAIL_CLIENT_NAME,
                                          )

        # получаем идентификатор детали
        detail_id = get_current_detail_id(client_id=client_id,
                                          status=CURRENT_STATUS_DETAIL_CLIENT_NAME)

        # сохраняем имя
        name = "Спектр"
        save_client_name_detail_receiver(name=name, detail_id=detail_id,
                                         status=CURRENT_STATUS_DETAIL_NAME)

        # отрпавляем сообщение для пользователя
        send_message(update, context,
                     message="Наименование детали и ее параметры",
                     reply="remove")

        # пропускаем ветки с именем и номером
        return CURRENT_STATUS_DETAIL_NAME

    # оповещаем пользователя что информация принята
    update.message.reply_text('Принято. Проверяю информацию...',
                              reply_markup=ReplyKeyboardRemove()
                              )

    number = search_num(number)
    if number is None:
        update.message.reply_text('Не видно цифр, попробуйте еще раз 😊',
                                  reply_markup=ReplyKeyboardRemove())
        return CURRENT_STATUS_DETAIL_NUMBER

    else:
        number = number.group(0)
    formatted_number = format_num(number)

    # сохраняем номер в таблице для деталей
    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    save_phone_number_detail_receiver(client_id=client_id,
                                      phone_number=formatted_number,
                                      status=CURRENT_STATUS_DETAIL_NAME,
                                      )
    send_message(update, context, message="Имя клиента", reply='remove')

    return CURRENT_STATUS_DETAIL_CLIENT_NAME


def fun_get_client_name_detail(update, context):
    """ Сохраняет имя клиента и запрашивает название детали
        Ветка детали
    :param: update: обьект обновления
    :param: context: обьект контекста

    """
    name = update.message.text
    # получаем client_id
    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)
    detail_id = get_current_detail_id(client_id=client_id,
                                      status=CURRENT_STATUS_DETAIL_NAME)
    # сохраняем имя
    save_client_name_detail_receiver(name=name, detail_id=detail_id,
                                     status=CURRENT_STATUS_DETAIL_NAME)

    # задаем вопрос для следующего состояния диалога
    send_message(update, context, message="Наименование детали и ее параметры")

    return CURRENT_STATUS_DETAIL_NAME


def fun_get_detail_name_detail(update, context):
    """ Сохраняет название детали и запрашивает комментарий
        Ветка детали
    """
    detail_name = update.message.text
    client_id = get_client_id(db,
                              table_clients,
                              chat_id=update.message.chat_id,
                              )
    current_detail_id = get_current_detail_id(client_id=client_id,
                                              status=CURRENT_STATUS_DETAIL_NAME)

    save_detail_name_receiver(detail_id=current_detail_id,
                              detail_name=detail_name,
                              status=CURRENT_STATUS_DETAIL_COMMENTARY,
                              )
    buttons = [
        "Пропустить",
    ]

    reply_keyboard = make_reply_keyboard(buttons)
    send_message(update, context, message="Замечания и цена",
                 reply=reply_keyboard)

    return CURRENT_STATUS_DETAIL_COMMENTARY


def fun_get_commentary(update, context):
    """ Сохраняет комментарий и запрашивает предоплату
            Ветка детали
        """
    detail_commentary = update.message.text
    if detail_commentary.lower() == "пропустить":
        detail_commentary = "Замечания отсуствуют"

    client_id = get_client_id(db,
                              table_clients,
                              chat_id=update.message.chat_id,
                              )
    current_detail_id = get_current_detail_id(client_id=client_id,
                                              status=CURRENT_STATUS_DETAIL_COMMENTARY)

    save_detail_commentary_receiver(detail_id=current_detail_id,
                                    detail_commentary=detail_commentary,
                                    status=CURRENT_STATUS_DETAIL_PREPAYMENT)
    send_message(update, context, message="Предоплата")

    return CURRENT_STATUS_DETAIL_PREPAYMENT


def fun_get_detail_prepayment(update, context):
    chat_id = update.message.chat_id
    prepayment_text = update.message.text
    client_id = get_client_id(db,
                              table_clients,
                              chat_id=chat_id,
                              )
    current_detail_id = get_current_detail_id(client_id=client_id,
                                              status=CURRENT_STATUS_DETAIL_PREPAYMENT)

    save_detail_prepayment_receiver(detail_id=current_detail_id,
                                    prepayment_text=prepayment_text,
                                    status=CURRENT_STATUS_SUCCESSFUL_END)
    send_message(update, context,
                 message=f"Запись детали завершена.",
                 reply=hub_keyboard(chat_id)
                 )

    return ConversationHandler.END


def fun_get_number(update, context):
    number = update.message.text.lower()  # update text what can contains the contact
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id=chat_id)

    # remove unfinished order

    unfinished_order = get_unfinished_order(statuses=STATUSES_CHECK,
                                            client_id=client_id)
    flush_unfinished_order(order_id=unfinished_order)
    # на случай если ошибся
    if number == "отмена":
        kbd = hub_keyboard(chat_id)
        send_message(update, context, message="Создание заказа отменено",
                     reply=kbd)
        return ConversationHandler.END

    # оповещаем пользователя что информация принята
    update.message.reply_text('Принято. Проверяю информацию...',
                              reply_markup=ReplyKeyboardRemove()
                              )

    number = search_num(number)
    if number is None:
        update.message.reply_text('Не видно цифр, попробуйте еще раз 😊',
                                  reply_markup=ReplyKeyboardRemove())
        return STATUS_AWAIT_NUMBER

    else:
        number = number.group(0)

    formatted_number = format_num(number)
    # save formatted phone
    save_phone_number_receiver(client_id=client_id,
                               phone_number=formatted_number,
                               status=CURRENT_STATUS_NAME
                               )
    send_message(update, context, message="Отправьте имя клиента")
    return CURRENT_STATUS_NAME


def fun_get_name(update, context):
    """Запрашивает имя"""

    name = update.message.text.capitalize()
    # получаем client_id
    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    # получаем order_id
    current_order_id = get_clients_current_order(db, table_orders, client_id,
                                                 CURRENT_STATUS_NAME)

    # сохраняем имя
    save_name_receiver(name=name, status=CURRENT_STATUS_SURNAME,
                       order_id=current_order_id)

    # задаем вопрос для следующего состояния диалога
    send_message(update, context, message="Фамилия?")
    return CURRENT_STATUS_SURNAME


def fun_get_surname(update, context):
    """Получает и сохраняет фамилию"""

    # текст сообщения
    surname = update.message.text.capitalize()

    # получаем client_id
    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    # новый номер заказа, который он оформляет
    current_order_id = get_clients_current_order(db, table_orders, client_id,
                                                 CURRENT_STATUS_SURNAME)

    # сохраняем данные
    save_surname(current_order=current_order_id, surname=surname,
                 status=CURRENT_STATUS_MIDDLE_NAME)

    return ask_middle_name(update, context)


def ask_middle_name(update, context):
    # задаем вопрос для следующего состояния диалога
    buttons = [
        "Пропустить",
    ]
    reply_keyboard = make_reply_keyboard(buttons)
    send_message(update, context, message="Отчество", reply=reply_keyboard)

    return CURRENT_STATUS_MIDDLE_NAME


def fun_get_middle_name(update, context):
    """
        Получает и сохраняет Отчество
    :param update:
    :param context:
    :return:
    """

    # текст сообщения
    middle_name = update.message.text.capitalize()
    if middle_name.lower() == "пропустить":
        middle_name = ""

    # получаем клиент_ид приемщика
    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    # текущий заказ, который он оформляет
    current_order_id = get_clients_current_order(db, table_orders, client_id,
                                                 CURRENT_STATUS_MIDDLE_NAME)

    # сохраняем данные
    save_middle_name(current_order=current_order_id,
                     middle_name=middle_name.capitalize(),
                     status=CURRENT_STATUS_ASK_BRAND)

    return ask_for_brand(update, context)


def ask_for_brand(update, context):
    """
    Отправляет вопрос о производителе
    Args:
        update:
        context:

    Returns:
    Следующее состояние
    """
    # получаем клиент_ид приемщика
    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    # текущий заказ, который он оформляет
    current_order_id = get_clients_current_order(db, table_orders, client_id,
                                                 CURRENT_STATUS_ASK_BRAND)

    # задаем вопрос для следующего состояния диалога
    buttons = [
        "Назад",
        "Неизвестно",
        "Acer",
        "Apple",
        "Asus",
        "HP",
        "Huawei",
        "Lenovo",
        "Philips",
        "OPPO",
        "Samsung",
        "Vivo",
        "Xiaomi",
    ]
    reply_keyboard = make_reply_keyboard(buttons)
    send_message(update, context,
                 message="Бренд (производитель) устройства 🤔🤔🤔:",
                 reply=reply_keyboard,
                 resize_keyboard=False)
    # сохраняем данные
    save_current_status(client_id=client_id, order_id=current_order_id,
                        current_status=CURRENT_STATUS_ANSWER_BRAND,
                        previous_status=CURRENT_STATUS_ASK_BRAND,
                        )

    return CURRENT_STATUS_ANSWER_BRAND


def fun_first_question_data_phone(update, context) -> str:
    """Опрос клиента об аппарате """
    print('1 question receiver')
    chat_id = update.message.chat_id
    brand = update.message.text
    client_id = get_client_id(db, table_clients, chat_id=chat_id)
    current_order_id = get_clients_current_order(db, table_orders, client_id,
                                                 status=CURRENT_STATUS_ANSWER_BRAND)

    if brand.lower() == "назад":
        save_current_status(client_id=client_id, order_id=current_order_id,
                            current_status=CURRENT_STATUS_MIDDLE_NAME,
                            previous_status=CURRENT_STATUS_ANSWER_BRAND,
                            )
        return ask_middle_name(update, context)

    save_order_1st_question_receiver(brand=brand,
                                     order_id=current_order_id,
                                     status=CURRENT_STATUS_MODEL,
                                     )
    return ask_model(update, context)


def ask_model(update, context):
    buttons = [
        'Назад',
    ]
    reply_keyboard = make_reply_keyboard(buttons)
    send_message(update, context, message='Напиши модель аппарата 😏',
                 reply=reply_keyboard)

    return CURRENT_STATUS_MODEL  # возврат на этот же стейт, для уверенного ввода пользователя


def fun_second_question_data_phone(update, context) -> str:
    """
        принимает данные с второго ответа
        возвращает след стейт диалога
        так, как  эта функция должна вызыватся из фильтра текста,
        соотвественно проверок на None не надо


    :param update:
    :param context:
    :return status: next point of dialogue
    """
    print('2 question')

    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id,
                                              CURRENT_STATUS_MODEL)
    model_name = update.message.text

    if model_name.lower() == "назад":
        client_id = get_client_id(db, table_clients,
                                  chat_id=update.message.chat_id)
        save_current_status(client_id=client_id, order_id=current_order,
                            current_status=CURRENT_STATUS_ASK_BRAND,
                            previous_status=CURRENT_STATUS_MODEL)
        return ask_for_brand(update, context)

    send_message(update, context, message='Принято, записываю 😎')

    save_order_2nd_question_receiver(
        order_id=current_order,
        model_name=model_name,
        current_status=CURRENT_STATUS_PROBLEM,
    )
    # отправляем подсказки для следующего шага
    buttons = [
        "Замена дисплея",
        "Замена USB",
        "Замена АКБ",
        "Диагностика",
    ]
    reply_keyboard = make_reply_keyboard(buttons)
    send_message(update, context,
                 message='Целевая проблема, которую требуется устранить 🤯:',
                 reply=reply_keyboard)

    return CURRENT_STATUS_PROBLEM


def fun_third_question_data_phone(update, context) -> str:
    """
        Принимает данные с третьего ответа
        возвращает следующий стейт

    :param update:
    :param context:
    :return:
    """
    print('3 question')
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id,
                                              CURRENT_STATUS_PROBLEM)
    problem = update.message.text

    update.message.reply_text(
        'Принято, записываю...'
    )

    save_order_3rd_question_receiver(db=db,
                                     table=table_orders,
                                     order_id=current_order,
                                     problem=problem,
                                     current_status=CURRENT_STATUS_IMEI
                                     )
    buttons = [
        "Пропустить",
    ]
    reply_keyboard = make_reply_keyboard(buttons)
    send_message(update, context,
                 message='Идентификатор устройства, серийник, IMEI/MEID \n 👁👁👁',
                 reply=reply_keyboard)
    return CURRENT_STATUS_IMEI


def fun_get_imei(update, context):
    """получение имеи"""

    imei = update.message.text
    if imei.lower() == "пропустить":
        imei = "Идентификатор не найден или отсуствует"
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id,
                                              CURRENT_STATUS_IMEI)
    save_imei(order_id=current_order, imei=imei,
              current_status=CURRENT_STATUS_COMMENT)

    # отправляем подсказки для следующего шага
    buttons = [
        "Пропустить",
        "ЗУ",
    ]
    reply_keyboard = make_reply_keyboard(buttons)
    send_message(update, context, message='Замечания, комплектация',
                 reply=reply_keyboard)

    return CURRENT_STATUS_COMMENT


# get comment
def fun_get_comment(update, context):
    comment = update.message.text
    if comment.lower() == "пропустить":
        comment = "Замечаний при приеме нет"
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id,
                                              CURRENT_STATUS_COMMENT)

    save_comment(comment=comment, order_id=current_order,
                 current_status=CURRENT_STATUS_PRICE)
    buttons = [
        "1000",
        "1500",
        "2500",
        "3500",
        "После диагностики",
        "Только диагностика",
    ]
    reply_keyboard = make_reply_keyboard(buttons)
    send_message(update, context, message='Цена работы? \n💰💰💰',
                 reply=reply_keyboard)

    return CURRENT_STATUS_PRICE


def fun_get_price(update, context):
    price = update.message.text
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id,
                                              CURRENT_STATUS_PRICE)

    save_price(order_id=current_order, price=price,
               current_status=CURRENT_STATUS_PREPAYMENT)
    buttons = [
        "Предоплаты нет",
    ]
    reply_keyboard = make_reply_keyboard(buttons)
    send_message(update, context, message='Предоплата?\n🤑🤑🤑🤑',
                 reply=reply_keyboard)

    return CURRENT_STATUS_PREPAYMENT


def fun_get_prepayment(update, context):
    prepayment = update.message.text
    if prepayment.lower() == "предоплаты нет":
        prepayment = "0"
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id,
                                              CURRENT_STATUS_PREPAYMENT)

    save_prepayment(order_id=current_order, prepayment=prepayment,
                    current_status=CURRENT_STATUS_CHECK_DELIVERY)

    print('delivery question')
    buttons = [
        "Да",
        "Нет",
    ]

    reply_keyboard = make_reply_keyboard(buttons)
    message = "Доставка 🙄?"

    send_message(update, context, message=message, reply=reply_keyboard)

    return CURRENT_STATUS_CHECK_DELIVERY


def fun_get_delivery(update, context) -> str:
    """
    Отвечает пользователю началом диалога добавления заказа при соотвествующем триггере

    :param update:
    :type context: object
    :return status: str
    """

    answer = update.message.text.lower()
    print(answer)
    yes = "да"
    no = "нет"
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id,
                                              CURRENT_STATUS_CHECK_DELIVERY)

    if answer == yes:
        send_message(update, context, message="Сохраняю")
        save_delivery_status(order_id=current_order, delivery_status=answer)
        return fun_check_delivery(update, context, current_order)

    elif answer == no:
        send_message(update, context, message="Сохраняю")
        save_delivery_status(order_id=current_order, delivery_status=answer)
        return fun_check_delivery(update, context, current_order)

    else:
        send_message(update, context,
                     message="Ответьте да или нет! \n 😡😡😡😡😡")
        return CURRENT_STATUS_CHECK_DELIVERY


def fun_check_delivery(update, context, current_order):
    """Возвращает векторы веток
    """
    print('check delivery question')

    chat_id = update.message.chat_id
    # необходимо выбрать столбец статуса доставки, который принадлежит этому клиенту
    delivery_status = get_delivery_status(order_id=current_order)

    if delivery_status == ('да'):
        send_message(update, context, message='Вы выбрали доставку. ')
        # update.message.reply_text('Вы выбрали доставку. ')
        get_tg_contact_kbd = [[KeyboardButton(text="Отправить геопозицию",
                                              request_location=True)],
                              [KeyboardButton(text="Отмена")],
                              [KeyboardButton(text="Назад")],
                              ]
        reply = ReplyKeyboardMarkup(get_tg_contact_kbd)
        send_message(
            update, context,
            message='Отправьте адрес или геопозицию для приема аппарата курьером',
            reply=reply)

        return CURRENT_STATUS_AWAIT_COORDS

    elif delivery_status == 'нет':  # case if without delivery
        save_order_status(order_id=current_order,
                          status=CURRENT_STATUS_SUCCESSFUL_END,
                          )
        send_message(update, context,
                     message=f"Прием завершен. Заказ №{current_order} отправлен на печать. ",
                     reply=hub_keyboard(chat_id)
                     )
        # печать
        # print_document(doc)  # THAT FOR LOCAL PRINTER
        geo_staff = get_staff_geo(chat_id=chat_id)
        doc = save_document(current_order, geo=geo_staff)
        print(doc)
        # ALL CODE IS ASYNC
        subprocess.run(
            ['venv/scripts/python', 'telethon_call.py', f'{geo_staff}',
             f'{doc}'])  # THAT FOR SUBPROCESS

        return ConversationHandler.END


def fun_check_coords(update, context):
    print('1st coord question')
    text_address = update.message.text
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id,
                                              CURRENT_STATUS_CHECK_DELIVERY)

    if text_address == "Назад":
        return CURRENT_STATUS_CHECK_DELIVERY

    if text_address is not None:
        if text_address.lower() == "отмена":
            cancel(update, context, order_id=current_order)
            send_message(update, context, "Создание заказа отменено")
            save_current_status(db,
                                table_orders,
                                client_id,
                                order_id=current_order,
                                current_status=CURRENT_STATUS_UNSUCCESSFUL_END,
                                previous_status=CURRENT_STATUS_CHECK_DELIVERY)
            return ConversationHandler.END

    try:
        geo = update.message.location
    except TypeError:
        geo = None

    if geo is not None:
        # in case geo send
        print(geo)
        save_geo(db, table_addresses, order_id=current_order, position=1,
                 geo=geo)

    elif geo is None:
        # in case geo not sent
        if text_address is None:
            # in case address and geo is empty
            send_message(update, context,
                         message='Не видно адреса, попробуйте еще раз 😊')
            return CURRENT_STATUS_AWAIT_COORDS
        else:
            # in case geo not send and address not empty
            geo = update.message.text
            print(geo)
            save_address(db, table_addresses, order_id=current_order,
                         position=1, address=geo)

    save_current_status(db,
                        table_orders,
                        client_id,
                        order_id=current_order,
                        current_status=CURRENT_STATUS_SUCCESSFUL_END,
                        previous_status=CURRENT_STATUS_CHECK_DELIVERY)

    send_message(update, context, 'Принято, записываю... 😊')
    # печать
    geo_staff = get_staff_geo(chat_id=chat_id)
    doc = save_document(current_order, geo=geo_staff)
    # print_document(doc)
    send_message(update, context, 'Прием заказа завершен', reply='remove')
    # telethon
    subprocess.run(
        ['venv/scripts/python', 'telethon_call.py', f'{geo_staff}', f'{doc}'])
    # with ProcessPoolExecutor(max_workers=2) as executor:
    #    send_message_telethon(geo_staff, doc)

    return ConversationHandler.END


order_receive_handler_receiver = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_on_entry)],

    states={
        # select branch
        STATUS_SELECT_TYPE: [MessageHandler(Filters.text, fun_selected_branch)],

        # detail branch
        CURRENT_STATUS_DETAIL_NUMBER: [
            MessageHandler(Filters.text, fun_detail_number)],
        CURRENT_STATUS_DETAIL_CLIENT_NAME: [
            MessageHandler(Filters.text, fun_get_client_name_detail)],
        CURRENT_STATUS_DETAIL_NAME: [
            MessageHandler(Filters.text, fun_get_detail_name_detail)],
        CURRENT_STATUS_DETAIL_COMMENTARY: [
            MessageHandler(Filters.text, fun_get_commentary)],
        CURRENT_STATUS_DETAIL_PREPAYMENT: [
            MessageHandler(Filters.text, fun_get_detail_prepayment)],

        # get regular order branch
        STATUS_AWAIT_NUMBER: [MessageHandler(Filters.text, fun_get_number)],
        CURRENT_STATUS_NAME: [MessageHandler(Filters.text, fun_get_name)],
        CURRENT_STATUS_SURNAME: [MessageHandler(Filters.text, fun_get_surname)],
        CURRENT_STATUS_MIDDLE_NAME: [
            MessageHandler(Filters.text, fun_get_middle_name)],
        CURRENT_STATUS_ASK_BRAND: [MessageHandler(Filters.text, ask_for_brand)],
        CURRENT_STATUS_ANSWER_BRAND: [
            MessageHandler(Filters.text, fun_first_question_data_phone)],

        CURRENT_STATUS_MODEL: [
            MessageHandler(Filters.text, fun_second_question_data_phone)],
        CURRENT_STATUS_PROBLEM: [
            MessageHandler(Filters.text, fun_third_question_data_phone)],
        CURRENT_STATUS_IMEI: [MessageHandler(Filters.text, fun_get_imei)],
        CURRENT_STATUS_COMMENT: [MessageHandler(Filters.text, fun_get_comment)],
        CURRENT_STATUS_PRICE: [MessageHandler(Filters.text, fun_get_price)],

        CURRENT_STATUS_PREPAYMENT: [
            MessageHandler(Filters.text, fun_get_prepayment)],

        CURRENT_STATUS_CHECK_DELIVERY: [
            MessageHandler(Filters.text, fun_get_delivery)],
        # fun_check_delivery функция должна вернуть стейт
        # в зависимости от выбора юзера с доставкой или без, для направления на соттвествующую ветку
        # курьерская ветка = CURRENT_STATUS_AWAIT_COORDS с кнопками управления адресом
        # без доставки = ConversationHandler.END

        # courier branch
        # 1st address

        # если адреса нет то функция fun_check_coords возвращает этот же стейт, или вопрос о другом адресе
        CURRENT_STATUS_AWAIT_COORDS: [
            MessageHandler(Filters.text | Filters.location, fun_check_coords)],

    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=True
)
