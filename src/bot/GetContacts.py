from time import time

from Pool import Pool
from config_bot import DATABASE_TABLE_CLIENTS_NUMBERS_NAME
# projects files
from config_bot import DB_NAME
from db_manager import save_client_number


class GetContacts:

    def __init__(self, update, context, number_id, number):
        """
            Получает client_id (1),
        записывает номер (contact) в БД с идентификатором клиента (client_id)
            """
        # получаем основные объекты
        self.number_id = number_id
        self.number = number
        self.update = update
        self.context = context
        self.timestamp = time()

        # раскладываем на составляющие
        self.chat_id = self.update.message.chat_id
        # Получает client_id (1)
        self.client_id = Pool(DB_NAME).retrieve_last_id('client_id')

    def fulfill(self):
        save_client_number(DB_NAME,
                           DATABASE_TABLE_CLIENTS_NUMBERS_NAME,
                           self.client_id,
                           self.number_id,
                           self.number,
                           self.timestamp)

        return None
