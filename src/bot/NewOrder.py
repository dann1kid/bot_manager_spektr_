#!/usr/bin/env python
# -*- coding: utf-8 -*-
from time import time

from db_manager import save_new_user
from src.bot.keyboards import make_reply_keyboard
from config_bot import DB_NAME, DATABASE_TABLE_CLIENTS_NAME

"""Подлежит сериализации для быстрого извлечения параметров
   Хранить ли эти промежуточные данные в работающем приложении
   или загружать в базу данных поэтапно?
   Так или иначе финализирющий штрих - это базирование композиции
   вокруг client_id в базе данных. 
"""


class NewOrder:

    def __init__(self):
        self.timestamp = time()

    def start(self):
        # начальный диалог
        # фильтр
        if self.user.is_bot:
            # field for ban bot

            pass

        self.context.bot.send_message(

            chat_id=self.chat_id,
            text=f"Я помогу вам отправить технику в ремонт."
                 f"Вы сейчас: \n"
                 f"(Нажмите кнопки ниже для взаимодействия.)",
            reply_markup=self.generate_keyboard(step=0),
        )

    def fulfill(self):
        save_new_user(db=DB_NAME,
                      table=DATABASE_TABLE_CLIENTS_NAME,
                      client_id=self.client_id,
                      chat_id=self.chat_id,
                      name=self.user_name,
                      timestamp=self.user_create_timestamp,
                      )

    def generate_keyboard(self, step):
        """
            Чекает права и текущую роль юзера,
            добавляет кнопки к текущему сообщению на их основе
        :return:
        """
        # must be replaced by generated buttons by rights

        if step == 'order_start':

            buttons = ["Прием в магазине",
                       "Хочу чтобы прислали курьера",
                       ]
            return make_reply_keyboard(buttons)

        elif step == 'order_second':
            pass
