#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import telegram
from telegram import ReplyKeyboardMarkup
from telegram import ReplyKeyboardRemove
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram.ext import CommandHandler

from Pool import Pool
from GetContacts import GetContacts

from src.bot.keyboards import hub_keyboard

from config_bot import DB_NAME as db

ENTRY_POINT = Filters.regex("^Поделиться номером$")

STATUS_AWAIT_NUMBER = "await_number"
STATUS_CHECKING_NUMBER = "check_number"


# Затем в BotManager необходимо импортировать этот конверсейшн хандлер (обернув наверное в класс)
# Затем добавить в dispatcher.add_handler(number_receive_handler)


def send_message(update, context, message, reply=None) -> None:
    """context based send message for any reason
    :type update: object
    :param context:
    :param message:
    :param reply:
    
    :return None
    """
    context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=reply,
    )
    
    return None


def awaiting_number(update, context) -> str:
    """ При нажатии кнопки поделится отправляет сообщение
    пользователю, что у него есть варианты как поделится

    :type update: object
    :param context:
    
    :return STATUS: передает стейт для конверсейшн хандлеоа
    """
    user = update.message.from_user
    get_tg_contact_kbd = [[telegram.KeyboardButton(text="Поделится номером этого аккаунта", request_contact=True)]]
    reply = ReplyKeyboardMarkup(get_tg_contact_kbd)
    message = 'Отправьте Ваш номер или нажмите кнопку ниже.'
    send_message(update, context, message=message, reply=reply)
    
    return STATUS_CHECKING_NUMBER  # returns name of the next state


def search_num(text: str):
    text = text.replace(" ", "")
    return re.search('[\d]{11,12}|'
                     '\+7[\d]{10,13}|'
                     '8 [\d]{3} [\d]{3} [\d]{2} [\d]{2}|'
                     '\+7 [\d]{3} [\d]{3} [\d]{2} [\d]{2}|'
                     '8-[\d]{3}-[\d]{3}-[\d]{2}-[\d]{2}|'
                     '\+7-[\d]{3}-[\d]{3}-[\d]{2}-[\d]{2}|'
                     '[\d]{10}', text)  # СОРЯН РЕГУЛЯР ОЧКА


def format_num(num: str) -> str:
    """

    :type num: object
    """
    
    # регексп проверяет и пускает только полноценные номера
    if num[0] == "+":
        index = num[2:5]
        prefix = num[5:8]
        num_1 = num[8:10]
        num_2 = num[10:12]
    elif len(num) == 11:
        index = num[1:4]
        prefix = num[4:7]
        num_1 = num[7:9]
        num_2 = num[9:11]
    elif len(num) == 10:
        index = num[0:3]
        prefix = num[3:6]
        num_1 = num[6:8]
        num_2 = num[8:10]
    
    else:
        return ''  # бля, НА ВСЯКИЙ СЛУЧАЙ
    
    return f"+7({index}){prefix}-{num_1}-{num_2}"


def checking_number(update, context):
    """проверяет номер в стейте и сохраняет его, используя модель"""
    
    # init
    user = update.message.from_user  # users dict. Later for logging
    number = update.message.text  # update text what can contains the contact
    chat_id = update.message.chat_id

    # оповещаем пользователя что информация принята
    update.message.reply_text('Принято. Проверяю информацию...',
                              reply_markup=ReplyKeyboardRemove()
                              )
    
    # проверка меседжа на содержания в нем контакта
    try:
        contact = update.effective_message.contact['phone_number']
        print(f"contact = {contact}")
    except TypeError:  # я не помню какое исключение выскакивает на пустой контакт в апдейте
        contact = None
    
    # ветвление на проверку контакта ИЛИ строки с номером
    
    if contact is not None:  # в случае если контакт АККАУНТА
        # сопоставить статус на "получение номера"
        print(contact)
        number = contact
    
    elif contact is None:  # если нет контакта АККАУНТА,
        # то забиваем номер с текста ->
        number = search_num(number)
        
        if number is None:
            update.message.reply_text('Не видно цифр, попробуйте еще раз 😊',
                                      reply_markup=ReplyKeyboardRemove())
            return STATUS_CHECKING_NUMBER
        else:
            number = number.group(0)
        
        print(f"re.search = {number}")
    
    # отдельная ветка на тест
    elif number is None and contact is None:
        update.message.reply_text('Номер не определен... Попробуйте еще раз 😊',
                                  reply_markup=get_tg_contact)
        return STATUS_CHECKING_NUMBER
    
    # определяем идентификатор номера для таблицы
    number_id = Pool(db).retrieve_last_id('number_id')
    # форматируем номер для записи в таблицу
    formatted_number = format_num(number)
    # заполняем поля класса
    get_contacts = GetContacts(update, context, number_id, formatted_number)
    # сохраняем методом класса
    get_contacts.fulfill()
    # отправляем ответное сообщение юзеру.
    # После записи номера ему выдается нужная клавиатура
    update.message.reply_text('Номер определен и записан 😊.',
                              reply_markup=hub_keyboard(chat_id))
    # завершаем
    return ConversationHandler.END


def cancel(update, context):
    user = update.message.from_user
    # logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


number_receive_handler = ConversationHandler(
        entry_points=[MessageHandler(ENTRY_POINT, awaiting_number)],
        
        states={
                # alone state =(
                STATUS_CHECKING_NUMBER: [MessageHandler(Filters.text | Filters.contact, checking_number)],
            
        },
        fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=True
)
