"""
Сущность Заказа ремонта для выполнения всех связанных методов, в тч. бд, репрезентация и прочий вывод
"""
from src.bot.MessageBuilder.OrderMessageBuilder import OrderMessageBuilder
from src.bot.models.pw_models import Order, AdditionalService


# Todo: переписать с учетом этого класса
class OrderRepair:

    def __init__(self, order_id: str | int = None):
        self.reply = None
        self.message = None
        self.order_id = order_id
        self.order = None
        self.get_db_data()

    def get_db_data(self):
        self.order = Order.get(Order.item_id == self.order_id)

    def add_work(self, price):
        AdditionalService.create(order_id=self.order_id, price=price)

    def create_message(self):
        builder = OrderMessageBuilder(self.order)
        self.message = builder.create_message()
        # self.reply = builder.create_buttons_inline()
