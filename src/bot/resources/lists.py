from src.bot.resources import strings

btn_default_brands: list[str] = [

    "Acer",
    "Apple",
    "Asus",
    "HP",
    "Huawei",
    "Lenovo",
    "Philips",
    "OPPO",
    "Samsung",
    "Vivo",
    "Xiaomi",
    "Poco",
    "Неизвестно",
]

btn_default_colors: list[str] = [
    "Красный",
    "Синий",
    "Зеленый",
    "Белый",
    "Черный",
    "Розовый",
]

btn_default_colors_list = btn_default_colors.extend(
    [
        strings.txt_cancel,
        strings.txt_back,
    ]
)

btn_default_brands_list = btn_default_brands.extend(
    [
        strings.txt_cancel,
        strings.txt_back,
    ]
)
btn_default_trouble: list[str] = [
    "Замена платы системного разъёма",
    "Замена дисплея",
    "Замена разъема",
    "Замена шлейфа",
    "Ремонт платы",
    "Замена АКБ",
    "Диагностика",
    "FRP",
]
btn_default_trouble_list = btn_default_trouble.extend(
    [
        strings.txt_cancel,
        strings.txt_back,
    ]
)
btn_default_prices: list[str] = [

    "0",
    "500",
    "1000",
    "1500",
    "2000",
    "2500",
    "3000",
    "3500",
    "4000",
    "4500",
    "5000",
]
btn_default_prices_list = btn_default_prices.extend([
    strings.txt_cancel,
    strings.txt_back,
    strings.txt_btn_skip,
])
