# dialog entries
# TODOlit files
# TODO: lang split wit language select by user

txt_prompt_found_profiles = "Найдены профили для этого номера"
client_orders_menu = "Статусы моих заказов"  # client orders menu
txt_extradited = "Выдано"
txt_to_extradite = "Выдать"
txt_select_extradition_type = "Выбрать тип доставки"
txt_sent_to_extradition = "Отправить в \"Решение по выдаче\""
txt_on_extradition = "На выдаче"
txt_on_extradition_f = "Заказ №{0} передан на выдачу. "
txt_default_number = "+7(999)999-99-99"
txt_default_name = "Cпектр"
txt_ordered: str = "Заказано"
txt_mark_as_ordered: str = "Пометить как заказано"
txt_not_ordered = 'Не заказано'
txt_installed = "Установлено"
txt_back = "Назад"
btn_finish = "Завершить"
txt_prepayment = "Предоплата"
txt_prompt_commentary = "Комментарии, комплектация"
txt_caption_registration = "Ссылка для быстрой регистрации"
txt_successful_registration = "Регистрация завершена успешна."
txt_unsuccessful_registration = "Неправильная ссылка для регистрации"
# roles
str_role_receiver = "receiver"
str_prompt_number_receiver = "Отправьте номер клиента"

txt_prompt_select_menu_enumerate = "Выберите категорию"

btn_t_cancelled_orders = "Отмененные заказы"
btn_t_under_eval = "На оценке"
btn_t_in_work = "В работе"
txt_in_work = "В работе"
btn_t_ready_for_return = "Ожидают возврата курьером"
btn_t_on_delivery_back = "Курьер везет клиенту"
btn_t_on_delivery_work = "Курьер везет в мастерскую"
btn_t_on_transmit_work_courier = "Ожидает курьера в мастерской"
btn_t_on_delivery_client = "На доставке клиенту"
btn_t_delivered_to_work = "Доставлено до мастерской"
btn_t_finished = "Завершены"
txt_finished = "Завершено"
txt_on_delivery = "Доставляется до заказчика"
txt_prompt_delivery = "Будет ли доставка? \n" \
                      "Если да, то введите адрес, или местоположение.\n" \
                      "Если нет, то нажмите кнопку \"Нет\""
txt_prompt_delivery_client = "Пожалуйста, укажите обратный адрес или геопозицию"
txt_prompt_send_address = "Отправьте обратный адрес"
txt_prompt_end_list = "Вывод списка завершен"
btn_to_send_address = "Отправить адрес"
btn_pls_add_address = "Укажите адрес"
btn_dont_change_address = "Не вводить адрес"

txt_fulfill_profile_prompt = "Заполнить профиль"
btn_fulfill_profile_prompt = [
    txt_fulfill_profile_prompt,
]

txt_yes = "Да"
txt_no = "Нет"
please = "Пожалуйста"
txt_prompt_model = f"{please}, укажите модель"
prompt_trouble = f"{please}, укажите в чем неисправность"
prompt_color = f"{please}, укажите цвет"
txt_prompt_imei = f"{please}, укажите IMEI. \n \
Он указан на задней крышке, коробке от аппарата или в меню настроек телефона (раздел об аппарате).\n" \
                  "Также можно набрать <code> *#06# </code> в приложении для звонков "
txt_prompt_photo = f"{please}, отправьте фото повреждения или самого устройства "
txt_prompt_photo_add = f"{please}, отправьте еще фото либо нажмите кнопку \"Завершить\""
txt_prompt_brand = f"{please}, укажите бренд аппарата"
txt_prompt_order_finished = "Заказ отправлен на оценку. После оценки и с вашего согласия прибудет курьер."
txt_cancel = "Отмена"
txt_cancelled = "Отменено"
txt_fail_message = "Попытка сохранить провалилась."
txt_try_again = f"{please}, попробуйте еще раз."
txt_not_registered = "Вы зарегистрированы, но профиль не заполнен."
txt_prompt_profile = f"Добрый день. \n Для начала работы необходимо заполнить профиль"
txt_prompt_number_error = "Не видно цифр, попробуйте еще раз 😊"
txt_prompt_name = f"{please}, укажите имя."
txt_prompt_surname = f"{please}, укажите фамилию."
txt_prompt_price = f"{please}, укажите цену."
btn_get_number = ["Поделиться номером этого аккаунта"]

prompt_unknown_args = "Неизвестный аргумент."
btn_receiver_rgm_slt = [
    "Запчасть",
    "Ремонт",
    "Отмена",
]

prompt_number = 'Отправьте Ваш номер или нажмите кнопку ниже.'
txt_account_number = "Отправить номер аккаунта"
txt_success_profile = "Профиль заполнен. \n "
main_menu = "Главное меню"
txt_error_to_start_prompt = "Произошла ошибка. Выполните команду /start"
txt_err_deeplink_start = "Недостаточно аргументов команды"
txt_wait_client = "Ожидает клиента"

# client_states
new_client = "new_client"
txt_status_registered = "registered"

# staff statuses
client = 'client'
receiver = 'receiver'

btn_active_orders = "Мои заказы"

btn_send_geo = "Отправить геопозицию"
txt_print_question = "Отправлять в печать?"
prompt_select_which_profile = 'Выберите профиль из указанных'
prompt_correct_data = f"{please}, введите корректные данные."
alert_return_main_menu = "Возврат в главное меню"
txt_link_expired = "Ссылка недействительна"
txt_new_client = "Новый клиент"
txt_client = "Клиент"
txt_receiver = "Приемщик"
txt_courier = "Курьер"
txt_master = "Мастер"
txt_admin = "Администратор"

txt_no_orders = "Заказов нет"

txt_restrict_access = "Вы не имеете права на доступ к этой информации." \
                      " Отправляю отчет администратору о несанкционированном доступе."

txt_new_orders = "Новые доступные заказы: "
err_finished_order = "Этот заказ завершен"
err_access_denied = "Доступ запрещен"
txt_detail_name = "Название детали"
txt_btn_skip = "Пропустить"
txt_add_detail = "Добавить деталь"
txt_add_order = "Добавить ремонт"
txt_order_detail = "Заказать деталь"
txt_available_orders = "Заказы на ремонт"
txt_available_orders_status = "Заказы в ремонте: {0}"
txt_orders = "Заказы"
prompt_select_statuses_orders = "Выбери статус"
txt_select = "выбор"
txt_select_order = "Выбери заказ"
txt_no_orders_with_selected_status: str = "Нет заказов с указанным статусом"
txt_add_comment = "Добавить комментарий"
txt_show_comment = "Показать комментарии"
txt_send_comment = "Отправь комментарий и я добавлю к этому заказу"
txt_comment_added = "Комментарий добавлен"
txt_enumerating_comments = "Перечисляю комментарии"
txt_enumerating_details = "Перечисляю детали"
txt_add_detail_order = "Добавить деталь"
prompt_explain_detail = "Опишите деталь"
prompt_price_detail = "Цена детали"
txt_detail_added = "Деталь добавлена"
txt_show_details = "Показать детали"
txt_change_price_order = "Изменить цену"
prompt_sure_change_price = "Цена согласована с клиентом?"
txt_change_price_order_cancelled = "Изменение цены отменено"
txt_new_price_order = "Отправьте измененную цену для этого заказа"
txt_change_price_order_confirmed = "Изменение цены принято"
txt_output_ended = "Вывод завершен"
txt_sum_payment = "Сумма оплаты"
prompt_payment = "Нажмите на кнопку или введите вручную"
prompt_payment_received = "Оплата принята"
txt_on_waiting_courier_at_client = "Ожидает выбора курьером"
txt_on_wait_courier = "Ожидает курьера на выдаче"
btn_eval = "На оценке"
btn_evaluate = "Оценить заказ"
notif_evaluated_order = "Ваш заказ №{0} оценен с примерной суммой в {1}. Проверьте ваши заказы со статусом  "
txt_ok = "Хорошо"
txt_preprice = "Предварительная сумма для заказа"
txt_get_preprice = "Укажите предварительную сумму для клиента"
txt_select_type_delivery = "Выберите тип доставки заказа: курьер или самовывоз"
courier = "Курьер"
self_delivery = "Самовывоз"
note_sent_extradition_client = "Заказ отправлен на выдачу и ожидает клиента"
note_sent_extradition_courier = "Заказ отправлен на выдачу и ожидает клиента"
note_detail_await_select_delivery = "Заказ будет ожидать решения по доставке"

note_back_address = "Указан обратный адрес. Изменить или оставить " \
                    "прежним?\nУказанный адрес {0} "

note_back_address_select_delivery = "Указан обратный адрес или геопозиция.\n"

note_no_back_address_select_delivery = "Не указан обратный адрес или геопозиция."
txt_ask_delivery_type = "Отправить курьером или самовывоз?"
note_no_back_address = "Не указан обратный адрес."
note_back_geo = "Указана геопозиция. Изменить на другую (или адрес) или " \
                "оставить прежней? "
approve_order = "Заказ успешно подтвержден."
wait_for_courier = "Ожидайте курьера"
txt_store_address = "Адрес магазина - Бронницы, Советская 63"
txt_enumerate_detail = "Заказы на детали"
txt_add_add_service = "Добавить дополнительную услугу"
txt_selected = "Выбрано"
txt_selected_profile = "Выбран профиль"
txt_no_access_here = "Нет доступа здесь"
err_expired_link = "Ссылка недействительна"
err_user_already_exists = "Пользователь уже зарегистрирован"
ok_user_registered = "Профиль создан."
txt_order_transported = "Заказ перенесен."
txt_order_transported_f = "Заказ №{0} перенесен."
err_banned = "Вы заблокированы данным ботом."
str_get_order_eval = "Отправить заказ на оценку"
str_change_profile = "Изменить профиль"
btn_hide_keyboard = "Скрыть клавиатуру"
ok_keyboard_hide = "Убираю клавиатуру"
btn_show_keyboard = "Показать клавиатуру"
ok_keyboard_show = "Убираю клавиатуру"
err_only_digits = "Введите число."
prt_press_again_to_start = "Предыдущий диалог завершен. " \
                           "Нажмите еще раз чтобы начать."
prt_additional_info = "Дополнительная информация."
btn_remove_detail = "Удалить заказ"  # rename detail
txt_available_details = "Доступные заказы на детали"
weekdays = {
    1: "Понедельник",
    2: "Вторник",
    3: "Среда",
    4: "Четверг",
    5: "Пятница",
    6: "Суббота",
    7: "Воскресенье",
}
txt_is_detail_part = "Это запчасть?"
prt_are_you_sure = "Вы уверены?"
txt_detail_marked = "Деталь отмечена как заказанная"
txt_select_address = "Обратный адрес не указан. Отправьте адрес или оставьте" \
                     " как самовывоз"
note_no_back_delivery_data = "Не указан обратный адрес или геопозиция." \
                             "Отправить курьером или самовывоз?"
note_repair_sent_on_extradition = "Заказ отправлен на выдачу"
note_will_delivered_by_courier = "Заказ будет передан клиенту через курьера."
txt_wait_delivery_selection = "Ожидает решения по доставке"

btn_send_delivery_self_delivery = "Самовывоз"
btn_send_delivery_by_address = "Отправить по адресу"
# продумать
# короткую формулировку для кнопки ->
btn_send_delivery_by_add_address = "Добавить адрес и на выдачу"
txt_detail_was_deleted = "Заказ удален."
txt_detail_was_extradited = "Заказ выдан"
txt_no_comments = "Комментариев нет"
txt_no_details = "Добавленных деталей нет"
txt_no_data = "Не указано"
txt_charge_device = "Зарядное устройство"
txt_cover = "Чехол"
txt_noname_model = "NoName"
btn_reveal_goods_to_order = "Отправить товары на мыло"
txt_await_detail = "Ожидает запчасти {count} шт."
txt_no_awaited_details = "Запчасти для ремонта не указаны"
txt_finished_order_num = "Заказ №{order_id} завершен"
txt_error_occured = "Произошла ошибка"
txt_notice_awaiting_detail = "<b>Внимание - ожидаются запчасти</b>"
txt_notice_no_awaiting_detail = "<b>Запчастей для заданного заказа не ожидается</b>"
txt_notice_important_mark = "ОЧЕНЬ ВАЖНО"
txt_summary_payment = "Σ"
txt_search_orders = "Поиск ремонт"
txt_search_details = "Поиск товар"
txt_search_result = "Результаты поиска по запросу"
prompt_search = "Что ищем?"
prompt_search_again = "Еще ищем?"
txt_no_orders_with_query = "Нет заказов по указанному запросу"
txt_labels_print_question = "Сколько этикеток печатать?"
txt_labels_count_1 = "1"
txt_labels_count_2 = "2"
txt_labels_count_3 = "3"
txt_store_name_1 = "Спектр"
err_already_extradited = "Заказ уже выдан!"
wait_decision_client = "Ожидает решение клиента"
txt_confirm_order = "Согласны с указанной ценой?"
note_new_order_from_client = "Доступен новый заказ для доставки от клиента "
note_new_order_to_eval = "Добавлен заказ для оценки"
err_order_not_available = "Заказ не доступен"
btn_commit_order = "Подтвердить заказ"
note_order_committed = "Заказ помещен"
txt_showing_pics = "Перечисляю прикрепленное фото"
btn_show_pics = "Показать фото"
transmit_order_from_store_to_courier = "Передать с выдачи курьеру"
err_order_in_another_status = "Этот заказ в другом статусе."
err_order_in_another_status_f = "Этот заказ в другом статусе: {0}"
txt_link_to_transmit_order_to_courier_from_store = "Ссылка для передачи от магазина к курьеру"
note_order_transmitted = "Заказ перемещен"
note_order_transmitted_to_you = "Заказ перемещен Вам"
btn_available_orders = "Доступные заказы"
txt_order = "Заказ"
active_wait_courier = "Ожидает прибытия курьера"
txt_pickup_order = "Забрать заказ"
txt_order_picked = f"Заказ выбран и доступен в статусе \"{active_wait_courier}\""
err_already_wait_courier = "Заказ уже выбран курьером (возможно вами). \n" \
                           "Проверьте заказы со статусом \"{active_wait_courier}\" "

txt_cancel_pickup_order = "Освободится от заказа"
err_no_courier_selected = "Нет назначенного курьера"
txt_order_cancelled_pick = f"Заказ выбран и доступен в статусе \"{txt_on_waiting_courier_at_client}\""
txt_order_was_cancelled_from_pickup = "Произошла отмена курьером поездка до клиента" \
                                      " курьером для принятия объекта ремонта"
txt_tell_me_why = "Укажите причину"
note_order_has_preprice = "Предварительная сумма уже определена. Вы уверены?"
btn_cancel_order = "Отменить заказ"
txt_order_cancelled = "Заказ отменен"
btn_get_order_from_client = "Получить заказ от клиента"
txt_qr_caption_for_client_to_courier = "Если курьер прибыл, то покажите ему этот QR для передачи вашего заказа №{0}"
btn_client_not_takes = "Клиент не принимает заказ"
txt_order_will_returned_store = "Заказ будет возвращен в мастерскую. Причина: {0}"
txt_order_to_transmit_to_store_from_client = "QR для передачи приемщику заказа №{0}"
txt_order_to_transmit_to_client_from_courier_order = "QR для сканирования курьером для передачи заказа №{0} " \
                                                     "для передачи Вам. Пожалуйста, проверьте аппарат " \
                                                     "и выполненную работу, затем покажите данный QR курьеру " \
                                                     "и оплатите заказ. " \
                                                     "Если Вы считаете что работа не выполнена или присутствуют " \
                                                     "недочеты, сообщите курьеру."
btn_transmit_order_from_courier_to_work = "Передать приемщику заказ"
btn_transmit_order_from_courier_to_client = "Передать клиенту заказ"
btn_help = "Помощь"
txt_client_help = "Добро пожаловать в чат-бот магазина Спектр!\n" \
                  "Мы готовы помочь вам с заказом ремонта и оценкой стоимости работ. \n" \
                  "Вы можете оставить заявку на оценку стоимости работ, выбрать удобное время для звонка или получить" \
                  " консультацию по ремонту. Мы будем держать вас в курсе статуса выполнения заказа и сообщать" \
                  " о готовности результата. Спасибо, что выбрали нас!\n" \
                  f"Контактный номер магазина: {txt_default_number}\n"\
                  "Связь с разработчиком: "
txt_order_cancelled_by_client = "Заказ №{0} отменен клиентом"
txt_order_was_not_cancelled = "Заказ №{0} не был отменен."
txt_order_wait_courier = "Заказ был передан на выдачу  ожидает курьера. "
txt_no_chat_id = "Нет зарегистрированного контакта клиента для чат бота. Заказ отправлен на самовывоз."
err_you_not_receiver = "Вы не являетесь приемщиком"
err_you_not_courier = "Вы не являетесь курьером"
err_you_not_client = "Вы не являетесь клиентом"
err_you_not_admin = "Вы не являетесь администратором"
target_status_in_work = "в статус 'в работе'"
target_status_in_delivery_to_work = "в статус 'доставка в мастерскую'"
txt_order_transmitted_to_store = "Заказ перемещен в мастерскую"
txt_order_transmitted_to_client = "Заказ отдан клиенту"
note_courier_payment = "Возьмите оплату в размере {0} рублей"
note_new_order_in_work = "Доступен новый заказ в статусе 'В работе'"
note_client_order_finished = "Заказ №{0} передан вам и завершен"
note_qr_sent_to_client = "QR был отправлен клиенту. \n" \
                         "После проверки заказа клиент покажет Вам QR, " \
                         "вы должны взять сумму в размере {0} рублей, " \
                         "просканировать код и нажать кнопку \"Начать\" или \"Start\"" \
                         "для завершения заказа!"
txt_send_oriented_time = "Укажите ориентировочную дату"
txt_no_username = "Юзернейм не указан"
note_added_client_by_client = "Добавлен комментарий от клиента к заказу"
note_press_me_to_take_payment = "Пожалуйста, нажмите кнопку ниже для принятия оплаты"
status_wait_payment = "Ожидает оплаты"
btn_take_payment = "Принять оплату"
order_has_been_paid = "Заказ был оплачен"
txt_labels_printed = "Этикетки отправлены на печать"
btn_print_labels = "Напечатать наклейки"
err_outside_of_conversation = "Вне диалога."
err_no_client_chat_id = "С клиентом нет чат айди, и доставки без регистрации юзером в боте не осуществляется"
err_please_send_photo = "Пожалуйста, пришлите фото"
btn_add_next_photo = "Добавить еще фото"
note_saving_client_order = "Сохраняю Ваш заказ"
err_not_recognize = "Ввод не распознан"
txt_continue_add_photo = "Вы добавили фото. Добавить еще?"
btn_extradite_detail = "Выдать заказ"
note_your_order_on_extradition = "Ваш заказ №{0} находится на выдаче для самовывоза."
btn_change_user_data = "Изменить юзера"
btn_err_buttons = "Ошибка клавиатуры"
btn_select_user = "Выбрать юзера"
btn_search_user = "Найти юзера"
prompt_get_users_regime = "Выберите режим"
prompt_available_users = "Доступные юзеры"
btn_name = "Имя"
btn_surname = "Фамилия"
btn_middle_name = "Отчество"
prompt_select_field = "Выберите поле для редактирования"
prompt_send_new_value = "Пожалуйста, отправьте новое значение для этого поля"
prompt_new_name = "Отправьте новое имя"
prompt_new_surname = "Отправьте новую фамилию"
prompt_new_middle_name = "Отправьте новое отчество"
confirm_value_changed = "Значение изменено"
select_users_fields = "Напишите имя, фамилию, отчество или номер для поиска"
