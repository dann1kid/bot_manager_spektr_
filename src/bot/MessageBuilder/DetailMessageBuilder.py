"""
    Common fabric class for Detail messages. Build it from ready samples as patterns
"""
from enum import Enum

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusDetailMenu import StatusDetailMenu
from src.bot.entry_points.CallbackEntry import CallbackParser, CallbackEntry
from src.bot.models.pw_models import Client, ContactClient, Detail
from src.bot.resources import strings
from src.bot.utils.message import mult_inline_keyboard
from src.bot.utils.timestamp_parses import readable_data_from_datetime


class DetailMessageBuilder:
    class BuilderMode(Enum):
        status = 1
        search = 2

    def __init__(self, detail: Detail = None, users_chat_id=0, mode: BuilderMode = None):
        self.detail = detail
        self.mode = mode
        self.add_data = readable_data_from_datetime(self.detail.added)
        self.client = Client.get(Client.item_id == detail.client)
        self.numbs = self.client.numbs.order_by(ContactClient.added)[0].number
        self.staff_user = Client.get(Client.chat_id == users_chat_id)

    def create_message(self):
        message = f'<b>№ заказа</b>: {self.detail.item_id} <i>Создал</i>  {self.detail.creator.name}\n' \
                  f'Добавлен: {self.add_data} \n' \
                  f'<b>Статус</b>: {StatusDetailMenu[self.detail.status].value} \n' \
                  f' \n' \
                  f'<b><i>Информация о детали</i></b>\n' \
                  f'<i>Деталь</i>: {self.detail.detail_name} \n' \
                  f'<i>Описание</i>: {self.detail.text} \n' \
                  f'<i>Цена</i>: {self.detail.price} \n' \
                  f'<i>Предоплата</i>: {self.detail.prepayment} \n' \
                  f'<i>Запчасть</i>: {strings.txt_yes if self.detail.is_detail else strings.txt_no} \n' \
                  f' \n' \
                  f'<b><i>Информация о клиенте</i></b>\n' \
                  f'<i>Имя клиента</i>: {self.client.name}{self.client.surname}\n' \
                  f'<i>Последний номер клиента</i>: {self.numbs}\n' \
                  f'<i>Комментарий при приеме</i>: {self.detail.comments[0].comment if self.detail.comments.count() > 0 else strings.txt_no_data}\n' \
                  f'<i>Последний комментарий</i>: {self.detail.comments[-1].comment if self.detail.comments.count() > 0 else strings.txt_no_data} \n' \
                  f'<i>Комментариев сейчас</i>: {self.detail.comments.count()}\n'

        return message

    def create_buttons_inline(self, data):
        """
        Метод должен учитывать контекст (статус заказа, статус юзера)
        """
        # staff
        match self.staff_user.staff_status:
            case StaffLevel.receiver.value:
                return self.build_receiver_buttons(data)
            case StaffLevel.client.value:
                return self.build_client_buttons(data)
            case StaffLevel.admin.value:
                return self.build_receiver_buttons(data)
            case _:
                pass

    def build_receiver_buttons(self, data=None):
        """Построение пользовательских кнопок
        Контекст юзер -> статус заказа
        """
        if not data:
            data = CallbackParser.make_paginator_detail(self.detail.item_id,
                                                        self.detail.status,
                                                        0,
                                                        mode=DetailMessageBuilder.BuilderMode.status.value
                                                        )

        reply_markup = []
        match self.detail.status:
            # мне сейчас требуются - прием от лица персонала,
            # отметки ожидает заказа, заказано, выдать
            # обязательно ко всем статусам добавить "добавить комментарий"

            case StatusDetailMenu.on_eval.name:
                # оценить (диалог с оценкой)
                reply_markup.append(
                    (strings.btn_evaluate,
                     CallbackEntry.evaluate_detail.value + data))
                # удалить
                reply_markup.append(
                    (strings.btn_remove_detail,
                     CallbackEntry.delete_detail_detail.value + data))
                # коммент
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_detail.value + data))
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_detail.value + data))

            case StatusDetailMenu.wttrdr.name:
                # заказан (галочка)
                reply_markup.append(
                    (strings.txt_mark_as_ordered,
                     CallbackEntry.mark_detail_as_ordered.value + data))

                # удалить (не знаю, добавлять ли диалог с подтверждением)
                reply_markup.append(
                    (strings.btn_remove_detail,
                     CallbackEntry.delete_detail_detail.value + data))

                # коммент
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_detail.value + data))
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_detail.value + data))

            case StatusDetailMenu.ordrd.name:
                # в зависимости от статуса доставки (delivery_check):
                # передать курьеру

                # на выдачу
                reply_markup.append(
                    (strings.txt_sent_to_extradition,
                     CallbackEntry.send_to_extradition_detail.value + data))

                # удалить
                reply_markup.append(
                    (strings.btn_remove_detail,
                     CallbackEntry.delete_detail_detail.value + data))

                # коммент
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_detail.value + data))
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_detail.value + data))
                ...

            case StatusDetailMenu.sldlvr.name:

                # todo добавить пункт меню с выбором выдачи
                # удалить
                reply_markup.append((strings.btn_remove_detail,
                                     CallbackEntry.delete_detail_detail.value + data))
                # выбрать доставку
                reply_markup.append((strings.txt_select_extradition_type,
                                     CallbackEntry.select_delivery_type_detail.value + data))

                # коммент
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_detail.value + data))
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_detail.value + data))

                ...

            case StatusDetailMenu.odwc.name:
                # ждет курьера
                # коммент
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_detail.value + data))
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_detail.value + data))
                ...

            case StatusDetailMenu.osldlvr.name:
                # ждет курьера
                # коммент
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_detail.value + data))
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_detail.value + data))
                # extradite dialog
                reply_markup.append((strings.btn_extradite_detail,
                                     CallbackEntry.extradite_detail_detail.value + data))
                ...

            case StatusDetailMenu.od.name:
                # коммент
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_detail.value + data))
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_detail.value + data))
                ...

            case _:
                ...

        match self.mode:
            case self.BuilderMode.status:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_detail.value + data))
            case self.BuilderMode.search:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_detail_search.value + data))

        return mult_inline_keyboard(reply_markup)

    def build_client_buttons(self, data):
        pass
