from dataclasses import dataclass

from telegram_bot_pagination import InlineKeyboardButton

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.MessageBuilder.ItemsInline import ItemsInline
from src.bot.MessageBuilder.OrderMessageBuilder import OrderMessageBuilder
from src.bot.entry_points.CallbackEntry import CallbackParser
from src.bot.models.pw_models import Order, Client
from src.bot.resources import strings
from src.bot.utils.timestamp_parses import readable_data_from_datetime_inline


class StatusOrdersInline(ItemsInline):
    def __init__(self, status: str = "", page: int = 1, mode: int = 0, chat_id: int = 0):
        self.chat_id = chat_id
        self.page = int(page)
        self.status = status
        self.mode = mode
        self.items = None
        self.data = f"{CallbackParser.paginator_gen_order_status(status=self.status)}"

        self.make_items()
        self.make_keyboard()

    def make_items(self):
        print("StatusOrdersInline: make items")
        client = Client.get(Client.chat_id == self.chat_id)
        print("client id", client.item_id)
        match client.staff_status:

            case StaffLevel.client.value:
                self.items = Order.select().where((Order.status == self.status) & (Order.client == client)).order_by(
                    Order.item_id)

            case StaffLevel.receiver.value:
                self.items = Order.select().where(Order.status == self.status).order_by(Order.item_id)

            case StaffLevel.admin.value:
                self.items = Order.select().where(Order.status == self.status).order_by(Order.item_id)

            case StaffLevel.courier.value:
                # сюда добавить условие для тех заказов которые ожидаются для выбора курьером так же требуется как то
                # определить какой курьер будет направлен на заказы нужен дополнительный статус - ожидает курьера.
                # После того как курьер выбрал при помощи кнопки заказ, то заказ меняет статус на "ожидает курьера",
                # ему присваевается определенный курьер, и при нажатии на ту же кнопку другим курьером происходит
                # отказ доступа к заказу второму курьеру.
                # Так же нужна отдельная кнопка отказа от заказа, с указанием
                # причины так же нужно уведомление о поступлении всем курьерам о каждом новом доступном заказе или
                # смене статуса:
                statuses_by_courier = [
                    StatusOrderMenu.odw.name,
                    StatusOrderMenu.dc.name,
                    StatusOrderMenu.wc.name,
                    StatusOrderMenu.wp.name,

                ]
                if self.status in statuses_by_courier:
                    self.items = Order.select().where((Order.status == self.status) & (Order.courier == client)).order_by(
                        Order.item_id)
                else:
                    self.items = Order.select().where((Order.status == self.status)).order_by(
                        Order.item_id)

    @dataclass
    class InlineDevice:
        brand_name = strings.txt_no_data
        model_name = strings.txt_no_data

    def make_callback_data(self, item_index):
        return CallbackParser.make_paginator_order(
            order_id=self.items[item_index].item_id,
            status=self.items[item_index].status,
            page=self.page,
            mode=OrderMessageBuilder.BuilderMode.status.value,
        )

    def get_device(self, item_index):
        try:
            device = [x for x in item_index.devices][0]
        except IndexError:
            device = self.InlineDevice()
        return device

    def make_inline_item(self, item_index, callback_data):
        return InlineKeyboardButton(
            f"№{self.items[item_index].item_id} "
            f"{self.get_device(self.items[item_index]).brand_name} "
            f"{self.get_device(self.items[item_index]).model_name} \n"
            f"({readable_data_from_datetime_inline(self.items[item_index].added)})",
            callback_data=callback_data)
