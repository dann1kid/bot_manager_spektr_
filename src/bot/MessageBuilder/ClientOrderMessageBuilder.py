"""

    Type of message builder for PeeWee3

    Usage:
        # single
        order = peeweeModel.get(...)
        message = ClientOrderMessageBuilder(order)
        # cycle
        orders = peeweeModel.select()...
        for order in orders:
            send_message(text=ClientOrderMessageBuilder(order))

"""
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.models.pw_models import Client, Order
from src.bot.resources import strings
from src.bot.utils.timestamp_parses import readable_data_from_datetime


class ClientOrderMessageBuilder:

    def __init__(self, order: Order = None):
        self.order = order
        self.add_data = readable_data_from_datetime(self.order.added)
        self.client = Client.get(Client.item_id == order.client)

    def create(self):
        message = f'<b>№ заказа</b>: {self.order.item_id}{self.client.workspace_geo} \n' \
                  f'Добавлен: {self.add_data} \n' \
                  f'<b>Статус</b>: {StatusOrderMenu[self.order.status].value} \n' \
                  f' \n' \
                  f'<b><i>Информация об аппарате</i></b>\n' \
                  f'<i>Производитель</i>: {self.order.devices[0].brand_name if self.order.devices.count() > 0 else strings.txt_no_data} \n' \
                  f'<i>Модель</i>: {self.order.devices[0].model_name if self.order.devices.count() > 0 else strings.txt_no_data} \n' \
                  f'<i>Целевая проблема</i>: {self.order.trouble} \n' \
                  f'<i>Цена работы</i>: {self.order.price} \n' \
                  f'<i>Предоплата</i>: {self.order.prepayment} \n'

        return message
