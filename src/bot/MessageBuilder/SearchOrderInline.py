import pprint
from dataclasses import dataclass

from peewee import TextField, fn, SQL, JOIN
from telegram_bot_pagination import InlineKeyboardButton

from src.bot.MessageBuilder.ItemsInline import ItemsInline
from src.bot.MessageBuilder.OrderMessageBuilder import OrderMessageBuilder
from src.bot.entry_points.CallbackEntry import CallbackParser
from src.bot.models.pw_models import Order, Client, CommentOrder, ContactClient, Device
from src.bot.resources import strings


class SearchOrdersInline(ItemsInline):
    """
    Makes repair orders inline paginator menu.
        params:
        query: str:
        chat_id: str | int: users chat_id
        page: int: default 1

        Result: object with set of inline buttons with callbacks.
            Every callback calls showing Order with containing string
            in Order or its constrains by select query in `self.items` field.
    """

    def __init__(self, search_query=None, page: int = 1):
        self.page = int(page)  # С апдейтов всегда идет строка.
        self.query = search_query
        self.items = None
        self.query_list = self.query.query_text.split()
        self.status = "search_orders"
        self.data = f"{CallbackParser.paginator_gen_order_search(query=self.query)}"
        print("orders search patter + data", self.data)
        self.searching_for()
        self.make_keyboard()

    def searching_for(self):
        selection = Order.select() \
            .join_from(Order, CommentOrder, join_type=JOIN.FULL_OUTER) \
            .join(ContactClient, on=(ContactClient.client == Order.client)) \
            .join_from(Order, Client, join_type=JOIN.FULL_OUTER) \
            .join_from(Order, Device, join_type=JOIN.FULL_OUTER)

        queries = []
        for word in self.query_list:
            print("type self.query_list", type(self.query_list))
            try:
                word = int(word)
            except ValueError:
                print("except type word", type(word))
                queries.append(
                    selection.where(
                        Order.item_id.cast('text').like(f"%{word}%") |
                        CommentOrder.comment.ilike(f"%{word}%") |
                        ContactClient.number.ilike(f"%{word}%") |
                        Order.trouble.ilike(f"%{word}%") |
                        Device.brand_name.ilike(f"%{word}%") |
                        Device.model_name.ilike(f"%{word}%") |
                        Device.color.ilike(f"%{word}%") |
                        Device.imei.ilike(f"%{word}%") |
                        Client.name.ilike(f"%{word}%") |
                        Client.surname.ilike(f"%{word}%")

                        # Order.item_id.cast('text') == f"%{word}%"
                        # Order.item_id.cast('text').like(f"%{word}%")
                        # Order.item_id.cast(TextField()).ilike(f"%{word}%")
                    ).distinct()
                )
            else:
                print("else type number", type(word))
                queries.append(
                    selection.where(
                        (Order.item_id == word) |
                        CommentOrder.comment.ilike(f"%{word}%") |
                        ContactClient.number.ilike(f"%{word}%") |
                        Order.trouble.ilike(f"%{word}%") |
                        Device.brand_name.ilike(f"%{word}%") |
                        Device.model_name.ilike(f"%{word}%") |
                        Device.color.ilike(f"%{word}%") |
                        Device.imei.ilike(f"%{word}%") |
                        Client.name.ilike(f"%{word}%") |
                        Client.surname.ilike(f"%{word}%")

                        # Order.item_id.cast('text') == f"%{word}%"
                        # Order.item_id.cast('text').like(f"%{word}%")
                        # Order.item_id.cast(TextField()).ilike(f"%{word}%")
                    ).distinct()
                )

        print('query list', self.query_list, len(self.query_list),)

        for i, query in enumerate(queries):
            if i == 0:
                self.items = query
            else:
                self.items = self.items & query

        self.items = self.items.order_by(Order.item_id)
        print("items", self.items)

        # по сути нам нужно собрать только orders
        # почему бы просто не пройтись по всем полям и констрейнтам?

    @dataclass
    class InlineDevice:
        brand_name = strings.txt_no_data
        model_name = strings.txt_no_data

    def make_callback_data(self, item_index):
        #
        return CallbackParser.make_paginator_order_search(
            order_id=self.items[item_index].item_id,
            search_id=self.query.item_id,
            page=self.page,
            mode=OrderMessageBuilder.BuilderMode.search.value,
        )

    def get_device(self, item_index):
        try:
            device = [x for x in item_index.devices][0]
        except IndexError:
            device = self.InlineDevice()
        return device

    def make_inline_item(self, item_index, callback_data):
        return InlineKeyboardButton(
            f"№{self.items[item_index].item_id} "
            f"{self.get_device(self.items[item_index]).brand_name} "
            f"{self.get_device(self.items[item_index]).model_name} ",
            callback_data=callback_data)
