from telegram_bot_pagination import InlineKeyboardButton

from src.bot.MessageBuilder.ItemsInline import ItemsInline
from src.bot.MessageBuilder.UserMessageBuilder import UserMessageBuilder
from src.bot.entry_points.CallbackEntry import CallbackParser
from src.bot.models.pw_models import Client


class UsersInline(ItemsInline):
    def __init__(self, page: int = 1, mode: int = 0):
        self.page = int(page)
        self.mode = mode
        self.items = None
        self.items = Client.select().order_by(Client.item_id)
        self.data = f"{CallbackParser.paginator_gen_user()}"
        self.make_keyboard()

    def make_callback_data(self, item_index):
        return CallbackParser.make_paginator_users(
            client_id=self.items[item_index].item_id,
            page=self.page,
            mode=UserMessageBuilder.BuilderMode.all.value,
        )

    def make_inline_item(self, item_index, callback_data):
        return InlineKeyboardButton(
            f"№{self.items[item_index].item_id} "
            f"{self.items[item_index].name} "
            f"{self.items[item_index].surname}",
            callback_data=callback_data)
