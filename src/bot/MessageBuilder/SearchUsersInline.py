from telegram_bot_pagination import InlineKeyboardButton

from src.bot.MessageBuilder.ItemsInline import ItemsInline
from src.bot.MessageBuilder.UserMessageBuilder import UserMessageBuilder
from src.bot.entry_points.CallbackEntry import CallbackParser
from src.bot.models.pw_models import Client, ContactClient


class SearchUsersInline(ItemsInline):
    """
    Makes repair orders inline paginator menu.
        params:
        query: str:
        chat_id: str | int: users chat_id
        page: int: default 1

        Result: object with set of inline buttons with callbacks.
            Every callback calls showing Order with containing string
            in Order or its constrains by select query in `self.items` field.
    """

    def __init__(self, search_query=None, page: int = 1):
        self.page = int(page)  # С апдейтов всегда идет строка.
        self.query = search_query
        self.items = None
        self.query_list = search_query.query_text.split()
        self.data = f"{CallbackParser.paginator_gen_user_search(query=self.query)}"
        self.searching_for()
        self.make_keyboard()

    def searching_for(self):
        selection = Client.select()\
                     .join(ContactClient, on=(ContactClient.client == Client.item_id))

        queries = (
            selection.where(
                Client.surname.ilike(f"%{word}%") |
                Client.name.ilike(f"%{word}%") |
                Client.middle_name.ilike(f"%{word}%") |
                ContactClient.number.ilike(f"%{word}%")
            )
            .distinct()

            for word in self.query_list

        )

        for i, query in enumerate(queries):
            if i == 0:
                self.items = query
            else:
                self.items = self.items & query

        self.items = self.items.order_by(Client.item_id)
        print("items", self.items)

    def make_callback_data(self, item_index):
        return CallbackParser.make_paginator_user_search(
            client_id=self.items[item_index].item_id,
            search_id=self.query.item_id,
            page=self.page,
            mode=UserMessageBuilder.BuilderMode.search.value,
        )

    def make_inline_item(self, item_index, callback_data):
        return InlineKeyboardButton(
            f"№{self.items[item_index].item_id} "
            f"{self.items[item_index].name} "
            f"{self.items[item_index].surname}",
            callback_data=callback_data)
