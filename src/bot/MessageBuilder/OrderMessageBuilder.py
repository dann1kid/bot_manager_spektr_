"""
    Common class for order messages. Build it from ready samples as patterns
"""
import ast
from enum import Enum

from telegram.error import BadRequest

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.config_bot import web_preview_api
from src.bot.entry_points.CallbackEntry import CallbackEntry, CallbackParser
from src.bot.models.pw_models import Order, Client, ContactClient
from src.bot.resources import strings
from src.bot.utils.message import mult_inline_keyboard
from src.bot.utils.timestamp_parses import readable_data_from_datetime


class OrderMessageBuilder:
    """
    Describes info and buttons on tap InlineOrder buttons.
    order: Order: Order object for accessing fields.
    users_chat_id: int | str: users chat id whoever taking info for security access
    mode: BuildMode

    """

    class BuilderMode(Enum):
        status = 1
        search = 2

    def __init__(self, order: Order = None, users_chat_id=0, mode: BuilderMode = BuilderMode.status, update=None,
                 context=None):
        self.order = order
        self.users_chat_id = users_chat_id
        self.mode = mode
        self.update = update
        self.context = context
        self.add_data = readable_data_from_datetime(self.order.added)
        self.client = Client.get(Client.item_id == order.client)
        self.numbs: str = self.client.numbs.order_by(ContactClient.added)[
            0].number
        self.staff_user: Client = Client.get(Client.chat_id == users_chat_id)
        self.sum_details_price: int = sum([x.price for x in
                                           self.order.details_order]) if self.order.details_order.count() > 0 else 0

    # message = OrderMessageBuilder(Order.get(Order.item_id == data["order"])).create_message()
    def create_message(self):
        message = f'<b>№ заказа</b>: {self.order.item_id}\n' \
                  f'Добавлен: {self.add_data}, {self.order.creator.name}\n' \
                  f'Предварительная цена: {self.order.predefined_price}\n' \
                  f'Ориентировочная дата: {self.order.oriented_timestamp}\n' \
                  f'\n<b>Статус</b>: {StatusOrderMenu[self.order.status].value} \n\n' \
                  f'{strings.txt_notice_awaiting_detail if self.order.details_order.count() > 0 else strings.txt_notice_no_awaiting_detail} \n' \
                  f'<i>Запчасти: </i>{self.order.details_order.count()}\n' \
                  f'<i>Суммарная стоимость запчастей: </i>{self.sum_details_price}' \
                  f' \n' \
                  f'\n<b><i>Информация о заказе</i></b>\n' \
                  f'<i>Производитель</i>: {self.order.devices[0].brand_name if self.order.devices.count() > 0 else strings.txt_no_data} \n' \
                  f'<i>Модель</i>: {self.order.devices[0].model_name if self.order.devices.count() > 0 else strings.txt_no_data} \n' \
                  f'<i>Целевая проблема</i>: {self.order.trouble}' \
                  f' \n' \
                  f'\n<b><i>Информация об оплате</i></b>\n' \
                  f'<i>Договоренная цена работы</i>: {self.order.price} \n' \
                  f'<i>Цена работы</i>: {self.order.price} \n' \
                  f'<i>Предоплата</i>: {self.order.prepayment}' \
                  f'<i>Оплата</i>: {self.order.payment} \n' \
                  f' \n' \
                  f'<b><i>Информация о клиенте</i></b>\n' \
                  f'<i>Идентификатор клиента</i>: {self.order.client.item_id} \n' \
                  f'<i>Имя клиента</i>: {self.client.name} {self.client.surname}\n' \
                  f'<i>Последний номер клиента</i>: {self.numbs}\n' \
                  + self.add_comments() \
                  + self.add_courier() \
                  + self.add_delivery_info() \
                  + self.add_chat_info() \
                  # + self.add_photo()

        return message

    def add_chat_info(self):

        if self.order.status == StatusOrderMenu.wc.name:

            if self.staff_user.staff_status == StaffLevel.courier.value:
                if self.order.client.chat_id:
                    try:
                        contact = self.context.bot.get_chat(self.order.client.chat_id)
                        # здесь заменить на отправить контакт юзера telegram.Contact send_contact
                        self.context.bot.send_contact(chat_id=self.users_chat_id,
                                                      phone_number=self.client.numbs.order_by(ContactClient.added)[
                                                          0].number,
                                                      first_name=self.client.name)
                        return f'<i>Чат клиента</i>: {contact.username if contact.username is not None else strings.txt_no_username}\n'
                    except BadRequest:
                        return f'<i>Чат клиента</i>: чат клиента не обнаружен'
                else:
                    return f'<i>Чат клиента</i>: не доступно\n'

            elif self.staff_user.staff_status == StaffLevel.client.value:
                self.context.bot.send_contact(chat_id=self.users_chat_id,
                                              phone_number=self.order.courier.numbs.order_by(ContactClient.added)[
                                                  0].number,
                                              first_name=self.client.name)
                return f'<i>Контактный номер курьера</i>: {self.order.courier.numbs[-1].number}\n'

            else:
                return ""

        return ""

    def add_delivery_info(self):

        if self.order.status in [StatusOrderMenu.iw.name, StatusOrderMenu.wc.name, StatusOrderMenu.wcc.name]:

            if self.order.geo_check:
                geos = [geo.geo_pos for geo in self.order.geopositions]
                self.context.bot.send_location(chat_id=self.users_chat_id,
                                               latitude=ast.literal_eval(geos[0])["latitude"],
                                               longitude=ast.literal_eval(geos[0])["longitude"],
                                               reply_to_message_id=self.update.callback_query.message.message_id
                                               )

            if self.order.address_check:
                addresses = [address.address for address in self.order.addresses]
                return f'<i>Адрес доставки</i>: {addresses[-1]}\n'

            else:
                return ""

        else:
            return ""

    def add_photo(self):
        if len(self.order.photos) > 0:
            file_id = self.order.photos[0].pic_id
            print(f'<a href="{web_preview_api}/img/{file_id}.jpg">фото</a>\n')
            # return f'<a href="{web_preview_api}/img/{file_id}.jpg">фото</a>\n'
            return f'<a href="{web_preview_api}/img/00011-1390769062.png">фото</a>\n'
            # return f'<a href="127.0.0.1:8888/{file_id}.jpg">фото</a>\n'
            # return f'<a href="https://cdn.discordapp.com/attachments/1041973306775064606/1076973007202955425/00015-1616223381.png">фото</a>\n'
            # return f'<a href="https://sun9-west.userapi.com/sun9-10/s/v1/ig2/Uz5BxaoCPt7WZCX4g_lf17l2Y0eiAMpMQKaznOY_IYv87taoRbHOCo0bNOxSqdAf53iOjjkNMOBSHmIfV_SQK1PU.jpg?size=1287x1196&quality=96&type=album">фото</a>\n'

        else:
            return ""

    def add_comments(self):
        if self.staff_user.staff_status >= StaffLevel.receiver.value:
            if self.order.comments.count() == 1:
                return f'<i>Комментарий</i>: {self.order.comments[-1].comment}\n'
            elif self.order.comments.count() > 1:
                return f'<i>Комментарий</i>: {self.order.comments[0].comment}\n' \
                       f'<i>Последний комментарий</i>: {self.order.comments[-1].comment} \n' \
                       f'<i>Комментариев сейчас</i>: {self.order.comments.count()}\n'
            else:
                return ""
        else:
            return ""

    def add_courier(self):
        if self.order.courier:
            if self.order.courier.name:
                return f'\n<b><i>Доставка</i></b>:\n' \
                       f'<i>Текущий курьер</i>: {self.order.courier.name}\n'
            else:
                return f'\n<b><i>Доставка</i></b>:\n' \
                       f'<i>Текущий курьер</i>: {self.order.courier.numbs[-1].number}\n'

        else:
            return ""

    def sum_details_price(self):
        ...

    def create_buttons_inline(self, data):
        """
        Метод должен учитывать контекст (статус заказа, статус юзера)
        """
        # staff
        match self.staff_user.staff_status:
            case StaffLevel.receiver.value:
                return self.build_receiver_buttons(data)
            case StaffLevel.client.value:
                return self.build_client_buttons(data)
            case StaffLevel.courier.value:
                return self.build_courier_buttons(data)
            case StaffLevel.admin.value:
                return self.build_receiver_buttons(data)
            case _:
                pass

    def build_receiver_buttons(self, data=None):
        """Построение пользовательских кнопок
        Контекст юзер -> статус заказа
        """

        if not data:
            data = CallbackParser.make_paginator_order(self.order.item_id,
                                                       self.order.status,
                                                       0,
                                                       self.mode,
                                                       )
        else:
            print("OrderMessageBuilder data ", data)

        """
        в общем  идея такая - передавать режим, но суть перехода одна и та же. По сути один обьект меню, который 
        показывает разные "окна". Вопрос, как сохранять переход и контекст, чтобы не писать кучу одинакового кода с 
        одной стороны, с другой стороны, чтобы не ломалось когда появляется очередной режим или прочие метаэлементы
        
        можно передавать режим и дальше уже не ебет, пусть дочерние колбек парсеры меняют свое поведение и пох что все будут поломаны,
        либо учесть имеющиеся парсеры и внедрить метаэлемент что бы он не поломал все зависимые парсеры,
        либо учитывая текущие элементы написать класс парсера, который потребуется написать и внедрить вместо имеющихся парсеров
        Последний вариант самый лучший, но подойдет ли он под все варианты или нужно будет изменять имеющуюся абстракцию
        
        """
        reply_markup = []
        match self.order.status:

            case StatusOrderMenu.oe.name:

                # add_comment
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_order.value + data))

                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_order.value + data))

                # show pics
                reply_markup.append((strings.btn_show_pics,
                                     CallbackEntry.show_pics_order.value + data))

                # eval
                reply_markup.append(
                    (strings.btn_evaluate,
                     CallbackEntry.evaluate_order.value + data))

            case StatusOrderMenu.wcc.name:
                # add_comment
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.evaluate_order.value + data))

                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_order.value + data))
                ...

            case StatusOrderMenu.odw.name:
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_order.value + data))
                ...

            case StatusOrderMenu.iw.name:
                # инлайн кнопки: метка паттерна + данные с заказа

                # add_comment
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_order.value + data))

                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_order.value + data))

                # add_detail_order
                reply_markup.append(
                    (strings.txt_add_detail_order,
                     CallbackEntry.add_detail.value + data))

                # add services
                reply_markup.append((strings.txt_add_add_service,
                                     CallbackEntry.add_service_order.value + data))

                # Напечатать наклейки
                reply_markup.append((strings.btn_print_labels,
                                     CallbackEntry.staff_print_labels.value + data))

                # show menu sub-details
                # todo add menu with inline buttons "delete"
                reply_markup.append((strings.txt_show_details,
                                     CallbackEntry.show_details_order.value + data))

                # change price
                reply_markup.append(
                    (strings.txt_change_price_order,
                     CallbackEntry.change_price.value + data))

                # select extradition
                reply_markup.append((strings.txt_select_extradition_type,
                                     CallbackEntry.select_delivery_type_order.value + data))  # extradite_order

                """
                # send to extradition (through conversation handler)
                reply_markup.append((strings.txt_sent_to_extradition,
                                     CallbackEntry.send_to_extradition_order.value + data))
                                    """

            case StatusOrderMenu.wsd.name:
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_order.value + data))  # add comm
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_order.value + data))  # show comm
                reply_markup.append((strings.txt_select_extradition_type,
                                     CallbackEntry.select_delivery_type_order.value + data))  # extradite_order

            case StatusOrderMenu.sd.name:
                # todo изменить кнопки на контекст
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_order.value + data))  # add comm
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_order.value + data))  # show comm
                reply_markup.append(
                    (strings.txt_change_price_order,
                     CallbackEntry.change_price.value + data))  # change price
                reply_markup.append(
                    (strings.txt_to_extradite,
                     CallbackEntry.extradite_order.value + data))  # extradite order dial

            case StatusOrderMenu.dwc.name:
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_order.value + data))

                # transmit order to courier
                reply_markup.append((strings.transmit_order_from_store_to_courier,
                                     CallbackEntry.transmit_order_from_work_to_courier.value + data))
                ...

            case StatusOrderMenu.dc.name:
                # show_comment
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_order.value + data))
                ...

            case StatusOrderMenu.fin.name:
                reply_markup.append((strings.txt_show_comment,
                                     CallbackEntry.show_comments_order.value + data))  # show comm

            case _:
                ...

        match self.mode:
            case self.BuilderMode.status:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_order.value + data))
            case self.BuilderMode.search:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_order_search.value + data))

        return mult_inline_keyboard(reply_markup)

    def build_client_buttons(self, data):
        """Построение пользовательских кнопок
                Контекст юзер -> статус заказа
                """

        if not data:
            data = CallbackParser.make_paginator_order(self.order.item_id,
                                                       self.order.status,
                                                       0,
                                                       self.mode,
                                                       )
        else:
            print("OrderMessageBuilder data ", data)

        reply_markup = []
        match self.order.status:

            case StatusOrderMenu.oe.name:
                # отменить заказ
                reply_markup.append((strings.btn_cancel_order,
                                     CallbackEntry.client_discard_order.value + data))
                ...

            case StatusOrderMenu.wcc.name:
                reply_markup.append((strings.btn_cancel_order,
                                     CallbackEntry.client_discard_order.value + data))
                ...

            case StatusOrderMenu.wdc.name:
                reply_markup.append((strings.btn_cancel_order,
                                     CallbackEntry.client_discard_order.value + data))
                # подтвердить заказ
                reply_markup.append((strings.btn_commit_order,
                                     CallbackEntry.client_commit_order.value + data))

                # add comm
                reply_markup.append((strings.txt_add_comment,
                                     CallbackEntry.add_comment_order.value + data))
                ...

            case StatusOrderMenu.wc.name:
                reply_markup.append((strings.btn_cancel_order,
                                     CallbackEntry.client_discard_order.value + data))
                ...

            case StatusOrderMenu.odw.name:
                reply_markup.append((strings.btn_cancel_order,
                                     CallbackEntry.client_discard_order.value + data))
                ...

            case StatusOrderMenu.iw.name:
                ...

            case StatusOrderMenu.wsd.name:
                ...

            case StatusOrderMenu.sd.name:
                ...

            case StatusOrderMenu.dwc.name:
                ...

            case StatusOrderMenu.dc.name:
                ...

            case StatusOrderMenu.fin.name:
                ...

            case _:
                ...

        match self.mode:
            case self.BuilderMode.status:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_order.value + data))
            case self.BuilderMode.search:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_order_search.value + data))

        return mult_inline_keyboard(reply_markup)

    def build_courier_buttons(self, data):
        """Построение пользовательских кнопок
                Контекст юзер -> статус заказа
                """

        if not data:
            data = CallbackParser.make_paginator_order(self.order.item_id,
                                                       self.order.status,
                                                       0,
                                                       self.mode,
                                                       )
        else:
            print("OrderMessageBuilder data ", data)

        """
        какие кнопки нужны для курьера
        """
        reply_markup = []
        match self.order.status:

            case StatusOrderMenu.oe.name:
                ...

            case StatusOrderMenu.wdc.name:
                ...

            case StatusOrderMenu.wcc.name:
                reply_markup.append((strings.txt_pickup_order,
                                     CallbackEntry.courier_pickup_order_from_client.value + data))
                ...

            case StatusOrderMenu.wc.name:
                # отказ от прибытия на доставку
                reply_markup.append((strings.txt_cancel_pickup_order,
                                     CallbackEntry.courier_cancel_pickup_order_from_client.value + data))

                # запрос на передачу заказа от клиента курьеру
                reply_markup.append((strings.btn_get_order_from_client,
                                     CallbackEntry.courier_get_order_from_client.value + data))

                # в случае отказа клиента
                reply_markup.append((strings.btn_client_not_takes,
                                     CallbackEntry.courier_client_didnt_take_order.value + data))
                ...

            case StatusOrderMenu.odw.name:
                reply_markup.append((strings.btn_transmit_order_from_courier_to_work,
                                     CallbackEntry.transmit_order_to_receiver_from_courier.value + data))
                ...

            case StatusOrderMenu.iw.name:
                ...

            case StatusOrderMenu.wsd.name:
                ...

            case StatusOrderMenu.sd.name:
                ...

            case StatusOrderMenu.dwc.name:
                ...

            case StatusOrderMenu.wp.name:
                reply_markup.append((strings.btn_take_payment,
                                     CallbackEntry.courier_take_payment.value + data))
                ...

            case StatusOrderMenu.dc.name:
                reply_markup.append((strings.btn_transmit_order_from_courier_to_client,
                                     CallbackEntry.transmit_order_to_client_from_courier.value + data))
                ...

            case StatusOrderMenu.fin.name:
                ...

            case _:
                ...

        match self.mode:
            case self.BuilderMode.status:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_order.value + data))
            case self.BuilderMode.search:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_order_search.value + data))

        return mult_inline_keyboard(reply_markup)
