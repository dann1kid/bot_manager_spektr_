"""

    Type of message builder for PeeWee3

    Usage:
        # single
        order = peeweeModel.get(...)
        message = ClientOrderMessageBuilder(order)
        # cycle
        orders = peeweeModel.select()...
        for order in orders:
            send_message(text=ClientOrderMessageBuilder(order))

"""
from enum import Enum

from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.models.pw_models import Client, Order
from src.bot.resources import strings
from src.bot.utils.message import mult_inline_keyboard


class UserMessageBuilder:
    class BuilderMode(Enum):
        all = 1
        search = 2
        # возможно потребуется еще один параметр для выбора в режиме создания заказа

    def __init__(self, client: Client = None, mode: BuilderMode = None):
        self.client = Client.get(Client.item_id == client.item_id)
        self.mode = mode

    def create_message(self):
        message = f'<b>Фамилия</b>: {self.client.surname} \n' \
                  f'<b>Имя</b> {self.client.name} \n' \
                  f'<b>Отчество</b>: {self.client.middle_name} \n'

        return message

    def create_buttons_inline(self, data):
        """
        Метод должен учитывать контекст (статус заказа, статус юзера)
        """
        return self.build_admin_edit_user_buttons(data)

    def build_admin_edit_user_buttons(self, data):
        reply_markup = []
        reply_markup.append((strings.btn_change_user_data,
                             CallbackEntry.change_user.value + data))
        match self.mode:
            case self.BuilderMode.all.value:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_user.value + data))
            case self.BuilderMode.search.value:
                reply_markup.append((strings.txt_back, CallbackEntry.back_show_user_search.value + data))

        return mult_inline_keyboard(reply_markup)
