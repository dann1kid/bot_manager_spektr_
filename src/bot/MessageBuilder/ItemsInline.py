from abc import abstractmethod

from telegram_bot_pagination import InlineKeyboardPaginator


class ItemsInline:
    """
    Абстрактный класс для создания меню с вложенными кнопками совместно
    с библиотекой telegram_bot_pagination.
    Означает объект меню с обозначенными кнопками и указанными колбеками,
    с размерностью страницы == page_divider, текущей страницей page.

    Поля, требующие переопределения:
        self.page: int: текущая страница
        self.status: str
        self.items: объект Peewee (иначе - класс запроса с полями).
        self.data: str: колбек для пагинации объекта меню.
            (data = f"{CallbackParser.paginator_gen_order(status=self.status)}"

    Поля, которые можно переопределить:
        self.page_divider: int: Делитель страницы

    Методы, требующие переопределения:
        make_callback_data()
        make_inline_item()

    Пример:
    class OrdersInline(ItemsInline):
    def __init__(self, status: str = "", page: int = 1):
        self.page = int(page)  # С апдейтов всегда идет строка.
        self.status = status
        self.items = Order.select().where(Order.status == self.status)
        self.data = f"{CallbackParser.paginator_gen_order(status=self.status)}"

        try:
            self.make_keyboard()
        except Exception as e:
            print(e)

    def make_callback_data(self, item_index):
        return CallbackParser.make_paginator_order(
            order_id=self.items[item_index].order_id,
            status=self.items[item_index].status,
            page=self.page,
        )

    def make_inline_item(self, item_index, callback_data):
        return InlineKeyboardButton(
            f"№{self.items[item_index].order_id} "
            f"{self.items[item_index].brand_name} "
            f"{self.items[item_index].model_name} ",
            callback_data=callback_data)

    """

    page: int = 1
    status: str = ""
    items: any = None
    pages: int = 1
    shift_page: int = 1
    last_item: int = 0
    calculated_divide: int = 0
    page_divider = 7
    data: str = ""
    paginator: InlineKeyboardPaginator = None

    def calculate_pages(self):
        """Рассчитывает количество страниц пагинатора"""
        calculated_divide = self.items.count() % self.page_divider
        if calculated_divide > 0:
            self.calculated_divide = calculated_divide
            self.pages = self.items.count() // self.page_divider + 1
        else:
            self.pages = self.items.count() // self.page_divider

    def calculate_page_shift(self):
        """Считает смещение страницы,
        требуемое для корректного показа страниц."""

        if self.items.count() < self.page_divider:
            self.shift_page = 0
        else:
            self.shift_page = self.page - 1

    def calculate_last_item(self):
        """Рассчитывает последний элемент. Требуется для итератора
        от начала страницы до последнего элемента для рассчитываемой страницы.
        Если элементов больше чем на одну страницу, то последний элемент кратен
        делителю страниц.
        Иначе до н элемента.
        """

        if self.items.count() < self.page_divider:
            self.last_item = self.calculated_divide
        else:
            self.last_item = self.page_divider

    def make_paginator(self):
        """
        Метод создает начальный объект меню, поля которого в будущем изменятся
        """
        self.paginator = InlineKeyboardPaginator(
            page_count=self.pages,
            current_page=self.page,
            data_pattern=self.data + "#{page}")

    def make_paginator_buttons(self):
        """
                Метод создает вертикальные инлайн кнопки с номером заказа
                в диапазоне (текущий делитель/количество заказов на страницу от нуля) до
                (делитель/количество заказов на страницу от нуля + количество заказов на
                страницу).

                Если в списке есть 15 элементов, то он создаст 2 страницы, на второй
                будут с 11 по 15 элементов.
                Если в списке есть 21 элемент, то создаст 3 страницы, на 3 странице
                будет 21 расположен элемент одной кнопкой.
                Один элемент состоит из InlineKeyboardButton и CallbackData:

                callback_data = CallbackParser.make_paginator_order(
                            order_id=self.items[order_position].order_id,
                            status=self.items[order_position].status,
                            page=self.page,
                        )
                self.paginator. \
                    add_before(InlineKeyboardButton(
                    f"№{self.items[order_position].order_id} "
                    f"{self.items[order_position].brand_name} "
                    f"{self.items[order_position].model_name} ",
                    callback_data=callback_data))

                """

        for item_index in range(self.shift_page * self.page_divider,
                                self.shift_page * self.page_divider + self.last_item):
            try:
                callback_data = self.make_callback_data(item_index)
                self.paginator.add_before(
                    self.make_inline_item(item_index, callback_data))
            except IndexError:
                break

    @abstractmethod
    def make_callback_data(self, item_index):
        """Абстрактный метод для производства колбек дата для
        каждой инлайн кнопки

        Пример:
         return CallbackParser.make_paginator_order(
            order_id=self.items[item_index].order_id,
            status=self.items[item_index].status,
            page=self.page,
        )
        """

    @abstractmethod
    def make_inline_item(self, item_index, callback_data):
        """Абстрактный метод для производства инлайн элемента

        Пример:
        return InlineKeyboardButton(
            f"№{self.items[item_index].order_id} "
            f"{self.items[item_index].brand_name} "
            f"{self.items[item_index].model_name} ",
            callback_data=callback_data)
        """

    def make_keyboard(self):
        """Вызывается в конце создания дочернего класса для производства
        инлайн клавиатуры
        """
        self.calculate_pages()
        self.calculate_page_shift()
        self.calculate_last_item()
        self.make_paginator()
        self.make_paginator_buttons()
