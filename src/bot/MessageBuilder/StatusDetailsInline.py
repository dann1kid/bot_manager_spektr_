from telegram_bot_pagination import InlineKeyboardButton

from src.bot.MessageBuilder.DetailMessageBuilder import DetailMessageBuilder
from src.bot.MessageBuilder.ItemsInline import ItemsInline
from src.bot.entry_points.CallbackEntry import CallbackParser
from src.bot.models.pw_models import Detail


class StatusDetailsInline(ItemsInline):
    """Класс для выдачи построчно деталей"""

    def __init__(self, status: str = "", page: int = 1, mode: int = 0):
        self.page = int(page)  # С апдейтов всегда идет строка.
        self.status = status
        self.mode = mode
        self.items = Detail.select().where(Detail.status ==
                                           self.status).order_by(Detail.item_id)
        self.data = f"{CallbackParser.paginator_gen_detail(status=self.status)}"
        self.make_keyboard()

    def make_callback_data(self, item_index):
        return CallbackParser.make_paginator_detail(
            detail_id=self.items[item_index].item_id,
            status=self.items[item_index].status,
            page=self.page,
            mode=DetailMessageBuilder.BuilderMode.status.value,
        )

    def make_inline_item(self, item_index, callback_data):
        return InlineKeyboardButton(
            f"№{self.items[item_index].item_id} "
            f"{self.items[item_index].detail_name} ",
            callback_data=callback_data)
