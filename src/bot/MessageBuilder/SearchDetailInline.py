from telegram_bot_pagination import InlineKeyboardButton

from src.bot.MessageBuilder.DetailMessageBuilder import DetailMessageBuilder
from src.bot.MessageBuilder.ItemsInline import ItemsInline
from src.bot.entry_points.CallbackEntry import CallbackParser
from src.bot.models.pw_models import Order, Client, ContactClient, CommentDetail, \
    Detail


class SearchDetailsInline(ItemsInline):
    """
    Makes repair orders inline paginator menu.
        params:
        query: str:
        chat_id: str | int: users chat_id
        page: int: default 1

        Result: object with set of inline buttons with callbacks.
            Every callback calls showing Order with containing string
            in Order or its constrains by select query in `self.items` field.
    """

    def __init__(self, search_query=None, page: int = 1):
        self.page = int(page)  # С апдейтов всегда идет строка.
        self.query = search_query
        self.items = None
        self.query_list = search_query.query_text.split()
        self.status = "search_details"
        self.data = f"{CallbackParser.paginator_gen_detail_search(query=self.query)}"
        print("details search patter + data", self.data)
        self.searching_for()
        self.make_keyboard()

    def searching_for(self):
        selection = Detail.select() \
            .join(CommentDetail, on=(CommentDetail.detail == Detail.item_id)) \
            .join(ContactClient, on=(ContactClient.client == Detail.client)) \
            .join(Client, on=(Client.item_id == Detail.client))

        queries = (
            selection.where(
                Detail.text.ilike(f"%{word}%") |
                Detail.price.cast('text').contains(word) |
                Detail.item_id.cast('text').contains(word) |
                Detail.prepayment.cast('text').contains(word) |
                Detail.payment.cast('text').contains(word) |
                CommentDetail.comment.ilike(f"%{word}%") |
                Client.surname.ilike(f"%{word}%") |
                Client.name.ilike(f"%{word}%") |
                ContactClient.number.ilike(f"%{word}%")
            )
            .distinct()

            for word in self.query_list

        )

        for i, query in enumerate(queries):
            if i == 0:
                self.items = query
            else:
                self.items = self.items & query

        self.items = self.items.order_by(Detail.item_id)
        print("items", self.items)

    def make_callback_data(self, item_index):
        return CallbackParser.make_paginator_detail_search(
            detail_id=self.items[item_index].item_id,
            search_id=self.query.item_id,
            page=self.page,
            mode=DetailMessageBuilder.BuilderMode.search.value,
        )

    def make_inline_item(self, item_index, callback_data):
        return InlineKeyboardButton(
            f"№{self.items[item_index].item_id} "
            f"{self.items[item_index].detail_name} ",
            callback_data=callback_data)
