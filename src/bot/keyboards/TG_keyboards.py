#!/usr/bin/env python
# -*- coding: utf-8 -*-

from peewee import DoesNotExist
from telegram import InlineKeyboardButton
from telegram import InlineKeyboardMarkup
from telegram import KeyboardButton
from telegram import ReplyKeyboardMarkup

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.entry_points.MessageEntry import UserMenu
from src.bot.models.pw_models import Client


def reply_kbd(user_name, user_vk):
    """reply markup keyboard. Markup with message user from"""
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(f"Ответить {user_name}",
                                 callback_data=f"{user_vk} {user_name}")
        ]

        # end of buttons
    ]
    return keyboard


def reply_kbd_orders(order_id):
    """
        Возвращает клавиатуру только для новых заказов

    :param order_id:
    :return:
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(f"Добавить в работу",
                                 callback_data=f"change order_id {order_id}")
        ]

        # end of buttons
    ]
    return InlineKeyboardMarkup(keyboard)


def inline_kbd_order_to_delivery(order_id):
    """
        Возвращает клавиатуру для новых заказов

    :param order_id: идентификатор заказа
    :return:
    """
    keyboard = [
        # 1 line of buttons
        [
            InlineKeyboardButton(f"Перенести заказ в пул выдачи",
                                 callback_data=f"move_to_delivery {order_id}"),
        ],
        # end of buttons
    ]
    return InlineKeyboardMarkup(keyboard)


def inline_kbd_orders_in_work(order_id, work_id):
    """
        Возвращает клавиатуру для заказов в работе

    :param order_id: идентификатор заказа
    :param work_id: идентификатор выполняемой работы
    :return:
    """
    keyboard = [
        # 1 line of buttons
        [
            InlineKeyboardButton(f"Вернуть заказ в список 'готовых к работе'",
                                 callback_data=f"move back {order_id} {work_id}"),
        ],
        # 2 line of buttons
        [
            InlineKeyboardButton(f"Отправить на выдачу",
                                 callback_data=f"on delivery {order_id} {work_id}"),
        ],
        # 3 line of buttons
        [
            InlineKeyboardButton(f"Добавить текущее состояние заказа",
                                 callback_data=f"take_comments {order_id} {work_id}"),
        ],

        # end of buttons
    ]
    return InlineKeyboardMarkup(keyboard)


def inline_kbd_orders_on_delivery(order_id, delivery_id):
    """
        Возвращает клавиатуру для заказов в работе

    :param delivery_id:
    :param order_id: идентификатор заказа
    :return:
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(f"ОПРЕДЕЛИТЬ КАК ВЫДАННЫЙ КЛИЕНТУ!",
                                 callback_data=f"at client {order_id} {delivery_id}"),
        ],
        [
            InlineKeyboardButton(f" ❌ ПРИНЯТЬ ОПЛАТУ ❌",
                                 callback_data=f"take payments {order_id} {delivery_id}"),
        ],

        # end of buttons
    ]
    return InlineKeyboardMarkup(keyboard)


def inline_kbd_orders_on_delivery_ok(order_id, delivery_id):
    """
        Возвращает клавиатуру для заказов в работе

    :param delivery_id:
    :param order_id: идентификатор заказа
    :return:
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(f"ОПРЕДЕЛИТЬ КАК ВЫДАННЫЙ КЛИЕНТУ!",
                                 callback_data=f"at client {order_id} {delivery_id}"),
        ],
        [
            InlineKeyboardButton(f" ✅ ОПЛАТА ПРИНЯТА ✅",
                                 callback_data=f"take payments {order_id} {delivery_id}"),
        ],

        # end of buttons
    ]
    return InlineKeyboardMarkup(keyboard)


def inline_kbd_detail_on_delivery(detail_id):
    """
        Возвращает клавиатуру для заказов в работе

    :param detail_id:
    :return:
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(f"ОПРЕДЕЛИТЬ КАК ВЫДАННЫЙ КЛИЕНТУ!",
                                 callback_data=f"detail_at_client {detail_id}"),
        ],
        # end of buttons
    ]
    return InlineKeyboardMarkup(keyboard)


def make_reply_keyboard(buttons: list):
    """
        Reply pattern keyboard (instead regular).
        pattern generator with 2 buttons at 1 line
    :param buttons: list with resources for buttons
    :return keyboard: object keyboard
    """
    if not buttons:
        return None

    # generate buttons with text='x', callback_data='x' to take reactions from user
    # buttons = [KeyboardButton(f"{x}", callback_data=f"{x}") for x in parameters]
    # generates lines of buttons
    keyboard = []
    line = []
    index = 1

    for button in buttons:
        print(index, len(buttons))

        # iter buttons

        if (index % 2) != 0 and len(buttons) - index != 0:
            # append button to buttons line array
            line.append(button)
            print(line)
            index += 1

        elif (index % 2) == 0 and len(buttons) - index == 0:
            # append array buttons to keyboard
            line.append(button)
            # lines -> keyboard
            keyboard.append(line)
            # take new memory address instead .clean()
            line = []
            index += 1

        else:
            # append array buttons to keyboard
            line.append(button)
            # lines -> keyboard
            keyboard.append(line)
            # take new memory address instead .clean()
            line = []
            index += 1

    return ReplyKeyboardMarkup(keyboard, resize_keyboard=True)


def reply_r_kbd(parameters):
    """
        Reply pattern keyboard (instead regular).
        pattern generator with 2 buttons at 1 line
    :param parameters: list with text for buttons
    :return keyboard: object keyboard
    """

    # generate buttons with text='x', callback_data='x' to take reactions from user
    buttons = [KeyboardButton(f"{x}", callback_data=f"{x}") for x in parameters]
    print(buttons)
    # generate lines of buttons
    keyboard = []
    line = []
    index = 1

    for button in buttons:
        # iter buttons

        if (index % 2) != 0:
            # append button to buttons line array
            line.append(button)
            index += 1

        else:
            # append array buttons to keyboard
            line.append(button)
            # lines -> keyboard
            keyboard.append(line)
            # take new memory address instead .clean()
            line = []
            index += 1

    return ReplyKeyboardMarkup(keyboard, resize_keyboard=True)


def hub_keyboard(chat_id):
    """проверяем chat_id на роль,
        делаем соотвествующую клавиатуру, отправляем с меседжем
        :type chat_id: object: chat_id, по которому идет проверка
        """

    try:
        role = Client.get(Client.chat_id == chat_id)
    except DoesNotExist:
        role = None

    buttons = UserMenu().build_buttons(level=role.staff_status)
    return make_reply_keyboard(buttons)


def inline_kbd_detail_full_search(order_ids: list):
    """
        Возвращает инлайн клавиатуру для заказов в работе
    :param order_ids: список заказов
    :return: InlineKeyboard Object
    """

    keyboard = [

    ]
    for order_id in order_ids:
        message = f"Просмотреть заказ №{order_id}"
        keyboard.append([InlineKeyboardButton(f"{message}",
                                              callback_data=f"full_search {order_id}")])

    return InlineKeyboardMarkup(keyboard)


def inline_kbd_ticket(ticket_id=None):
    """
        Возвращает клавиатуру только для новых тикетов

    :param ticket_id: идентификатор обращения
    :return:
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(f"Ответить № {ticket_id}",
                                 callback_data=f"ticket answer {ticket_id}")
        ]

        # end of buttons
    ]

    return InlineKeyboardMarkup(keyboard)


def inline_kbd_ticket_for_client_send_photo(ticket_id=None):
    """
        Возвращает клавиатуру только для новых заказов

    :param ticket_id: дентификатор обращения
    :return:
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(f"Отправить фото по вопросу №{ticket_id}",
                                 callback_data=f"ticket send photo client {ticket_id}")
        ]

        # end of buttons
    ]

    return InlineKeyboardMarkup(keyboard)


def inline_kbd_ticket_for_client_send_text(ticket_id=None):
    """
        Возвращает клавиатуру только для новых заказов

    :param ticket_id: дентификатор обращения
    :return:
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(
                f"Описать неисправность по вопросу №{ticket_id}",
                callback_data=f"ticket send explain client {ticket_id}")
        ]

        # end of buttons
    ]

    return InlineKeyboardMarkup(keyboard)


def inline_kbd_ticket_for_client_send_model(ticket_id=None):
    """
        Возвращает клавиатуру для определения модели
    :param ticket_id: идентификатор обращения
    :return:
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(
                f"Отправить модель (текст или фото) по вопросу №{ticket_id}",
                callback_data=f"ticket send model client {ticket_id}")
        ]
        # end of buttons
    ]

    return InlineKeyboardMarkup(keyboard)


def inline_kbd_ticket_for_client_send_answer(ticket_id=None):
    """
        Возвращает клавиатуру только для новых заказов

    :param ticket_id: дентификатор обращения
    :return:
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(f"Ответить на запрос №{ticket_id}",
                                 callback_data=f"ticket send explain client {ticket_id}")
        ]

        # end of buttons
    ]

    return InlineKeyboardMarkup(keyboard)


def inline_question_ask_for_delivery_price(order_id=None):
    """
        НЕ НУЖНА
        Возвращает клавиатуру для заказов, отправляющихся на выдачу.

    :param order_id: Идентификатор обращения
    :return: InlineKeyboardMarkup object
    """
    keyboard = [
        # first line of buttons
        [
            InlineKeyboardButton(f"Обозначь для этого заказа  №{order_id} \n"
                                 f"конечную сумму работы!",
                                 callback_data=f"order final price {order_id}")
        ]

        # end of buttons
    ]

    return InlineKeyboardMarkup(keyboard)
