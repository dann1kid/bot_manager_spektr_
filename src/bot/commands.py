def command(self, command):
    """receive command from user and parse"""
    if command in commands:
        return commands.get(command)
    else:
        return "Неправильная команда."