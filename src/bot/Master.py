from User import User


class Master(User):
    """
    Класс админа.
    Держит в памяти сущность, данные которой сохраняются в базе.
    сравнивается при меседж евент для выполнения логики
    Наследует класс User
    """

    def __init__(self, name, client_id):
        User.__init__(self, name, 'master')
        self.client_id = client_id

    def __repr__(self):
        print(f'{self.client_id}, {self.name}, {self.status}, {self.client_id}')
