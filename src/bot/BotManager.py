#!/usr/bin/env python
# -*- coding: utf-8 -*-


# other packages
import logging

from telegram import ParseMode
from telegram.ext import CommandHandler, Updater

from conversations.echo import echo_handler
# conversations
from src.bot import config_bot
from src.bot.config_bot import MAIN_PATH, logging_level
from src.bot.conversations.client.order.client_commit_order import commit_order_conv_handler
from src.bot.conversations.client.order.client_discard_order import handle_cancel_order_by_client
from src.bot.conversations.client.order.client_enumerate_active_orders import \
    active_orders_clients_handler
from src.bot.conversations.client.order.client_enumerate_orders import \
    get_all_orders_clients_handler
from src.bot.conversations.client.order.client_new_order_v2 import \
    order_receive_handler_client
from src.bot.conversations.client.order.help import handler_help
from src.bot.conversations.client.registration.client_registration import \
    client_registration
from src.bot.conversations.staff.detail.callback_entry.receiver_extradite_and_take_payment_detail_callback import \
    get_payments_receiver_detail
from src.bot.conversations.staff.detail.callback_entry.staff_add_comment_detail_callback import \
    callback_handler_add_comment_detail
from src.bot.conversations.staff.detail.callback_entry.staff_delete_detail_callback import \
    handler_delete_detail_callback
from src.bot.conversations.staff.detail.callback_entry.staff_enumerate_comments_detail_callback import \
    handler_enumerate_comments_detail
from src.bot.conversations.staff.detail.callback_entry.staff_extradite_detail import handler_extradite_detail_callback
from src.bot.conversations.staff.detail.callback_entry.staff_mark_as_ordered_detail_callback import \
    handler_mark_detail_as_ordered
from src.bot.conversations.staff.detail.callback_entry.staff_mark_to_extradition_detail_callback import \
    handler_mark_detail_to_extradition
from src.bot.conversations.staff.detail.callback_entry.staff_select_extradition_type_detail import \
    handler_select_delivery_detail
# keyboards
from src.bot.conversations.staff.detail.message_entry.staff_new_detail_v2 import \
    staff_add_detail_CH
from src.bot.conversations.staff.detail.message_entry.staff_search_detail import staff_search_detail
from src.bot.conversations.staff.detail.staff_enumerate_detail_and_callback_v2 import \
    staff_enum_details_v2, callback_handler_switch_page_details, \
    callback_handler_show_detail, callback_handler_show_detail_back, callback_handler_search_detail_back, \
    callback_handler_show_detail_search, callback_handler_switch_page_detail_search
from src.bot.conversations.staff.order.callback_entry.change_price_order import \
    handler_add_update_price_order
from src.bot.conversations.staff.order.callback_entry.courier_cancel_delivery_order import \
    courier_handler_cancel_pickup_order
from src.bot.conversations.staff.order.callback_entry.courier_client_didnt_take_order import \
    handler_client_return_order_to_store
from src.bot.conversations.staff.order.callback_entry.courier_pickup_order import courier_handler_pickup_order
from src.bot.conversations.staff.order.callback_entry.courier_take_device_from_client import \
    send_qr_to_transmit_order_from_client_to_courier
from src.bot.conversations.staff.order.callback_entry.courier_take_payment import handler_take_payment_courier
from src.bot.conversations.staff.order.callback_entry.courier_transmit_order_to_client import \
    send_qr_to_transmit_order_from_courier_to_client
from src.bot.conversations.staff.order.callback_entry.courier_transmit_order_to_store import \
    send_qr_to_transmit_order_from_courier_to_store
from src.bot.conversations.staff.order.callback_entry.receiver_extradite_and_take_payment_order_callback import \
    get_payments_self_delivery_order
from src.bot.conversations.staff.order.callback_entry.staff_add_comment_order_callback import \
    handler_add_comment_order
from src.bot.conversations.staff.order.callback_entry.staff_enumerate_comments_order_callback import \
    handler_enumerate_comments_order
from src.bot.conversations.staff.order.callback_entry.staff_eval_order_callback import receiver_eval_order_conv
from src.bot.conversations.staff.order.callback_entry.staff_extradite_order_callback import \
    handler_extradite_and_take_payment_order
from src.bot.conversations.staff.order.callback_entry.staff_mark_extradition_order_callback import \
    handler_mark_order_to_delivery
from src.bot.conversations.staff.order.callback_entry.staff_on_press_prifile_outside_conversation import \
    handler_on_press_select_profile_outside_conversation
from src.bot.conversations.staff.order.callback_entry.staff_print_stickers import handler_print_labels
from src.bot.conversations.staff.order.callback_entry.staff_select_extradition_type_order import \
    handler_select_delivery_order
from src.bot.conversations.staff.order.callback_entry.staff_show_pics_order import handler_show_all_pics_order
from src.bot.conversations.staff.order.callback_entry.staff_transmit_order_to_courier_from_store import \
    send_qr_to_transmit_order_from_store_to_courier
from src.bot.conversations.staff.order.detail_order.receiver_add_detail_order import \
    handler_add_detail_order
from src.bot.conversations.staff.order.detail_order.staff_enumerate_details_order_callback import \
    handler_enumerate_details_order
from src.bot.conversations.staff.order.message_entry.courier_available_orders import get_all_orders_courier_handler
from src.bot.conversations.staff.order.message_entry.staff_new_order_v2 import \
    order_receive_handler_receiver_v2
from src.bot.conversations.staff.order.message_entry.staff_search_order import staff_search_orders
from src.bot.conversations.staff.order.staff_enumerate_order_and_callback_v2 import \
    staff_enum_orders_v2, \
    callback_handler_show_order, \
    callback_handler_show_order_back, callback_handler_switch_page_order, callback_handler_search_order_back, \
    callback_handler_show_order_search, callback_handler_switch_page_order_search
from src.bot.conversations.staff.utils.callback_entry.staff_change_user import handler_change_users_fields
from src.bot.conversations.staff.utils.message_entry.staff_enumerate_clients_and_callback import staff_enumerate_users, \
    callback_handler_switch_page_user, callback_handler_show_user, callback_handler_show_user_back, \
    callback_handler_show_user_search, callback_handler_search_user_back

from src.bot.conversations.staff.utils.message_entry.staff_send_goods_email import \
    staff_send_goods_email_handler
from src.bot.deeplink_commands.command_start import start_command
from src.bot.keyboards.TG_keyboards import make_reply_keyboard
from src.bot.models.pw_models import create_tables
# project functions
from src.bot.utils.log_exception import error_callback


# built in


def check_base():
    """
    Check base consistent to schema
        """
    create_tables()

    return None


def send_message_to_target(context, chat_id=None, text=None, reply=None):
    """Отправляет сообщение для конкретного chat_id
    :type context: Объект контекста
    :type chat_id: целевой chat_id
    :type text: строка с текстом
    :param reply: inline клавиатура
    """
    context.bot.send_message(chat_id=chat_id, text=text, reply_markup=reply)

    return None


def send_message(update=None, context=None, message=None, reply=None):
    """context based send message for any reason"""
    context.bot.send_message(
        chat_id=update.message.chat_id,
        text=message,
        reply_markup=reply,
        parse_mode=ParseMode.HTML,
    )

    return None


def contacts_kbd():
    get_user_defined_contact = "Поделится номером"
    get_new_order = "Новый заказ"

    buttons = [get_user_defined_contact,
               get_new_order
               ]
    return make_reply_keyboard(buttons)


def start_keyboard():
    buttons = ["Новый заказ",
               "Мои заказы",
               ]

    return make_reply_keyboard(buttons)


class BotManager:

    def __init__(self, token):
        self.echo_handler = None
        self.start_handler = CommandHandler('start', start_command,
                                            pass_args=True)
        self.token = token
        self.dispatcher = None
        self.updater = Updater

        logging.basicConfig(level=logging_level,
                            format='%(asctime)s - %(name)s '
                                   '- %(levelname)s - %(message)s')
        self.logger = logging.getLogger()
        self.logger.setLevel(logging_level)

    def load_handlers(self):
        # start
        self.dispatcher.add_error_handler(error_callback)
        self.dispatcher.add_handler(self.start_handler)

        self.dispatcher.add_handler(active_orders_clients_handler)
        self.dispatcher.add_handler(get_all_orders_clients_handler)

        # conversation to receive nums
        self.dispatcher.add_handler(client_registration)

        # conversation to commit order by client
        self.dispatcher.add_handler(commit_order_conv_handler)

        # conv to evaluate orderd
        self.dispatcher.add_handler(receiver_eval_order_conv)

        # conversation to receive orders (clients)
        self.dispatcher.add_handler(order_receive_handler_client)

        # conversation to receive orders from receiver
        self.dispatcher.add_handler(order_receive_handler_receiver_v2)
        self.dispatcher.add_handler(callback_handler_show_order)
        self.dispatcher.add_handler(callback_handler_show_order_back)

        self.dispatcher.add_handler(handler_add_comment_order)
        self.dispatcher.add_handler(handler_enumerate_comments_order)
        self.dispatcher.add_handler(handler_add_detail_order)
        self.dispatcher.add_handler(handler_enumerate_details_order)

        # Показать картинки к заказу
        self.dispatcher.add_handler(handler_show_all_pics_order)

        # энумерация заказов
        self.dispatcher.add_handler(staff_enum_orders_v2)
        # энумерация деталй
        self.dispatcher.add_handler(staff_enum_details_v2)

        # переключение страниц деталей
        self.dispatcher.add_handler(callback_handler_switch_page_details)
        # показ элементов деталей
        self.dispatcher.add_handler(callback_handler_show_detail)

        # обратно с детали на страницу
        self.dispatcher.add_handler(callback_handler_show_detail_back)

        # Добавить коммент к детали
        self.dispatcher.add_handler(callback_handler_add_comment_detail)
        # Показать комменты к детали
        self.dispatcher.add_handler(handler_enumerate_comments_detail)

        # Пометить деталь как заказанную
        self.dispatcher.add_handler(handler_mark_detail_as_ordered)

        # Пометить деталь как выданную
        self.dispatcher.add_handler(handler_extradite_detail_callback)

        # Пометить заказ как ожидающий решения по доставке
        self.dispatcher.add_handler(handler_mark_order_to_delivery)

        # взять бабки за ремонт на самовывозе
        self.dispatcher.add_handler(handler_extradite_and_take_payment_order)

        # отправить заказ на выдачу с самовывозом
        self.dispatcher.add_handler(handler_mark_detail_to_extradition)

        # принять решение по доставке заказ
        self.dispatcher.add_handler(handler_select_delivery_order)

        # меню курьера
        self.dispatcher.add_handler(get_all_orders_courier_handler)

        # принять заказ от клиента как цель для прибытия (выбран курьером как точка адреса)
        self.dispatcher.add_handler(courier_handler_pickup_order)

        # принять заказ от клиента (клиенту приходит QR который курьер должен отсканировать)
        self.dispatcher.add_handler(send_qr_to_transmit_order_from_client_to_courier)

        # отменить заказ от клиента как цель для прибытия (выбран курьером как точка адреса)
        # и вернуть в пул доступных заказов для выбора курьерами
        self.dispatcher.add_handler(courier_handler_cancel_pickup_order)

        # отмена заказа клиентом
        self.dispatcher.add_handler(handle_cancel_order_by_client)

        # отмена заказа клиентом по прибытию курьера
        self.dispatcher.add_handler(handler_client_return_order_to_store)

        # передача заказа от курьера приемщику
        self.dispatcher.add_handler(send_qr_to_transmit_order_from_courier_to_store)

        # передать заказ от приемщика курьеру
        self.dispatcher.add_handler(send_qr_to_transmit_order_from_store_to_courier)

        # передача заказа от курьера клиенту
        self.dispatcher.add_handler(send_qr_to_transmit_order_from_courier_to_client)

        # оплата заказа при передаче от курьера клиенту
        self.dispatcher.add_handler(handler_take_payment_courier)

        # оплата заказа при передаче от курьера клиенту
        self.dispatcher.add_handler(handler_print_labels)



        # раздел помощь
        self.dispatcher.add_handler(handler_help)

        # принять решение по доставке детали
        self.dispatcher.add_handler(handler_select_delivery_detail)

        # выдать ремонтный заказ
        self.dispatcher.add_handler(get_payments_self_delivery_order)

        # выдать деталь Detail
        self.dispatcher.add_handler(get_payments_receiver_detail)

        # удалить деталь Detail
        self.dispatcher.add_handler(handler_delete_detail_callback)

        # отправить емейл с товарами
        self.dispatcher.add_handler(staff_send_goods_email_handler)

        # обновить цену ремонт колбек
        self.dispatcher.add_handler(handler_add_update_price_order)

        # добавить деталь меседж сотрудники
        self.dispatcher.add_handler(staff_add_detail_CH)

        # колбек переключить страничку с ремонтом
        self.dispatcher.add_handler(callback_handler_switch_page_order)

        # список товаров
        self.dispatcher.add_handler(staff_enum_details_v2)

        # поиск в ремонте
        self.dispatcher.add_handler(staff_search_orders)
        # вернутся назад в поиске в ремонте
        self.dispatcher.add_handler(callback_handler_search_order_back)

        # показать ремонт поиск
        self.dispatcher.add_handler(callback_handler_show_order_search)

        # показать ремонт поиск назад (основной список)
        self.dispatcher.add_handler(callback_handler_switch_page_order_search)

        # поиск в товаре
        self.dispatcher.add_handler(staff_search_detail)

        # вернутся назад в поиске в товаре
        self.dispatcher.add_handler(callback_handler_search_detail_back)

        # показать товар поиск
        self.dispatcher.add_handler(callback_handler_show_detail_search)

        # показать товар поиск назад (основной список)
        self.dispatcher.add_handler(callback_handler_switch_page_detail_search)

        # нажатие на кнопку профиля вне диалога
        self.dispatcher.add_handler(handler_on_press_select_profile_outside_conversation)

        # вывод меню с выбором для выбора юзера
        self.dispatcher.add_handler(staff_enumerate_users)

        # колбек переключить страничку с юзером
        self.dispatcher.add_handler(callback_handler_switch_page_user)

        # колбек показать юзера
        self.dispatcher.add_handler(callback_handler_show_user)

        # колбек назад показать всех юзеров в обычном режиме
        self.dispatcher.add_handler(callback_handler_show_user_back)

        # колбек изменить поля юзера
        self.dispatcher.add_handler(handler_change_users_fields)

        # колбек показать юзера в режиме поиска
        self.dispatcher.add_handler(callback_handler_show_user_search)

        # колбек показать вернутся на страницу поиска юзера
        self.dispatcher.add_handler(callback_handler_search_user_back)

        # echo must be last
        self.dispatcher.add_handler(echo_handler)

        return None

    def start_poll(self):
        """
        """
        print("main path", MAIN_PATH)
        # init db and tables
        check_base()
        # init Pool manager
        # init poll
        self.updater = Updater(token=self.token, use_context=True)
        self.dispatcher = self.updater.dispatcher

        # add handlers
        self.load_handlers()

        # start poll
        print(config_bot.MODE)
        match config_bot.Mode(config_bot.MODE):
            case config_bot.Mode.polling:
                print("Starting polling")
                self.updater.start_polling()
            case config_bot.Mode.webhook:
                print("Webhook set")
                self.updater.start_webhook(listen=config_bot.listen,
                                           port=config_bot.webhook_port,
                                           cert=config_bot.cert,
                                           key=config_bot.key,
                                           url_path=config_bot.url_path,
                                           webhook_url=config_bot.webhook_url,
                                           )
            case _:
                print("Unknown mode")
                exit(input("Hit enter"))
