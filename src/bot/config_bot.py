import logging
import os

##########################################
# vk
from enum import Enum

# BEFORE INSTALL DO NOT FORGET INSTALL TELETHON ON SYSTEM CAUSE IT USES SUBPROCESS

COUNTRY = "RU"
LANG = 'ru'
VK_TOKEN = "98ab8cc0ef27264397eeb974d3be217376027f59010ff85800a713e7d336267dc759608347506028f4ddb"
VK_GROUP_ID = '155703281'  # Id Group
VK_APP_ID = '7475610'  # Id app
VK_LOGIN = '89167835999'  # login
VK_PASSWORD = 'xSmtw$s92xTRLAN'  # pass_user

# list with admins
VK_TEST_USER_ID = [453450481,
                   ]  # Id user

ADMIN_CHAT_ID = "1085323286"
ADMIN_CHAT_ID_anton = "223781831"
###########################################
# TG_BOT_TOKEN = os.environ.get('spektr_bot')
# TG_BOT_TOKEN = "1674768111:AAGvYi-gG4skhhGPXcvCQvAllJRni8h1AIY"  # test
# TG_BOT_TOKEN = "1374538172:AAGuUQpC4lYgnEcrxjaCDjJ7CvihgqACaMc"  # work
TG_BOT_TOKEN = os.environ.get('TG_BOT_TOKEN')
TG_DEEPLINK = os.environ.get('TG_DEEPLINK')
API_HASH_LABELS = os.environ.get('API_HASH_LABELS')

TG_ADMIN_TEST = os.environ.get('TG_ADMIN_TEST')

# todo перед продом обозначить пути исходя из переменных среды касательно
#  всех генераторов файлов и не забудь бд, айпи и урл для бота
###########################################
# test
TEST_MSG = os.environ.get('TEST_MSG')  # test message when starts

###########################################
# db
DB_NAME = os.environ.get('DB_NAME')
DB_HOST = os.environ.get('DB_HOST')
DB_USER = os.environ.get('DB_USER')
DB_PASS = os.environ.get('DB_PASS')
DB_MEMCACHED = os.environ.get('DB_MEMCACHED')

EMAIL_AUTH = os.environ.get('EMAIL_AUTH')
EMAIL_PASS = os.environ.get('EMAIL_PASS')

logs = os.environ.get('LOGS')
photo_downloads = os.environ.get('PHOTOS')
deeplink_vault = os.environ.get('DEEPLINK_VAULT')
deeplink_bot_name = os.environ.get('DEEPLINK_BOT_NAME')
printed_orders_vault = os.environ.get('PRINTED_ORDERS_VAULT')
template_vault = os.environ.get('TEMPLATE_VAULT')
printer_file = os.environ.get('PRINTER_TEMPLATE')
excel_file = os.environ.get('EXCEL_OUT')
userbot_file = os.environ.get('USERBOT_FILE')


# webhook settings
# if mode polling no need to working certs
MODE = int(os.environ.get('MODE'))
listen = os.environ.get('LISTENER_IP')
webhook_port = os.environ.get('WEBHOOK_PORT')
webhook_ip = os.environ.get('WEBHOOK_IP')
cert = os.environ.get('PEM')
key = os.environ.get('KEY')
url_path = f'bot{TG_BOT_TOKEN}'
# webhook_url=f'https://94.253.12.22:88/bot{config_bot.TG_BOT_TOKEN}' # work
# webhook_url = f'https://5.181.210.167:88/bot{TG_BOT_TOKEN}'  # test
webhook_url = f'https://{webhook_ip}:{webhook_port}/bot{TG_BOT_TOKEN}'


web_preview_api = f'http://{webhook_ip}'

MAIN_PATH = os.path.abspath(os.path.dirname(__file__))
VENV_PATH = os.environ.get('VENV_PATH')

logging_level = os.environ.get('DEBUG_LEVEL')   # // "WARNING" //logging.DEBUG


class Mode(Enum):
    polling = 1
    webhook = 2


class Stores(Enum):
    kolomna = "Коломна"
    bronnicy = "Бронницы"
