from dataclasses import dataclass
from numbers import Number

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.Enumerators.StatusDetailMenu import StatusDetailMenu
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.models.pw_models import Client
from src.bot.utils.memcashed import del_memcached, write_memcached


@dataclass
class TempOrder:
    """
        Dataclass for serialise Order data in memory vault.
    """
    number: Number = 0
    new_client: bool = False
    profile: Client = None
    name: str = ""
    surname: str = ""
    brand: str = ""
    model: str = ""
    imei: str = ""
    comment: str = ""
    price: int = 0
    predefined_price: int = 0
    model_name: str = "Не указано"
    prepayment: int = 0
    delivery: str = ""
    geo: str = ""
    geo_check: bool = False
    address: str = ""
    address_check: bool = False
    pic_path: str = ""
    pic_id: str = ""
    status: str = StatusOrderMenu.iw.name
    trouble: str = None
    color: str = ''
    labels: int = 0
    photo_ids = []

    def __post_init__(self):
        self.photo_ids = []


@dataclass
class TempDetail:
    """
        Dataclass for serialise DetailOrder data in memory vault.
    """
    number: Number = 0
    new_client: bool = False
    profile: Client = None
    name: str = ""
    surname: str = ""
    detail: str = ""
    comment: str = ""
    price: int = 0
    predefined_price: int = 0
    prepayment: int = 0
    delivery: str = ""
    geo: str = ""
    geo_check: bool = False
    address: str = ""
    address_check: bool = False
    pic_path: str = ""
    pic_id: str = ""
    status: str = StatusDetailMenu.wttrdr.name
    trouble: str = ""
    color: str = ""
    is_detail_part: bool = False
    photo_ids = []


def create_temp_order(update, number, model=None):
    model.number = number.num_formatted
    subset_key = Memcache(chat_id=update.message.chat_id,
                          subset=MemorySubset.new_order_staff).get_subset()
    del_memcached(subset_key=subset_key)
    write_memcached(subset_key=subset_key, value=model)


def create_temp_detail(update, number, model=None):
    model.number = number.num_formatted
    subset_key = Memcache(chat_id=update.message.chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    del_memcached(subset_key=subset_key)
    write_memcached(subset_key=subset_key, value=model)
