from datetime import time
from pprint import pprint

import peewee_async
from peewee import Model, IntegerField, TextField, \
    ForeignKeyField, CharField, BooleanField, \
    TimestampField, PrimaryKeyField, BigIntegerField

from src.bot import config_bot
from src.bot.Enumerators.AdditionalService import AdditionalServiceEnum
from src.bot.Enumerators.StatusDetailMenu import StatusDetailMenu
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.resources import strings
from src.bot.utils.contact_processing import InputNumber

connector_postgres = peewee_async.PostgresqlDatabase(config_bot.DB_NAME,
                                                     host=config_bot.DB_HOST,
                                                     user=config_bot.DB_USER,
                                                     password=config_bot.DB_PASS,
                                                     autoconnect=True,
                                                     )


class BaseModel(Model):
    class Meta:
        database = connector_postgres


class Client(BaseModel):
    """
        Клиенты бота (сотрудники, администраторы и другие)
    """
    item_id = PrimaryKeyField()
    name = CharField(default='', index=True)
    surname = CharField(default='', index=True)
    middle_name = CharField(default='', index=True)
    chat_id = BigIntegerField(default=0)
    current_state = CharField(default='')
    workspace_geo = CharField(default='')
    staff_status = IntegerField(default=0)
    selected_function = CharField(default='')
    delivery_status = CharField(default='')
    params = CharField(default='')
    banned = BooleanField(default=False)
    added = TimestampField()

    class Meta:
        db_table = f'clients'


def client_create_by_profile(chat_id, number, account_names):
    client = Client.create(chat_id=chat_id)
    ContactClient().create(number=number, client_id=client)
    client.name = account_names
    client.current_state = strings.txt_status_registered
    client.staff_status = strings.client
    client.save()


class Customer(BaseModel):
    """
        Внешний клиент.
    """
    item_id = PrimaryKeyField()
    name = CharField()
    surname = CharField()
    middle_name = CharField()
    address = TextField()
    selected_function = CharField()
    params = CharField()
    added = TimestampField()

    class Meta:
        db_table = f'customers'


class NewOrderClientTemp(BaseModel):
    item_id = PrimaryKeyField()
    brand_name = CharField()
    model_name = CharField()
    client = ForeignKeyField(Client, column_name='client_id', backref='orders')
    trouble = TextField()
    pic_path = TextField()
    pic_id = TextField()
    color = TextField()
    oriented_timestamp = TextField()
    status = TextField()
    timestamp = TimestampField()

    class Meta:
        db_table = f'temp_order_'


class Order(BaseModel):
    """
    Status: StatusOrderMenu.item.value

    """
    item_id: PrimaryKeyField = PrimaryKeyField(index=True, unique=True, )
    client: Client = ForeignKeyField(Client, column_name='client',
                                     backref='orders')
    trouble: TextField = TextField(default='не указано', index=True)
    pic_path: TextField = TextField(default='') # FIXME удалить эти столбцы затем сделать дамп и пересобрать с удалением в этой модели!
    pic_id: TextField = TextField(default='') # FIXME удалить эти столбцы затем сделать дамп и пересобрать с удалением в этой модели!
    oriented_timestamp: TextField = TextField(default='расчет не произведен') \
        # todo добавить календарь или что то в этом роде для выборки \
    #  временной точки, чтобы бот мог добавить задание с напоминанием
    status: str = TextField(default='')
    predefined_price: TextField = BigIntegerField(default=0)
    price: TextField = BigIntegerField(default=0, index=True, )
    prepayment: TextField = BigIntegerField(default=0, index=True)
    payment: TextField = BigIntegerField(default=0, index=True)
    delivery_check: TextField = BooleanField(default=False)
    geo_check: TextField = BooleanField(default=False)
    address_check: TextField = BooleanField(default=False)
    call_check: TextField = BooleanField(default=False)
    high_priority: TextField = BooleanField(default=False)
    courier: Client = ForeignKeyField(Client, column_name='courier',
                                      field=Client.item_id,
                                      backref='picked_orders',
                                      null=True)
    creator: Client = ForeignKeyField(Client, column_name='creator',
                                      backref='created_orders')
    added: time = TimestampField()

    class Meta:
        db_table = f'orders_clients'


class Photo(BaseModel):
    item_id = PrimaryKeyField()
    order_id = ForeignKeyField(Order, backref='photos')
    pic_path: TextField = TextField(default='')
    pic_id: TextField = TextField(default='')

    timestamp = TimestampField()

    class Meta:
        db_table = f'photo'


class AdditionalService(BaseModel):
    """
    Таблица с дополнительными услугами, выполненными к основному заказу
    (наклейка стекла и проч)11
    """
    item_id = PrimaryKeyField()
    order_id = ForeignKeyField(Order, column_name='order_id',
                               backref='services')
    status = TextField(default=AdditionalServiceEnum.done)
    price = IntegerField(default=0)
    timestamp = TimestampField()

    class Meta:
        db_table = f'services_repairs'


def temp_orders_table(chat_id):
    """
    Функция для создания временной таблицы по chat_id.
    Хранятся все необходимые данные, которые используются для создания заказа.
    Можно расширять столбцы, но при извлечении указывай конкретные столбцы, а не итеративно.
    Args:
        chat_id: Telegram.Update.message.chat_id

    Returns:
        мутированный класс temp_order_{chat_id}(BaseModel)
    """

    # temp_order_table = OrdersClients()
    attrs = {
        'order_id': PrimaryKeyField(),
        'brand_name': CharField(default='NoName'),
        'model_name': CharField(default='не указано'),
        'client_id': ForeignKeyField(Client, column_name='client_id',
                                     backref='orders'),
        'name': CharField(default=''),
        'surname': CharField(default=''),
        'trouble': TextField(default='не указано'),
        'number': TextField(default='не указано'),
        'pic_path': TextField(default=''),
        'pic_id': TextField(default=''),
        'color': TextField(default='не указано'),
        'imei': CharField(default=''),
        'oriented_timestamp': TextField(default='расчет не произведен'),
        'status': TextField(default=''),
        'comment': TextField(default=''),
        'predefined_price': IntegerField(default=0),
        'price': IntegerField(default=0),
        'payment': IntegerField(default=0),
        'prepayment': IntegerField(default=0),
        'geo_pos': TextField(default=''),
        'address': TextField(default=''),
        'timestamp': TimestampField(),
        'new_client': BooleanField(default=False),
        'address_check': BooleanField(default=False),
        'geo_check': BooleanField(default=False),

    }
    temp_order_table = type('Detail', (BaseModel,), attrs)
    temp_order_table._meta.table_name = f'temp_order_{chat_id}'

    return temp_order_table


def temp_details_table(chat_id):
    """
    Функция для создания временной таблицы по chat_id.
    Хранятся все необходимые данные, которые используются для создания заказа.
    Можно расширять столбцы, но при извлечении указывай конкретные столбцы, а не итеративно.
    Args:
        chat_id: Telegram.Update.message.chat_id

    Returns:
        мутированный класс temp_order_{chat_id}(BaseModel)
    """

    attrs = {
        'detail_id': PrimaryKeyField(),
        'detail_name': CharField(default='NoName'),
        'text': CharField(default='не указано'),
        'client_id': ForeignKeyField(Client, column_name='client_id',
                                     backref='details'),
        'name': CharField(default=''),
        'surname': CharField(default=''),
        'number': TextField(default='не указано'),
        'status': TextField(default=''),
        'comment': TextField(default=''),
        'price': IntegerField(default=0),
        'payment': IntegerField(default=0),
        'prepayment': IntegerField(default=0),
        'geo_pos': TextField(default=''),
        'address': TextField(default=''),
        'timestamp': TimestampField(),
        'new_client': BooleanField(),
    }
    temp_order_table = type('Detail', (BaseModel,), attrs)
    temp_order_table._meta.table_name = f'temp_detail_{chat_id}'

    return temp_order_table


class NewOrderCustomerTemp(BaseModel):
    item_id = PrimaryKeyField()
    brand_name = CharField()
    model_name = CharField()
    customer_id = IntegerField()
    trouble = TextField()
    pic_path = TextField()
    pic_id = TextField()
    color = TextField()
    oriented_timestamp = TextField()
    status = TextField()
    timestamp = TimestampField()

    class Meta:
        db_table = f'temp_order_'


class OrdersCustomers(BaseModel):
    item_id = PrimaryKeyField()
    brand_name = CharField()
    model_name = CharField()
    customer_id = ForeignKeyField(Customer, column_name='customer_id',
                                  backref='orders')
    trouble = TextField()
    pic_path = TextField()
    pic_id = TextField()
    color = TextField()
    oriented_timestamp = TextField()
    status = TextField()
    timestamp = TimestampField()

    class Meta:
        db_table = f'orders_customers'


class ContactClient(BaseModel):
    item_id = PrimaryKeyField()
    number = TextField(index=True)
    client = ForeignKeyField(Client, column_name='client_id', backref='numbs')
    # todo добавить соло паттерн для использованных номеров
    status = CharField(default='in_use')
    added = TimestampField()

    class Meta:
        db_table = f'numbs_clients'


class ContactNumbsCustomer(BaseModel):
    item_id = PrimaryKeyField()
    number = TextField()
    customer = ForeignKeyField(Customer, column_name='client_id',
                               backref='numbs')
    added = TimestampField()

    class Meta:
        db_table = f'numbs_customers'


class CommentOrderCustomer(BaseModel):
    order = ForeignKeyField(OrdersCustomers, db_column='order_id',
                            backref='comments')
    comment = TextField()
    added = TimestampField()

    class Meta:
        db_table = f'comments_customers'


class CommentOrder(BaseModel):
    item_id = PrimaryKeyField()
    order = ForeignKeyField(Order, db_column='order_id', backref='comments')
    client = ForeignKeyField(Client, backref='comments', default='', null=True)
    comment = TextField(index=True)
    added = TimestampField()

    class Meta:
        db_table = f'comments_orders'


class DetailOrder(BaseModel):
    """Класс для деталей, заказываемых от заказа
    Не забудь расписать как х выводить в списки!
    для этой таблицы не требуется создавать комментарии, поскольку
     это часть заказа
    TODO: сделать гугл таблиц под заказы и детали (что проще)
    """
    item_id: int = PrimaryKeyField()
    order_id: int = ForeignKeyField(Order, backref="details_order")
    detail_name: str = TextField(default='')
    text: str = TextField(default='')
    status: str = CharField(default='')
    price: int = IntegerField(default=0)
    approved_price: int = IntegerField(default=0)
    mark_approved: bool = BooleanField(default=False)
    added: int = TimestampField()

    class Meta:
        db_table = f'details_orders'


class Device(BaseModel):
    item_id = PrimaryKeyField()
    order = ForeignKeyField(Order, backref="devices")
    brand_name = CharField(default='', index=True)
    model_name = CharField(default='', index=True)
    imei = CharField(default='', index=True)
    color = CharField(default='', index=True)

    class Meta:
        db_table = f'devices'


class Detail(BaseModel):
    """Отдельный класс под отдельно заказываемые детали
    status: StatusDetailMenu.value
    """
    item_id: PrimaryKeyField = PrimaryKeyField(index=True)
    detail_name: TextField = TextField(default='')
    client: Client = ForeignKeyField(Client, backref="clients_details")
    text: TextField = TextField(default='', index=True)
    status: str = CharField()
    price: IntegerField = IntegerField(default=0, index=True)
    prepayment: IntegerField = IntegerField(default=0, index=True)
    payment: IntegerField = IntegerField(default=0, index=True)
    delivery_check: BooleanField = BooleanField(default=False)
    geo_check: BooleanField = BooleanField(default=False)
    address_check: BooleanField = BooleanField(default=False)
    call_check: BooleanField = BooleanField(default=False)
    is_detail: BooleanField = BooleanField(default=False)
    creator: Client = ForeignKeyField(Client, backref="created_details")
    added: time = TimestampField()

    class Meta:
        db_table = f'details'


class CommentDetail(BaseModel):
    item_id = PrimaryKeyField()
    detail = ForeignKeyField(Detail, backref='comments')
    client = ForeignKeyField(Client, backref='comments')
    comment = TextField(default='', index=True)
    added = TimestampField()

    class Meta:
        db_table = f'comments_details'


class AddressOrder(BaseModel):
    item_id = PrimaryKeyField()
    order = ForeignKeyField(Order, backref='addresses')
    address = TextField(default='')
    added = TimestampField()

    class Meta:
        db_table = f'address_order'


class GeoPositionOrder(BaseModel):
    item_id = PrimaryKeyField()
    order = ForeignKeyField(Order, backref='geopositions')
    geo_pos = TextField(default='')
    added = TimestampField()

    class Meta:
        db_table = f'geo_position_order'


class AddressDetail(BaseModel):
    item_id = PrimaryKeyField()
    detail = ForeignKeyField(Detail, backref='addresses')
    address = TextField(default='')
    added = TimestampField()

    class Meta:
        db_table = f'address_detail'


class GeoPositionDetail(BaseModel):
    type_id = PrimaryKeyField()
    detail = ForeignKeyField(Detail, backref='geopositions')
    geo_pos = TextField(default='')
    added = TimestampField()

    class Meta:
        db_table = f'geo_position_detail'


class QR(BaseModel):
    item_id = PrimaryKeyField()
    type_id = CharField()
    link = TextField()
    expired = BooleanField(default=False)
    expired_data = TimestampField()
    added = TimestampField()

    class Meta:
        db_table = f'qr_links'


class SearchQuery(BaseModel):
    item_id = PrimaryKeyField()
    client: Client = ForeignKeyField(Client, backref="search_queries")
    query_text = TextField()
    added = TimestampField()

    class Meta:
        db_table = f'search_queries'


all_tables_to_create = [
    Client,
    Order,
    AdditionalService,
    ContactClient,
    CommentOrder,
    DetailOrder,
    Detail,
    CommentDetail,
    AddressOrder,
    GeoPositionOrder,
    AddressDetail,
    GeoPositionDetail,
    QR,
    Device,
    SearchQuery,
    Photo,
]


def create_tables():
    connector_postgres.create_tables(all_tables_to_create)

    return None


def recreate_tables():
    connector_postgres.drop_tables(all_tables_to_create)
    connector_postgres.create_tables(all_tables_to_create)

    return None


def migrate_orders():
    pass


def parse_orders():
    pass


def migrate_profile():
    ...


def enumerate_orders():
    pass


def enumerate_profiles():
    pass


def parse_details():
    pass


def enumerate_details():
    pass


def migrate_details():
    ...


def set_admin_profile():
    client = Client().get(Client.item_id == 1)
    client.staff_status = 2
    client.save()


def migrate():
    from peewee import SqliteDatabase
    from playhouse.reflection import generate_models, print_model
    db = SqliteDatabase('old_base/main_database.sqlite')
    models = generate_models(db)
    globals().update(models)
    list(models.items())
    print_model(orders)
    """
    all_clients = clients.select()
    for i in all_clients:
        client = Client.create(
            name=i.name,
            chat_id=i.chat_id,
            current_state=i.current_state,
            workspace_geo=i.workspace_geo,
            staff_status=0,
        )
        ContactClient.create(
            number=

        )
    """
    all_orders = orders.select()
    all_clients = {}
    all_clients["+79160306220"] = 1
    print(all_orders[0].order_id)
    print("orders parsing")
    for i in all_orders:
        if not isinstance(i, orders):
            break
        try:
            print(i)
        except Exception as e:
            print(e)
        number = InputNumber(i.phone_number)
        number.make_number()
        client = None
        try:
            name, surname, middle_name = i.client_name, i.client_surname, i.client_middle_name
        except Exception as e:
            print(e)
            name, surname, middle_name = "Не указано", "Не указано", "Не указано"

        if not name:
            name = "Не указано"
        if not surname:
            surname = "Не указано"
        if not middle_name:
            middle_name = "Не указано"

        try:
            client = all_clients[number.num_formatted]
        except KeyError:
            try:
                client = Client.create(name=name,
                                       surname=surname,
                                       middle_name=middle_name,
                                       )
                all_clients[number.num_formatted] = client.item_id
                print("new client ", client.item_id)
            except Exception as e:
                print(f"error on create client {e}")

        print('creating contact')
        print("number", number.num_formatted, "client", client)
        contact = ContactClient.create(
            number=number.num_formatted,
            client=client
        )

        print("client for order is ", client)

        prepayment = i.prepayment
        if prepayment is None or isinstance(prepayment, str):
            prepayment = 0
        payment = i.payment
        if payment is None or isinstance(payment, str):
            payment = 0

        price = i.price
        if price is None or isinstance(prepayment, str):
            price = 0

        status = i.status
        if status == "end_ok":
            status = StatusOrderMenu.iw.name
        elif status == "finished":
            status = StatusOrderMenu.fin.name

        trouble = i.trouble
        if trouble == (None):
            trouble = ""

        try:
            price = int(price)
        except Exception as e:
            price = 0

        try:
            prepayment = int(prepayment)
        except Exception as e:
            prepayment = 0

        try:
            payment = int(payment)
        except Exception as e:
            payment = 0

        print(
            f"client{client} trouble {trouble} status {status} predefined_price {price} prepayment {prepayment} payment {payment} "
        )
        order = Order.create(
            client=client,
            trouble=trouble,
            status=status,
            predefined_price=price,
            price=price,
            prepayment=prepayment,
            payment=payment,
            creator=1
        )
        print(order.item_id, "order transmitted")

        brand_name, model_name, imei = i.brand_name, i.model_name, i.imei
        if brand_name is None:
            brand_name = "Не указано"

        if model_name is None:
            model_name = "Не указано"

        if imei is None:
            imei = "Не указано"

        device = Device.create(
            order=order,
            brand_name=brand_name,
            model_name=model_name,
            imei=imei,
        )
        print("device created")
        pprint(device)
        comment = i.comments_receive
        if not comment:
            comment = "Не указано"

        print("comment order creeating")
        comment_order = CommentOrder.create(
            order=order,
            client=client,
            comment=comment
        )
        pprint(comment_order)

    print("detil parsing")
    all_details = details.select()
    for i in all_details:
        if not isinstance(i, details):
            break
        number = InputNumber(i.phone_number)
        number.make_number()
        print(number.num_formatted)
        try:
            client = all_clients[number.num_formatted]
        except KeyError:
            try:
                client = Client.create(name=i.client_name)
                all_clients[number.num_formatted] = client.item_id
            except Exception as e:
                print(f"error on create  client {e}")

        print('creating contact')
        print("number", number.num_formatted, "client", client)
        contact = ContactClient.create(
            number=number.num_formatted,
            client=client
        )

        print("client for order is ", client)

        prepayment = i.prepayment
        if prepayment is None or isinstance(prepayment, str):
            prepayment = 0
        payment = i.payment
        if payment is None or isinstance(payment, str):
            payment = 0

        status = i.status
        if status == "end_ok":
            status = StatusDetailMenu.wttrdr.name
        elif status == "finished":
            status = StatusDetailMenu.finished.name

        try:
            prepayment = int(prepayment)
        except Exception as e:
            prepayment = 0

        try:
            payment = int(payment)
        except Exception as e:
            payment = 0

        detail_name = i.detail_name
        if not detail_name:
            detail_name = "Не указано"
        else:
            try:
                detail_name = i.detail_name.encode("utf-8")
            except AttributeError:
                pass

        detail_text = i.detail_text
        if not detail_text:
            detail_text = "Не указано"
        else:
            try:
                detail_text = i.detail_text.encode("utf-8")
            except AttributeError:
                pass

        print("creating detail")
        print("params")
        print(f"detail_name {detail_name} client {client} "
              f"text {detail_text}  status {status} prepayment {prepayment}"
              f"payment {payment}")

        detail = Detail.create(
            detail_name=detail_name,
            client=client,
            text=detail_text,
            status=status,
            price=0,
            prepayment=prepayment,
            payment=payment,
            creator=1,
        )
        print(detail.item_id, "detail transmitted")

        print("creating comments")
        comment_detail = CommentDetail.create(
            detail=detail,
            client=client,
            comment=detail_text

        )
        pprint(comment_detail)
