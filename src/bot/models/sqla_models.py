from sqlalchemy import Column, create_engine, Integer, String, Boolean, DateTime
from sqlalchemy import ForeignKey
from sqlalchemy.orm import declarative_base

Base = declarative_base()

engine = create_engine("mysql+pymysql://root:@localhost/"
                       "telegram_spektr_bot?charset=utf8mb4", echo=True, future=True)


class Client(Base):
    """
        Клиенты бота (сотрудники и администраторы)
    """
    __tablename__ = "clients"

    client_id = Column(Integer, primary_key=True)
    name = Column(String(50))
    surname = Column(String(50))
    middle_name = Column(String(50))
    chat_id = Column(String(50))
    workspace_geo = Column(String(50))
    staff_status = Column(String(50))
    selected_function = Column(String(80))
    params = Column(String(200))
    banned = Column(Boolean)
    added = Column(DateTime)
