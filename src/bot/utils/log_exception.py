import traceback
from datetime import datetime

from telegram.ext import ConversationHandler

from src.bot import config_bot
from src.bot.config_bot import logs
from src.bot.models.pw_models import connector_postgres
from src.bot.resources import strings
from src.bot.utils.message import send_message, send_message_inline, \
    send_message_to_target


def error_callback(update, context):
    write_log(context.error)
    try:
        send_message(update, context,
                     message=strings.txt_error_to_start_prompt)
    except:
        send_message_inline(update, context,
                            message=strings.txt_error_to_start_prompt)

    info = f"{strings.txt_error_occured}:\n " \
           f"{traceback.format_exc()}\n" \
           f"{context.error}"
    if len(info) >= 4095:
        for x in range(0, len(info), 4095):
            send_message_to_target(context,
                                   chat_id=config_bot.TG_ADMIN_TEST,
                                   text=info[x:x + 4095],
                                   )
    else:
        send_message_to_target(context,
                               chat_id=config_bot.TG_ADMIN_TEST,
                               text=info
                               )
    connector_postgres.close()

    return ConversationHandler.END


def write_log(data):
    date = datetime.today().date()
    time = datetime.today().time().isoformat().replace(":", "_")
    name_file = f"{logs}/log_" + f"{date.year}_{date.month}_{date.day}_{time}.log"
    file = open(name_file, mode='w')
    file.write(f"{data}\n")
    file.write(traceback.format_exc())
    file.close()

    return None
