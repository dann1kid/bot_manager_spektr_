# !/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from src.bot.resources import strings


def readable_data_from_timestamp(timestamp):
    return datetime.fromtimestamp(timestamp, ).strftime(
        '%d.%m.%Y {} %H:%M').format("в")


def readable_data_from_datetime(datetime_object):
    week_day = strings.weekdays[datetime_object.isoweekday()]
    return f"{datetime_object.day}.{datetime_object.month}." \
           f"{datetime_object.year}" \
           f" в {datetime_object.hour}:{datetime_object.minute} " \
           f"({week_day})"


def readable_data_from_datetime_inline(datetime_object):
    week_day = strings.weekdays[datetime_object.isoweekday()]
    return f"{datetime_object.day}.{datetime_object.month}." \
           f"{datetime_object.year}"
