import qrcode

from src.bot.Enumerators.Deeplink import DeeplinkEnum, Deeplink
from src.bot.config_bot import deeplink_vault
from src.bot.models.pw_models import QR


def qr_generate_link_file(enum_type: DeeplinkEnum = None,
                          client_id: str | int = None,
                          type_id: str | int = None):
    file = f"{deeplink_vault}/qr_client_{client_id}_type_id_{type_id}.jpg"

    link = Deeplink(enum_type=enum_type.value, client_id=client_id,
                    type_id=type_id).generate_link()

    qrcode.make(link).save(file)

    QR.create(type_id=type_id, link=link,
              expired=False)

    return file
