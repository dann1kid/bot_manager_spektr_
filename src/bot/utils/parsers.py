# TODO удалить
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re


def parse_query(update):
    """

    :param update:
    :return:
    """
    query = update.callback_query

    # поиск идентификаторов
    data = re.findall(r'[\d]{1,15}', query.order)
    ticket_id = data[0]

    return ticket_id
