

def on_delivery_keyboard(order_id=None):
    """Шорткат для создания клавиатуры на выдаче
    :order_id: int

    returns InlineKeyboardMarkup
     """
    from src import get_order_data_delivery_columns
    from src.bot.keyboards import inline_kbd_orders_on_delivery

    delivery_row = get_order_data_delivery_columns(order_id=order_id)
    delivery_row = dict(zip(delivery_row[1], delivery_row[0]))

    return inline_kbd_orders_on_delivery(delivery_row['order_id'], delivery_row['delivery_id'])
