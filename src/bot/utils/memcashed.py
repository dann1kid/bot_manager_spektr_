import json
import pickle

from pymemcache.client.base import Client

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.config_bot import DB_MEMCACHED


# TODO: хранение в списке ключей со значениями  -- key_prefix -- subset
# TODO: сборка мусора с редиса -- time -- secs
# TODO: может поменять на сериализацию через json?


def write_memcached(subset_key: str | int = None, value: any = None) -> None:
    """
    Write a pair of key: value object to the memory.
    in this project use Memcache enumerator for key generator
    :subset_key: Memcache(chat_id=update.message.chat_id, subset=MemorySubset.new_order_receiver).get_subset()
    """

    client = Client(DB_MEMCACHED)
    value = pickle.dumps(value)
    client.set(subset_key, value)

    return None


def write_memcached_item(chat_id: str | int = None,
                         memory_subset: MemorySubset = None, value: any = None):
    """
    Записывает значение из хранилища на основе субсета и чат ид
    chat_id
    memory_subset: MemorySubset.value

    """
    subset_key = Memcache(
        chat_id=chat_id,
        subset=memory_subset).get_subset()

    write_memcached(subset_key=subset_key, value=value)

    return None


def get_memcached_item(chat_id: str | int = None,
                       memory_subset: MemorySubset = None):
    """
    Получает значение из хранилища на основе субсета и чат ид
    chat_id
    memory_subset: MemorySubset.value

    """
    subset_key = Memcache(
        chat_id=chat_id,
        subset=memory_subset).get_subset()

    return get_memcached(subset_key=subset_key)


def get_memcached(subset_key: str | int = None) -> any:
    client = Client(DB_MEMCACHED)
    return pickle.loads(client.get(subset_key))


def del_memcached(subset_key: str | int = None) -> any:
    client = Client(DB_MEMCACHED)
    client.delete(subset_key)
    return None


def update_memcached(chat_id: str | int = None, subset: MemorySubset = None,
                     temp_structure=None, data: str | int = None):
    """Usage:
    """
    subset_key = Memcache(chat_id=chat_id,
                          subset=subset).get_subset()
    temp_order = get_memcached(subset_key=subset_key)
    temp_order[temp_structure] = data
    write_memcached(subset_key=subset_key, value=temp_order)
