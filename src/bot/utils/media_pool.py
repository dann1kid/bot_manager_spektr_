#!/usr/bin/env python
# -*- coding: utf-8 -*-
# загрузка и сохранение файлов в медиапул, иначе передача готового файла
from telegram import InputMediaPhoto

from src import get_file_id_by_name


def photos_send_question_admin(context, chat_id=None):
    """
    Args:
        context: context object
        chat_id: target client chat_id
    """
    photos = ['model_xiaomi', 'model_iphone']
    # формирование списка arrayMedia
    media_input = []
    for photo in photos:
        # получение идентификатора
        media = get_file_id_by_name(name=photo)
        media_input.append(InputMediaPhoto(media))

    context.bot.send_media_group(chat_id=chat_id, media=media_input)
