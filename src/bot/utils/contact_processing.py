from dataclasses import dataclass

import phonenumbers
from phonenumbers import PhoneNumber, parse, NumberParseException, \
    is_possible_number

from src.bot import config_bot


@dataclass
class InputNumber:
    """Class for parse users text as number
    Usable: number = InputNumber(users_text).make_number()

    """
    users_text: str
    num_parsed: PhoneNumber | None = None
    num_formatted: str = None
    num_possible: bool = False
    raw_num_length: int = 0

    def parse_number(self):
        """try to parse users_text to a valid number"""
        try:
            self.num_parsed = parse(number=self.users_text,
                                    region=config_bot.COUNTRY)
        except NumberParseException:
            self.num_parsed = None
        else:
            self.num_possible = is_possible_number(self.num_parsed)

    def format_number(self):
        """format valid parsed number"""
        self.num_formatted = \
            phonenumbers.format_number(self.num_parsed,
                                       phonenumbers.PhoneNumberFormat.E164)

    def check_length(self):
        self.raw_num_length = len(self.users_text)

    def make_number(self):
        """Fulfills num_parsed field with formatted number valid number,
        otherwise leaves empty
        """
        self.check_length()
        if self.raw_num_length >= 10:
            self.parse_number()
            if self.num_parsed:
                self.format_number()
