# !/usr/bin/env python
# -*- coding: utf-8 -*-
from enum import Enum

from telegram import ReplyKeyboardRemove, InlineKeyboardButton, \
    InlineKeyboardMarkup, Update, ParseMode
from telegram.error import BadRequest, TelegramError
from telegram.ext import ConversationHandler
from telethon import TelegramClient

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.keyboards.TG_keyboards import hub_keyboard, make_reply_keyboard
from src.bot.models.pw_models import Client
from src.bot.resources import strings
from src.bot.resources.strings import txt_cancelled, txt_back
from src.bot.utils.timestamp_parses import readable_data_from_datetime


def send_message_inline(update, context, message, reply=None, parse_mode=None,
                        **kwargs) -> None:
    """context based send message for inline buttons with callback
    :param parse_mode:
    :type update: callback update
    :param context: context obj
    :param message: string
    :param reply: InlineKeyboard

    :return None
    """
    if reply == 'remove':
        reply = ReplyKeyboardRemove()

    context.bot.send_message(
        chat_id=update.callback_query.from_user.id,
        text=message,
        reply_markup=reply,
        parse_mode=parse_mode,
        **kwargs
    )

    return None


def send_photo_inline(update, context, caption, media_id=None, photo_path=None, **kwargs) -> None:
    """context based send message for inline buttons with callback
    :type update: callback update
    :param context: context obj
    :param caption: string
    :param media_id: string
    :param photo_path: string

    return None
    """

    if media_id:
        context.bot.send_photo(
            photo=media_id,
            chat_id=update.callback_query.from_user.id,
            caption=caption,
            **kwargs)
    elif photo_path:
        context.bot.send_photo(
            photo=photo_path,
            chat_id=update.callback_query.from_user.id,
            caption=caption,
            **kwargs
        )
    else:
        raise "No photo"

    return None


def send_message_inline_enum(update, context, enum_state: Enum = None,
                             parse_mode=None,
                             **kwargs) -> None:
    """context based send message for inline buttons with callback
    :param parse_mode:
    :type update: callback update
    :param context: context obj
    :param enum_state: enum class based states:

        sample class:
            class ConversationStates(Enum):
            WAIT_CONFIRM = {"txt": strings.prt_are_you_shure,
                            "buttons": [strings.txt_yes,
                                        strings.txt_no,
                                        strings.txt_cancel]}



    :return None
    """
    if enum_state.value["buttons"] == 'remove':
        reply = ReplyKeyboardRemove()

    message = context.bot.send_message(
        chat_id=update.callback_query.from_user.id,
        text=enum_state.value["txt"],
        reply_markup=make_reply_keyboard(enum_state.value["buttons"]),
        parse_mode=parse_mode,
        **kwargs
    )

    return message


def send_message_enum(update: Update, context,
                      enum_state: Enum = None, parse_mode=ParseMode.HTML) -> None:
    """context based send message for any reason
    """
    if enum_state.value["buttons"] == 'remove':
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=enum_state.value["txt"],
            reply_markup=ReplyKeyboardRemove(),
            parse_mode=parse_mode, )
    elif enum_state.value["buttons"] is None:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=enum_state.value["txt"],
            parse_mode=parse_mode,
        )
    else:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=enum_state.value["txt"],
            reply_markup=make_reply_keyboard(enum_state.value["buttons"]),
            parse_mode=parse_mode,
        )

    return None


def concat_comments(comments):
    message = '\n'
    comms = [
        f"{x.client.name} {readable_data_from_datetime(x.added)}\n {x.comment}\n"
        for x in comments]

    return message.join(comms)


def send_message(update: Update, context, message: str,
                 reply: any = None,
                 parse_mode: str = None) -> None:
    """Context based send message for any reason.

    Reply="remove" if need to delete reply keyboard

    """
    try:
        if reply == 'remove':
            context.bot.send_message(
                chat_id=update.message.chat_id,
                text=message,
                reply_markup=ReplyKeyboardRemove(),
                parse_mode=parse_mode, )

        elif reply is None:
            context.bot.send_message(
                chat_id=update.message.chat_id,
                text=message,
                parse_mode=parse_mode,
            )
        else:
            context.bot.send_message(
                chat_id=update.message.chat_id,
                text=message,
                reply_markup=reply,
                parse_mode=parse_mode,
            )
    except Exception:
        raise

    return None


def send_file(update, context, caption='', file=None):
    context.bot.send_document(chat_id=update.message.chat_id, caption=caption,
                              document=open(file, 'rb'))

    return None


def send_file_inline(update, context, caption='', file=None):
    context.bot.send_document(chat_id=update.effective_chat.id, caption=caption,
                              document=open(file, 'rb'))

    return None


def send_file_to_target(context, chat_id, caption='', file=None):
    try:
        context.bot.send_document(chat_id=chat_id, caption=caption,
                                  document=open(file, 'rb'))
    except TelegramError:
        raise

    return None


def send_message_callback(update, context, message, reply=None,
                          parse_mode=None) -> None:
    """context based send message for any reason
    :param update: 
    :param context:
    :param message:
    :param reply:
    :param parse_mode:
    :return None
    """
    if reply == 'remove':
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=message,
            reply_markup=ReplyKeyboardRemove(),
            parse_mode=parse_mode,
        )
    elif reply is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=message,
            parse_mode=parse_mode,
        )
    else:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=message,
            reply_markup=reply,
            parse_mode=parse_mode,
        )

    return None


def send_message_to_target(context, chat_id=None, text=None, reply=None, parse_mode=ParseMode.HTML):
    """Отправляет сообщение для конкретного chat_id
    :type context: Объект контекста
    :type chat_id: целевой chat_id
    :type text: строка с текстом
    :param reply: inline клавиатура
    :param parse_mode: метод отображения форматированного текста
    """
    if reply:
        context.bot.send_message(chat_id=chat_id, text=text, reply_markup=reply, parse_mode=parse_mode)
    else:
        context.bot.send_message(chat_id=chat_id, text=text)

    return None


def hide_keyboard(fun):
    def wrapper(*args, **kwargs):
        try:
            update = args[0]
            context = args[1]
            if update.message.text == strings.btn_hide_keyboard:
                send_message(update, context,
                             message=strings.ok_keyboard_hide,
                             reply=make_reply_keyboard(
                                 [strings.btn_show_keyboard]))
                return None
            else:
                return fun(*args, **kwargs)

        except AttributeError:
            return None

    return wrapper


def show_keyboard(next_state: Enum = None):
    def wrap(fun):
        def wrapper(*args, **kwargs):
            try:
                update = args[0]
                context = args[1]
                if update.message.text == strings.btn_show_keyboard:
                    send_message(update, context,
                                 message=next_state.value["txt"],
                                 reply=make_reply_keyboard(
                                     next_state.value["buttons"]))
                    return None
                else:
                    return fun(*args, **kwargs)
            except AttributeError:
                return None

        return wrapper

    return wrap


def get_chat_id(update):
    try:
        chat_id = update.message.chat_id
    except AttributeError:
        chat_id = update.callback_query.message.chat.id

    return chat_id


def skip_step(next_state: Enum = None):
    """Декоратор для пропуска шагов в диалоге.
    Если значение записывается в базу оно не должно иметь
    None или Null, то есть должно иметь значение по умолчанию.
    Только для MessageFilter, не для колбека
    (если захочется переделай отпраку меседжа на инлайн)
    """

    def wrap(func):
        def wrapper(*args, **kwargs):
            try:
                update = args[0]
                context = args[1]
                if update.message.text == strings.txt_btn_skip:
                    send_message(update, context,
                                 message=next_state.value["txt"],
                                 reply=make_reply_keyboard(
                                     next_state.value["buttons"]))
                    return next_state.name
                else:
                    return func(*args, **kwargs)
            except AttributeError:
                return func(*args, **kwargs)

        return wrapper

    return wrap


def cancel_conversation(fun):
    def wrapper(*args, **kwargs):
        try:
            update = args[0]
            context = args[1]
            try:
                if update.message.text == StatusOrderMenu.cancel.value:
                    send_message(update, context, message=txt_cancelled,
                                 reply=hub_keyboard(update.message.chat_id))
                    return ConversationHandler.END
                else:
                    return fun(*args, **kwargs)
            except AttributeError:
                # case callback
                return fun(*args, **kwargs)
        except AttributeError:
            return fun(*args, **kwargs)

    return wrapper


# todo придумать что то превентирующее что нажатие на колбек вызовет конверсейшн хендлер - енд
# todo написать враппер для завершения других диалогов перед реакцией с колбека
# todo написать враппер с обрезкой диалога для всех колбекквери ентрипойнтов и хандлеров (начинать с этих слов для поиска)
# попробовать написать враппер с остановкой и принудительным повторным запуском
def stop_previous_conversations(fun):
    def wrapper(*args, **kwargs):
        context = args[1]
        # context.dispatcher.stop()
        # context.dispatcher.start()

    return wrapper


def back_conversation(next_state: Enum = None):
    """Deco for turn back on selected state in conversation handler
    next_state: Any: Enum.parameter = {"txt": "description", "buttons": ["str_button_name_1", "str_button_name_2"]

    update: Telegram.Update: by called conversation handler
    context: Telegram.Context: by called conversation handler

    EnumCls(Enum):
        parameter_1 = "value"

    EnumCls.parameter_1.name = "parameter_1"
    EnumCls.parameter_1.values = "value"

    @back_wrapped(EnumCls.parameter_1)
    Returns: func
    """

    # todo переписать врапперы на безопасные (блять как че не так идет сразу ломаются как девочки)

    def wrap(func):
        def wrapper(*args, **kwargs):
            try:
                update = args[0]
                context = args[1]
                try:
                    if update.message.text == txt_back:  # TODO приделать передачу энумератора конверсейшна
                        send_message(update, context,
                                     # todo для отдельного определения контекста для кнопки "назад"
                                     message=next_state.value["txt"],
                                     reply=make_reply_keyboard(
                                         next_state.value["buttons"]))
                        return next_state.name
                    else:
                        return func(*args, **kwargs)
                except AttributeError:
                    return func(*args, **kwargs)
                except TypeError:
                    return ConversationHandler.END
            except AttributeError:
                return func(*args, **kwargs)

        return wrapper

    return wrap


def only_digits(next_state: Enum = None):
    """Deco for turn back on selected state in conversation handler
    EnumCls(Enum):
        parameter_1 = "value"

    EnumCls.parameter_1.name = "parameter_1"
    EnumCls.parameter_1.values = "value"

    @back_wrapped(EnumCls.parameter_1)
    Returns: func
    """

    def wrap(func):
        def wrapper(*args, **kwargs):
            try:
                update = args[0]
                context = args[1]
                try:
                    int(update.message.text)
                except (ValueError, TypeError):
                    send_message(update, context,
                                 message=strings.err_only_digits)
                    send_message(update, context,
                                 message=next_state.value["txt"],
                                 reply=make_reply_keyboard(
                                     next_state.value["buttons"]))
                    return next_state.name
                else:
                    return func(*args, **kwargs)
            except AttributeError:
                return func(*args, **kwargs)

        return wrapper

    return wrap


def mult_inline_keyboard(data: [()] = None):
    """making keyboard with [list of lists with [text, callback_data],...]
    Returns InlineKeyboardMarkup
    """
    keyboard = []
    for button_set in data:
        keyboard.append([InlineKeyboardButton(text=button_set[0],
                                              callback_data=button_set[1])])

    return InlineKeyboardMarkup(keyboard)


def mono_inline_keyboard(text: str = None, callback_data: str = None):
    """making keyboard with text for button and callback data
    Returns InlineKeyboardMarkup
    """

    keyboard = [
        [
            InlineKeyboardButton(text=text, callback_data=callback_data)
        ],
    ]

    return InlineKeyboardMarkup(keyboard)


def send_message_telethon(geo_staff, doc):
    try:
        # telethon
        api_id = 2895372
        api_hash = "a6311721eb3d2b25872337df774fd336"
        client_telethon = TelegramClient('session_name', api_id, api_hash)
        client_telethon.start()

    except Exception as e:
        raise e

    if geo_staff == 'colomna':
        client_telethon.send_file('printer_kolomna_bot', doc)
    elif geo_staff == 'bronnicy':
        client_telethon.send_file('printer_bronnicy_bot', doc)
    else:
        client_telethon.send_message('printer_bronnicy_bot', 'hello')
        client_telethon.send_file('printer_bronnicy_bot', doc)
    return None


def notify_staff(context, item=None, staff_level: StaffLevel = StaffLevel.receiver, message: str = ""):
    staffs = Client.select().where(
        Client.staff_status == staff_level.value)
    if item:
        for staff in staffs:
            try:
                send_message_to_target(context, chat_id=staff.chat_id,
                                       text=f"{message} №{item.item_id}")
            except BadRequest:
                pass
    else:
        for staff in staffs:
            try:
                send_message_to_target(context, chat_id=staff.chat_id,
                                       text=message)
            except BadRequest:
                pass
