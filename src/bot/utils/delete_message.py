# !/usr/bin/env python
# -*- coding: utf-8 -*-


def callback_delete_message(update, context):
    """
    удаляет меседж с колбек-апдейта
    :param update:
    :param context:
    :return:
    """
    query = update.callback_query
    chat_id = update.effective_chat.id
    message_id = query.message.message_id

    context.bot.delete_message(chat_id, message_id)
    return None
