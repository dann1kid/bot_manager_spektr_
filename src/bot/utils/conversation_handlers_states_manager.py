from telegram.ext import CallbackQueryHandler, ConversationHandler, \
    CommandHandler

from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.resources import strings
from src.bot.utils import cancel
from src.bot.utils.message import send_message_inline


def fallback(update, context):
    if not update.callback_query.data:
        return

    update.callback_query.answer()
    send_message_inline(update, context,
                        message=strings.prt_press_again_to_start,
                        reply=hub_keyboard(
                            update.callback_query.from_user.id))
    return ConversationHandler.END


fallbacks = [
    CallbackQueryHandler(fallback,
                         pattern=x) for x in CallbackEntry.dialog.value]
fallbacks.append(CommandHandler('cancel', cancel))
# fallbacks.append(CommandHandler('start', start_command))

fallback_handlers = fallbacks

# todo поменять все это говно на апдейт обьект а точнее взять с апдейт
#  обьекта колбек квери и
# todo не забудь добавить этот костыль во всех CH
# todo добавить колбек в хендлери, которые запускают диалог
# todo добавить в сущность бота, чтобы он автоматически наполнял эту херню
