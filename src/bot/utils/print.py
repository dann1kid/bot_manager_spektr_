import os
import shutil

import xlsxwriter
from docx.shared import Mm
from docxtpl import DocxTemplate, InlineImage
from openpyxl import load_workbook

from src.bot.Enumerators.Deeplink import DeeplinkEnum
from src.bot.config_bot import Stores, printed_orders_vault, template_vault, excel_file
from src.bot.models.pw_models import Order, Client, ContactClient, CommentOrder, \
    Device
from src.bot.resources import strings
from src.bot.utils.folders import create_folder
from src.bot.utils.qr_processing import qr_generate_link_file
from src.bot.utils.timestamp_parses import readable_data_from_datetime


def save_document(order_id, geo=None):
    # todo: перенести в утилиты
    """
        Сохраняет квитанцию в документ
    Args:
        geo: объект, где работает этот работник
        order_id: Идентификатор заказа.
        :return: Name document
    """

    # получаем всю инфу по заказу
    order = Order.get(Order.item_id == order_id)
    client = Client.get(Client.item_id == order.client)
    phone_number = ContactClient.get(
        ContactClient.client == order.client).number
    device = Device.get(Device.order == order)
    model_name = device.model_name
    trouble = order.trouble
    oriented_datetime = order.oriented_timestamp
    status = order.status
    timestamp = order.added
    brand_name = device.brand_name
    comments_receive = CommentOrder.select().where(
        CommentOrder.order == order_id)
    comments_receive = f"{comments_receive[0].comment if comments_receive.count() > 0 else strings.txt_no_data}"
    client_name = client.name
    client_surname = client.surname
    imei = device.imei
    prepayment = order.prepayment
    price = order.predefined_price

    template_doc = DocxTemplate(f"{template_vault}/template.docx")

    normalized_date = readable_data_from_datetime(timestamp)
    qr_file_extradite = qr_generate_link_file(DeeplinkEnum.extradite_order,
                                              client_id=client,
                                              type_id=order.item_id)
    inline_qr = InlineImage(template_doc, qr_file_extradite, width=Mm(60),
                            height=Mm(60))
    try:
        geo = Stores[f"{geo}"].value
    except KeyError:
        geo = Stores.bronnicy

    context = {
        'geo': geo,
        'create_order_date': normalized_date,
        'order_id': order_id,
        'client_name': client_name,
        'client_surname': client_surname,
        'pre_value': price,
        'prepayment': prepayment,
        'client_number': phone_number,
        'brand': brand_name,
        'model': model_name,
        'imei': imei,
        'trouble': trouble,
        'comments_receive': comments_receive,
        'qr': inline_qr,
    }

    template_doc.render(context)
    new_doc_name = f"{printed_orders_vault}/{order_id}.docx"
    template_doc.save(new_doc_name)

    return new_doc_name


def print_document(document_path: str) -> None:
    """
        печатает на принтере по умолчанию документ с именем document
    :type document_path: str
    """

    os.startfile(document_path, "print")

    return None


def create_label(order: Order):
    create_folder(excel_file)
    pattern = f'{excel_file}label_pattern.xlsx'
    copy = f'{excel_file}label_pattern_{order.item_id}.xlsx'
    wb = load_workbook(pattern)
    ws = wb.active
    cell_upper = ws.cell(row=1, column=2)
    cell_lower = ws.cell(row=2, column=2)
    cell_upper.value = order.item_id
    cell_lower.value = f"{order.devices[0].brand_name}" \
                       f"\n {order.devices[0].model_name} " \
                       f"\n {order.client.numbs[-1].number}"
    wb.save(copy)

    return copy
