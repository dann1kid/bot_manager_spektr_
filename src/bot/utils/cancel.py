#!/usr/bin/env python
# -*- coding: utf-8 -*-
from telegram.ext import ConversationHandler

from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.utils.message import send_message
from src.bot.resources import strings


def cancel(update, context):
    chat_id = update.message.text
    update.message.reply_text(strings.alert_return_main_menu,
                              reply_markup=hub_keyboard(chat_id))

    return ConversationHandler.END


def cancel_callback(update, context):
    return ConversationHandler.END


def cancel_conversation(update, context):
    chat_id = update.message.text
    update.message.reply_text(strings.alert_return_main_menu,
                              reply_markup=hub_keyboard(chat_id))

    return ConversationHandler.END


def back_to_main_menu(update, context):
    send_message(update, context, message=strings.txt_cancelled,
                 reply=hub_keyboard(update.message.chat_id))
    return ConversationHandler.END
