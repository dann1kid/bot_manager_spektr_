#!/usr/bin/env python
# -*- coding: utf-8 -*-

# standard libraries
from time import time

from src.bot.keyboards.TG_keyboards import make_reply_keyboard
from src.bot.models.pw_models import Client
from src.bot.resources import strings


class Hello:
    """ При нажатии кнопки /start инициирует начальный диалог
    В конструктор передаются параметры:
    :update : пакет апдейта
    :context : пакет контекста
    :client_id : id выданный pool_id.retrieve_id(client_id) управляющим классом
    """

    def __init__(self, update, context):
        # DB fields
        self.context = context
        self.update = update

        # update fields
        self.user = self.update.effective_user
        self.chat_id = self.update.message.chat_id
        self.user_name = self.user.username
        self.user_fist_name = self.user.first_name
        self.current_state = 'new_client'
        self.timestamp = time()

    def start(self):
        # начальный диалог
        # фильтр
        if self.user.is_bot:
            # TODO: ban user here
            pass

        else:
            self.context.bot.send_message(

                chat_id=self.chat_id,
                text=strings.txt_prompt_profile,
                reply_markup=self.profile_kbd(),

            )

            # сохраняем
            # self.fulfill()

    def check_users_contact(self):
        """Проверяет переданный юзером номер на присутствие в списке номеров у этого chat_id"""

        pass

    def fulfill(self):
        try:
            if self.user_name is None:
                self.user_name = ""
            Client.create(chat_id=self.chat_id, name=self.user_name,
                          current_state=self.current_state, )
        except Exception:
            pass

    def generate_keyboard(self):
        """
            Чекает права и текущую роль юзера,
            добавляет кнопки к текущему сообщению на их основе
        :return reply:
        """
        # get roles of user
        # must be replaced by generated buttons by rights
        # later brr

        buttons = ["Новый заказ",
                   "Мои заказы",
                   ]
        return make_reply_keyboard(buttons)

    def profile_kbd(self):
        return make_reply_keyboard(strings.btn_fulfill_profile_prompt)
