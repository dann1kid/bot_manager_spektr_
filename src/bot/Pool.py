#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sqlite3


class Pool:
    """
        Получение последнего айди.
        Создает сущность со всеми атрибутами настроек при старте бота,
        для работы  использовать метод retrieve_last_id
            """

    def __init__(self, db):
        """
            Инициализация при создании обьекта
            :param db: база данных, указывается при создании
            """
        self.db = f'{db}.sqlite'

        # ограничение принимаемых атрибутов:
        # либо предварительный список класса
        self.attrs = ['client_id',
                      'order_id',
                      'number_id',
                      ]
        # либо словарь из файла config_bot.py

        # динамическое добавление атрибутов
        for attr in self.tables:
            self.__dict__[attr] = self.get_last_id_db(attr)
            print(attr, self.__dict__[attr])

    def __getattr__(self, attr):
        """Перехват получения получения атрибута с фильтрацией"""
        # словарь из файла config_bot.py
        if attr in self.tables.keys():
            return self.retrieve_last_id(attr)

        elif attr == '__len__':
            pass

        else:
            print(f'Wrong attribute == {attr}.\n'
                  f'Get another attribute from this method.')

    def get_last_id_db(self, attr):
        """обращается к бд
        для получения максимального значения по attribute"""
        query = f'SELECT MAX ( {attr} ) FROM {self.tables.get(attr)}'
        try:
            connector = sqlite3.connect(self.db)

        except Exception as e:
            raise e

        else:
            cursor = connector.cursor()

            try:
                cursor.execute(query)
            except Exception as e:
                print(query, self.db)
                raise e
            else:
                try:
                    max_id = int(cursor.fetchone()[0])
                    print(max_id)
                    
                    if max_id is None:
                        max_id = 0
                except TypeError:
                    max_id = 0
            finally:
                connector.close()

            return max_id

    def retrieve_last_id(self, attr):
        """
           возвращает
           атрибут пула при обращении
           :param attr: действительный атрибут пула
           :return attr:
           """
        print('attr', attr)
        print('self.__dict__[attr]', self.__dict__[attr])
        
        # здесь скорее на всякий случай идет конвертация типа
        self.__dict__[attr] = int(self.__dict__[attr])
        return self.__dict__[attr]
