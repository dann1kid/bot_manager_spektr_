#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os


def run(run_file):
    with open(run_file, "r") as rnf:
        exec(rnf.read())


if __name__ == '__main__':
    path = "patches"
    files = os.listdir(path=path)
    for file in files:
        if "__" in file:
            continue
        print("patching", path + "." + file)
        # importlib.import_module(path + file[:-3])
        try:
            run(f"{path}/{file}")
        except Exception as e:
            print(f"{e}")
        # call(["python", f"{path}/{_}"])
        # os.system(f"py {_}")
        # Popen(f"py {_}")
