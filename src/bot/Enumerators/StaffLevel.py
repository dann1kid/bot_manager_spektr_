from enum import IntEnum


class StaffLevel(IntEnum):
    """
    Staff levels enumerator
    """

    client = 0
    courier = 1
    receiver = 2
    master = 3
    admin = 99

