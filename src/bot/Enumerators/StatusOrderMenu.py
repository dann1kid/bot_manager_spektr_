from enum import Enum

from src.bot.resources import strings


class StatusOrderMenu(Enum):
    """
    Class provides link between menus buttons
    and order statuses

    ex. 1:
    {Cls.field.name: Cls.field.value}

    ex. 2:
    match some_str:
        case Cls.field.name
            some_fun(Cls.field.value)

    Сокращения потому что енамы используются как ключи,
        а ключи с полным именованием не помещаются в метаданные кнопок Телеги
    """

    oe = strings.btn_t_under_eval  # на оценке todo: изменить статус на ue
    wdc = strings.wait_decision_client  # ожидает решение клиента, отправлять на ремонт или нет

    wcc = strings.txt_on_waiting_courier_at_client  # ЗАКАЗ У КЛИЕНТА, ОЖИДАЕТ ПРИНЯТИЯ ЛЮБЫМ КУРЬЕРОМ

    wc = strings.active_wait_courier  # ожидает прибытия КОНКРЕТНОГО курьера

    odw = strings.btn_t_on_delivery_work  # курьер везет в мастерскую

    iw = strings.btn_t_in_work  # в работе

    # я забыл промежуточный статус - на передаче с мастерской курьеру!
    # (он не нужен, ибо он ждет курьера в магазине, а затем сразу попадает нужному!) FIXME удалить
    twc = strings.btn_t_on_transmit_work_courier

    # еще один промежуточный статус - передача от курьера в мастерскую!
    # этот тоже лишний FIXME удалить
    dw = strings.btn_t_delivered_to_work

    # этот промежуточный статус ставится после присвоения курьером этого заказа для доставки

    # еще один промежуточный статус - передача с работы на выдачу, где ожидает
    # решения приемщика
    wsd = strings.txt_wait_delivery_selection

    dc = strings.btn_t_on_delivery_client  # курьер везет клиенту
    dwc = strings.txt_on_wait_courier  # c выдачи ожидает курьера  к клиенту
    wp = strings.status_wait_payment  # ожидает оплаты
    sd = strings.txt_on_extradition  # на выдаче ждет самовывоза клиентом todo: изменить статус на oe
    fin = strings.btn_t_finished  # завершён
    cld = strings.btn_t_cancelled_orders  # отменён
    cancel = strings.txt_cancel  # отмена

    """
    Order.update(status="ue").where(Order.status == 'oe').execute()
    Order.update(status="ue").where(Order.status == 'sd').execute()
    """

    """
    Order.update(status="oe").where(Order.status == 'oneval')
    
    Order.update(status="iw").where(Order.status == 'inwork')
    
    Order.update(status="odw").where(Order.status == 'ondelwork')
    Order.update(status="twc").where(Order.status == 'transcw')
    
    Order.update(status="dw").where(Order.status == 'delvdw')
    
    Order.update(status="wsd").where(Order.status == 'wseldel')
    
    Order.update(status="dc").where(Order.status == 'delivcl')
    
    Order.update(status="dwc").where(Order.status == 'delwc')
    
    Order.update(status="cld").where(Order.status == 'cans')
    """

    """
    oneval = strings.btn_t_under_eval  # на оценке
    inwork = strings.btn_t_in_work  # в работе
    ondelwork = strings.btn_t_on_delivery_work  # курьер везет в мастерскую
    # я забыл промежуточный статус - на передаче с мастерской курьеру!
    transcw = strings.btn_t_on_transmit_work_courier
    # еще один промежуточный статус - передача от курьера в мастерскую!
    delvdw = strings.btn_t_delivered_to_work
    # еще один промежуточный статус - передача с работы на выдачу, где ожидает
    # решения приемщика
    wseldel = strings.txt_wait_delivery_selection

    delivcl = strings.btn_t_on_delivery_client  # курьер везет клиенту
    delwc = strings.txt_on_wait_courier  # ожидает курьера для доставки с выдачи к клиенту
    wcc = strings.txt_on_waiting_courier_at_client  # Клиент ожидает курьера
    sd = strings.txt_on_extradition  # на выдаче ждет самовывоза клиентом
    fin = strings.btn_t_finished  # завершён
    cans = strings.btn_t_cancelled_orders  # отменён
    cancel = strings.txt_cancel  # отмена
    """
