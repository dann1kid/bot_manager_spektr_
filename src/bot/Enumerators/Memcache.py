"""
Энумератор для записей в редис, чтобы не перекрывать записи других диалогов
"""
from dataclasses import dataclass
from enum import IntEnum


@dataclass
class MemorySubset(IntEnum):
    """
    Singleton for access to namespaces in-memory vault
    """
    new_order_client = 0
    new_detail_client = 1
    new_order_staff = 2
    new_detail_staff = 3
    add_detail_staff = 4
    new_comment_detail = 5
    new_comment_order = 6
    mark_detail_as_ordered = 7
    order_get_payment = 8
    detail_get_payment = 9
    order_mark_to_extradition = 10
    order_send_to_extradition = 11
    detail_send_to_extradition = 12
    evaluate_order = 14
    delete_detail = 15
    client_commit_order = 16
    courier_pickup_order = 17
    client_cancel_order = 18
    courier_take_payment = 19
    print_labels = 20
    change_user = 21


@dataclass
class Memcache:
    """
    Singleton for access to get data from redis
    Use:
    Memcache(chat_id=chat_id, key="key").get_subset()
    """
    chat_id: str | int = 0
    subset: MemorySubset = None

    def get_subset(self) -> str:
        return f"{self.chat_id}_{self.subset.value}"
