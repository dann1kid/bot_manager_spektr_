from dataclasses import dataclass, fields
from enum import Enum

from src.bot.config_bot import TG_DEEPLINK

# TODO заменить все связанные сравнения
from src.bot.models.pw_models import Client


class DeeplinkEnum(Enum):
    """
    Deeplink /start command enumerator
    """
    extradite_order = 1
    register_user_order = 2
    register_user_detail = 3
    transmit_order_from_store_courier = 4
    transmit_order_from_client_courier = 5
    transmit_order_from_courier_store = 6
    transmit_order_from_courier_to_client = 7


# Todo перенести в контроллеры

@dataclass
class Deeplink:
    """
    Class provides backlink between deeplink data and deeplink url generation.

    enum_type: DeeplinkEnum field
    client_id: Client.client_id
    type_id: any model data object.object_id (Order.order_id etc)

    Usage:
        deeplink = Deeplink()
        deeplink.parse_link(link)

        if check_args(context.args):
            match deeplink.enum_type:
                case DeeplinkEnum.extradite_order:
                    ...
    """

    enum_type: int = None  # DeeplinkEnum(enum_type)
    type_id: int = None  # vector type
    client_id: int = None  # init client_id

    def generate_link(self):
        return f"https://telegram.me/{TG_DEEPLINK}?start=" \
               f"{self.enum_type}-{self.type_id}-{self.client_id}"

    def parse_link(self, list_args: list = None):
        list_args: list = list_args[0].split("-")
        self.enum_type = int(list_args[0])
        self.type_id = int(list_args[1])
        self.client_id = int(list_args[2])

    @staticmethod
    def check_args(raw_args: str = None) -> bool:
        """check args """
        if len(raw_args) == 0:
            return False
        else:
            return True

    @staticmethod
    def check_args_length(raw_args):
        try:
            raw_args = raw_args[0]
            list_args: list = raw_args.split("-")
        except TypeError:
            return False
        else:
            if len(list_args) != len(fields(Deeplink)):
                return False
            else:
                return True
