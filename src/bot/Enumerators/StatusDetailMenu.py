from enum import Enum

from src.bot.resources import strings


class StatusDetailMenu(Enum):
    """
    Class provides link between menus buttons
    and order statuses

    ex. 1:

    {Cls.field.name: Cls.field.value}


    ex. 2:

    match some_str:
        case Cls.field.name
            some_fun(Cls.field.value)
    """
    on_eval = strings.btn_t_under_eval
    wttrdr = strings.txt_not_ordered
    ordrd = strings.txt_ordered
    sldlvr = strings.txt_wait_delivery_selection
    osldlvr = strings.txt_on_extradition
    odwc = strings.txt_on_wait_courier  # ожидает курьера для доставки с выдачи к клиенту
    od = strings.txt_on_delivery
    finished = strings.btn_t_finished  # завершён
    cancelled = strings.btn_t_cancelled_orders  # отменён
    """
    on_eval = strings.btn_t_under_eval
    wait_to_order = strings.txt_not_ordered
    ordered = strings.txt_ordered
    select_delivery = strings.txt_wait_delivery_selection
    on_self_delivery = strings.txt_on_extradition
    on_del_wait_c = strings.txt_on_wait_courier  # ожидает курьера для доставки с выдачи к клиенту
    on_delivery = strings.txt_on_delivery
    finished = strings.btn_t_finished  # завершён
    cancelled = strings.btn_t_cancelled_orders  # отменён
    """
