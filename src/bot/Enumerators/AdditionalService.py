from enum import Enum


class AdditionalServiceEnum(Enum):
    added = "added"
    done = "done"
    deleted = "deleted"
