from BotManager import BotManager
from config_bot import TG_BOT_TOKEN


def main() -> None:
    # in memory kv db

    try:
        bot = BotManager(TG_BOT_TOKEN)
        bot.start_poll()
    except Exception as e:
        print(e)
        exit(input("Press any key to exit."))


if __name__ == '__main__':
    main()
