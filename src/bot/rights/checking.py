# !/usr/bin/env python
# -*- coding: utf-8 -*-

from peewee import DoesNotExist
from telegram.ext import ConversationHandler

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Client
from src.bot.resources import strings
from src.bot.utils.message import send_message, get_chat_id


def access_level(chat_id: str | int, required_level: StaffLevel = None) -> bool:
    """
    Access level checking function
    Usage: bool_in_staff = access_level(chat_id=12345,
                                        required_level=StaffLevel.receiver)
    """

    client_staff_status = Client.get(Client.chat_id == chat_id).staff_status

    if client_staff_status >= required_level.value:
        return True
    else:
        return False


def access_level_wrapper(staff_status: StaffLevel = None) -> callable:
    """Deco for turn back on selected state in conversation handler
    @access_level_wrapper(staff_status=StaffLevel.receiver)
    Returns: func
    """

    def wrap(func):
        def wrapper(*args, **kwargs):
            try:
                update = args[0]
                context = args[1]
                try:
                    client = Client.get(
                        Client.chat_id == get_chat_id(update))
                except DoesNotExist:
                    send_message(update, context,
                                 message=strings.txt_error_to_start_prompt,
                                 reply=hub_keyboard(update.message.chat_id))
                    return ConversationHandler.END
                else:

                    # есть очень странный баг, поскольку не принимается обьект
                    # с енама(интенам) по атрибуту. Крч он почему то передает имя атрибута
                    # в ввиде строки вместо его значения
                    print(staff_status)
                    try:
                        client.staff_status >= staff_status
                    except TypeError:
                        if client.staff_status >= StaffLevel[staff_status]:
                            return func(*args, **kwargs)
                        else:
                            send_message(update, context,
                                         message=strings.err_access_denied)
                            return ConversationHandler.END
                    else:
                        if client.staff_status >= staff_status:
                            return func(*args, **kwargs)
                        else:
                            send_message(update, context,
                                         message=strings.err_access_denied)
                            return ConversationHandler.END
            except AttributeError:
                raise

        return wrapper

    return wrap
