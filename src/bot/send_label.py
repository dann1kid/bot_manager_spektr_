#!/usr/bin/env python
# -*- coding: utf-8 -*-
# send file to printer bots like a human

import sys

import asyncio
from telethon import TelegramClient

from src.bot.config_bot import API_HASH_LABELS


async def send_message_telethon(exc, count):
    """
    geo staff: shop place
    doc: docx file
    exc: Excel file
    count: count of files
    """

    try:
        # telethon
        api_id = 24073268
        api_hash = f"{API_HASH_LABELS}"
        # client_telethon = TelegramClient('session_name', api_id, api_hash)
        # client_telethon.start()

    except Exception as e:
        raise e
    async with TelegramClient('session_name_3', api_id, api_hash) as client:
        if exc:
            await client.send_file('label_printer_br_bot', exc, caption=count)
            await client.send_file(247926132, exc, caption=count)

    return None


def main():
    print(sys.argv[1], sys.argv[2])
    print("send label called")
    try:
        print("printed doc")
        asyncio.run(send_message_telethon(sys.argv[1], sys.argv[2]))
    except Exception as e:
        print(e)
    else:
        return True


if __name__ == '__main__':
    main()
