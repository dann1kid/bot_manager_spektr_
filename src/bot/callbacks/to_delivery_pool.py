#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from time import time

from telegram import ParseMode

from src import save_clients_function_db, get_selected_function, get_comment_order, save_comment
from src import change_status_order
from src import get_client_id
from src import change_status_work
from src import insert_delivery_pool
from src import get_order_status

from src.bot.keyboards import make_reply_keyboard

from utils.message import send_message_callback
from src import callback_delete_message


# алгоритм
""" на стандартную кнопку перемещения в пул идет запрос конечной суммы
 функция юзера переключается на установку оплаты
 следующее эхо идет в установку оплаты"""


def ask_for_price_to_delivery(update, context):
    """
    Запрашивает целевую сумму для заданного выполненного заказа.
    Устанавливает выбранную функцию для юзера, и когда он ответит,
    то  эхо функция перенаправит на следующий шаг
    Args:
        update:
        context:

    Returns:

    """

    query = update.callback_query
    print('query.data', query.order)

    # выдергивает номер заказа из колбэка
    order_id = re.findall(r'[\d]{1,15}', query.order)

    buttons = [
        "Пропустить",
    ]

    reply_keyboard = make_reply_keyboard(buttons)
    send_message_callback(update, context,
                          message="Итоговая сумма и комментарий",
                          reply=reply_keyboard,
                          )

    save_clients_function_db(chat_id=update.message.chat_id, selected_function="price_on_delivery", params=order_id)

    return None


def move_delivery_pool_orders_handler(update, context):
    """
        Перемещает выбранный заказ в пул выдачи

     """

    message = update.message.text
    chat_id = update.effective_chat.id
    client_id = get_client_id(chat_id=chat_id)

    # выдергивает номер заказа функции
    function, order_id = get_selected_function(chat_id=chat_id)
    current_status_order = get_order_status(order_id=order_id)

    if current_status_order[0] != 'in_work':
        send_message_callback(update,
                              context,
                              message=f"Заказ №{order_id} уже не доступен!",
                              parse_mode=ParseMode.HTML,
                              )
        callback_delete_message(update, context)
        return None

    # Обновляем комментарий
    comment = f"{get_comment_order(order_id=order_id)} \n {message}"
    save_comment(order_id=order_id, comment=comment)

    # меняет в таблице работы статус на выполненный
    change_status_work(order_id=order_id,
                       status='on_delivery',
                       )

    # меняет статус заказа на on_delivery
    change_status_order(order_id=order_id, status='on_delivery')

    # добавляет в таблицу с выдачей строку
    insert_delivery_pool(order_id=order_id,
                         client_id=client_id,
                         status='on_delivery',
                         timestamp=time(),
                         )

    # отвечает исполнителю о перемещении
    send_message_callback(update,
                          context,
                          message=f"Перемещаю заказ <b>№{order_id}</b> в пул выдачи. \n"
                                  f"Теперь он доступен только на выдаче. \n"
                                  f"Переключитесь на роль приемщика (если она ему доступна)"
                                  f" и нажмите на кнопку 'На выдаче'",
                          parse_mode=ParseMode.HTML,
                          )
    # Удаляет из списка сообщений свободных заказов сообщение с заказом
    callback_delete_message(update, context)

    return None
