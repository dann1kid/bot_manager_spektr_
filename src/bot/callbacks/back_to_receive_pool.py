#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from telegram import ParseMode
from src import change_status_order
from src import get_order_status
from src import delete_in_work_by_status
from src import callback_delete_message
from utils.message import send_message_callback


def change_back_received_pool_orders_handler(update, context):
    """
        Перемещает выбранный заказ обратно в пул принятых заказов

     """
    query = update.callback_query
    print('query.data', query.order)
    # выдергивает номер заказа из колбэка
    order_id, work_id = re.findall(r'[\d]{1,15}', query.order)

    current_status_order = get_order_status(order_id=order_id)
    if current_status_order[0] != 'in_work':
        send_message_callback(update,
                              context,
                              message=f"Заказ №{order_id} уже не доступен!",
                              parse_mode=ParseMode.HTML,
                              )
        callback_delete_message(update, context)
        return None

    # меняет в таблице работы статус на отмененный
    delete_in_work_by_status(work_id=work_id)
    # меняет в таблице заказов статус на доступный
    change_status_order(order_id=order_id, status='end_ok')
    # отвечает исполнителю о перемещении
    send_message_callback(update,
                          context,
                          message=f"Перемещаю заказ <b>№{order_id}</b> "
                                  f"в пул доступных к работе заказов. \n"
                                  f"\n"
                                  f"Для просмотра доступных работ нажмите 'Новые заказы'",
                          parse_mode=ParseMode.HTML,
                          )
    # Удаляет из списка сообщений свободных заказов сообщение с заказом
    callback_delete_message(update, context)

    # добавляем логику к кнопке в работе
    # выбрать все заказы по чат ид

    pass
