#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from src import save_clients_function_db
from src import save_details_finished_db

from utils.message import send_message_callback
from src import callback_delete_message



def take_payment_detail(update, context):
    """ функция принимает колбек с кнопки на меседже с деталью

        Решил пока просто удалять меседж, потому что путаница происходит из-за колбек-хелла
        Вместе свести трудно поскольку конверсейшнхендлер с колбек ентри
        не вызывается колбеком нормально,
        а перехватывает все колбеки
        """

    query = update.callback_query
    chat_id = update.effective_chat.id

    data = re.findall(r'[\d]{1,15}', query.order)
    function = re.match(r'^\D{1,200}', query.order).group(0)[0:-1]
    detail_id = data[0]

    save_clients_function_db(chat_id=chat_id, selected_function=function, params=detail_id)
    callback_delete_message(update, context)
    send_message_callback(update, context, message=f"Удаление из списка...",)
    save_details_finished_db(detail_id=detail_id)
    save_clients_function_db(chat_id=chat_id, selected_function='', params='')

    return None
