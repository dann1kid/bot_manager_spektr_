# !/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import CallbackQueryHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters

from src import save_payments_in_db
from src import get_order_id_on_payment
from src import save_payments_statement_in_db
from src import get_delivery_id_by_order

from src.bot.keyboards import make_reply_keyboard
from src.bot.keyboards import hub_keyboard
from src.bot.keyboards import inline_kbd_orders_on_delivery_ok
from src import alert_return_main_menu

from utils.message import send_message_callback
from utils.message import send_message

entry_point_pattern = "^take payments"

TAKE_PRICE = "taking_price"


def cancel(update, context, order_id):
    """
        Текущий заказ помечается как завершенный неоконченный.

    :param order_id:
    :param context:
    :type update: object
    """

    update.message.reply_text(
        alert_return_main_menu,
        reply_markup=hub_keyboard(update.message.chat_id))

    return ConversationHandler.END


def take_payment(update, context):
    """

    :param update:
    :param context:
    :return:
    """
    print("Апдейт на  запросе оплаты", update)
    print("Контекст на  запросе оплаты", context)
    buttons = [
        "0",
        "500",
        "1000",
        "1500",
        "2500",
        "3500",
        "диагностика",
    ]
    reply_keyboard = make_reply_keyboard(buttons)
    query = update.callback_query
    print(query)
    chat_id = query.message.chat_id
    message_text = query.message.text
    print("message_text", message_text)
    message_id = update.callback_query.message.message_id
    print('message_id', message_id)
    data = re.findall(r'[\d]{1,15}', query.order)
    order_id, work_id = data[0], data[1]
    context.bot.delete_message(chat_id, message_id)
    save_payments_statement_in_db(order_id=order_id,
                                  chat_id=chat_id,
                                  message_id=message_id,
                                  message_text=message_text)
    send_message_callback(update,
                          context,
                          message=f"Сколько таки денег вы взяли у клиента по заказу №{order_id}? 🌝",
                          reply=reply_keyboard)
    update.callback_query.answer()

    return TAKE_PRICE


def save_payments(update, context):
    print("Апдейт на принятии оплаты", context)
    chat_id = update.message.chat_id
    print("chat_id = ", chat_id)
    order_id, message_id, message_text = get_order_id_on_payment(
        chat_id=chat_id)
    kbd = hub_keyboard(chat_id)
    payment = update.message.text
    save_payments_in_db(payment=payment, order_id=order_id)
    send_message(update,
                 context,
                 message="Оплата зафиксирована.",
                 reply=kbd,
                 )
    delivery_id = get_delivery_id_by_order(order_id=order_id)
    new_kbd = inline_kbd_orders_on_delivery_ok(order_id, delivery_id)
    send_message(update, context, message=message_text, reply=new_kbd)

    return ConversationHandler.END


get_payments_on_deliver_handler = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(take_payment, pattern=entry_point_pattern)],

    states={
        TAKE_PRICE: [MessageHandler(Filters.text, save_payments)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=True
)
