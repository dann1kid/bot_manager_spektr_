#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

from telegram import ParseMode

from src import get_order_in_orders_by_order_id
from utils.message import send_message_callback
from src import readable_data_from_timestamp


def order_message(row):

    add_data = readable_data_from_timestamp(float(row['timestamp']))

    message = f'<b>№ заказа</b>: {row["order_id"]} \n' \
              f'Добавлен: {add_data} \n' \
              f'<b>Статус</b>: {row["status"]} \n' \
              f' \n' \
              f'<b><i>Информация об аппарате</i></b>\n' \
              f'<i>Производитель</i>: {row["brand_name"]} \n' \
              f'<i>Модель</i>: {row["model_name"]} \n' \
              f'<i>Целевая проблема</i>: {row["trouble"]} \n' \
              f'<i>Комментарий при приеме</i>: {row["comments_receive"]} \n' \
              f'<i>Цена работы</i>: {row["price"]} \n' \
              f'<i>Предоплата</i>: {row["prepayment"]} \n' \
              f' \n' \
              f'<b><i>Информация о клиенте</i></b>\n' \
              f'<i>Имя клиента</i>: {row["client_name"]} \n' \
              f'<i>Номер телефона клиента</i>: \n {row["phone_number"]} \n' \
              f'\n' \
        # f'<i>Исполнитель</i>: \n {master_id}'

    return message


def full_search_order_id_callback(update, context):
    """
        Отправляет форматированное сообщение
        по выбранному заказу (колбек) из полнотекстового поиска в бд

    :param update:
    :param context:
    :return:
    """
    query = update.callback_query

    # поиск чисел
    data = re.findall(r'[\d]{1,15}', query.order)
    detail_id = data[0]
    print("data", data, "detail_id", detail_id)

    # берем дату по заказу
    row, columns = get_order_in_orders_by_order_id(order_id=detail_id)
    try:
        current_row = dict(zip(columns, row))
    except TypeError:
        message = "Такого заказа нет."
        send_message_callback(update, context, message=message)
    else:
        print("full search current_row", current_row)
        send_message_callback(update, context,
                              message=order_message(current_row), parse_mode=ParseMode.HTML)

    return None
