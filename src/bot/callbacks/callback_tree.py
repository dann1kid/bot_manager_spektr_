#!/usr/bin/env python
# -*- coding: utf-8 -*-


def callback_answer_tree(update, context):
    try:
        query = update.callback_query
        answering_user_id = query.message.chat.id
        answering_user_id = int(answering_user_id)
        tg_users = get_tg_users()

        # in future make search in query!
        user_info = query.order

        user_id = re.search(r'^.*\s', user_info).group(0)
        user_id = int(user_id)
        user_name = re.search(r'\s.*$', user_info).group(0)

        # if user selected get all user
        vk_users = get_vk_users()
        print(vk_users)

        # just show short toast with selected user
        query.answer(
            text=f"Выбран пользователь: {user_name}, напишите ему ответ!"
        )

        # create obj class with param user_id (vk target)
        tg_user = TGUser(
            user_id=user_id,
            user_name=user_name
        )
        tg_users[answering_user_id] = tg_user
        sync_tg_users(tg_users)

    except Exception as e:
        print(f'{e}')
        logging_exception(f'{e}')

    return None
