#!/usr/bin/env python
# -*- coding: utf-8 -*-


from src import save_clients_function_db
from src.bot.keyboards import make_reply_keyboard
from utils.parsers import parse_query
from utils.message import send_message_callback


def make_query(update, context):
    """
        Отправляет сформированный запрос юзеру
    :param update: обьект данных с сервера telegram
    :param context: обьект контекста
    :return: None
    """
    # парсим данные
    ticket_id = parse_query(update)
    # получение админского chat_id
    admin_chat_id = update.effective_chat.id
    # выбранная функция пользователем - ответ на заданное обращение
    save_clients_function_db(chat_id=admin_chat_id, selected_function="price_query", params=ticket_id)
    # формируем клавиатуру для реплай кнопок
    reply = make_reply()
    # шлем меседж
    message = "Ответь по этому тикету или нажми на кнопку"
    send_message_callback(update, context, message=message, reply=reply)


def make_reply():
    """
       формирует клавиатуру для реплай
    :return:
    """

    buttons = ["Отправьте фото",
               "Опишите неисправность",
               "Укажите точную модель",
               ]
    return make_reply_keyboard(buttons)
