#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from time import time

from telegram import ParseMode

from src import change_status_order
from src import get_order_status
from src import insert_delivery_pool
from src import get_client_id
from src import callback_delete_message
from utils.message import send_message_callback


def move_new_order_to_delivery_callback(update, context):
    """
        перемещает новый выбранный заказ сразу в пул выдачи

    """
    query = update.callback_query
    chat_id = update.effective_chat.id
    data = re.findall(r'[\d]{1,15}', query.order)
    order_id = data[0]
    client_id = get_client_id(chat_id=chat_id)
    current_status_order = get_order_status(order_id=order_id)
    if current_status_order[0] != 'end_ok':
        send_message_callback(update,
                              context,
                              message=f"Заказ №{order_id} уже не доступен!",
                              parse_mode=ParseMode.HTML,
                              )
        callback_delete_message(update, context)
        return None

    # меняет статус заказа на on_delivery
    change_status_order(order_id=order_id, status='on_delivery')

    # добавляет в таблицу с выдачей строку
    insert_delivery_pool(order_id=order_id,
                         client_id=client_id,
                         status='on_delivery',
                         timestamp=time(),
                         )

    # отвечает исполнителю о перемещении
    send_message_callback(update,
                          context,
                          message=f"Перемещаю заказ <b>№{order_id}</b> в пул выдачи. \n"
                                  f"Теперь он доступен только на выдаче.",
                          parse_mode=ParseMode.HTML,
                          )
    # Удаляет из списка сообщений свободных заказов сообщение с заказом
    callback_delete_message(update, context)
