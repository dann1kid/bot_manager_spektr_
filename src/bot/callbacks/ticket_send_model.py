#!/usr/bin/env python.
# -*- coding: utf-8 -*-
from src import save_clients_function_db
from utils.parsers import parse_query
from utils.message import send_message_callback


def ask_model(update, context):
    """
    Ответ на колбэк ticket send photo client
        Запрашивает фото и сохраняет выбранную функцию.
    :param update:
    :param context:
    :return:
    """
    # парсим данные
    ticket_id = parse_query(update)
    chat_id = update.effective_chat.id
    # send message
    send_message_callback(update, context, message="Жду модель или фото с надписью ")
    # this callback are from client side not admin and saves the function in client table
    save_clients_function_db(chat_id=chat_id, selected_function='ticket send model', params=f'{ticket_id}')

    return None
