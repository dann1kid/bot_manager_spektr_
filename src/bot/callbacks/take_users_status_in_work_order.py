# !/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from src import save_clients_function_db
from utils.message import send_message_callback


def save_users_selected_fun_in_work_fun(update, context):
    """
        Сохраняет юзерс selected_function в бд .
    :param update:
    :param context:
    :return:
    """
    query = update.callback_query
    # chat_id исполнителя работы, который нажимает эту кнопку
    chat_id = update.effective_chat.id

    # выдергивает номер заказа из колбэка
    data = re.findall(r'[\d]{1,15}', query.order)
    function = re.match(r'^\D{1,200}', query.order).group(0)
    order_id, work_id = data[0], data[1]

    # сохраняет за указанным заказом переданный юзером статус (в работе, ждет деталь и тп) и аргументы
    save_clients_function_db(chat_id=chat_id, selected_function=function,
                             params=order_id)

    # Делаем тост юзеру
    send_message_callback(update, context,
                          message=f"Добавляешь комментарий к заказу №{order_id}, жду твой текст")

    query.answer(
        text=f"Добавляешь комментарий к заказу №{order_id}, жду твой текст"
    )

    return None
