#!/usr/bin/env python
# -*- coding: utf-8 -*-
# send file to printer bots like a human

import sys

import asyncio
from telethon import TelegramClient

from src.bot.config_bot import API_HASH_LABELS

async def send_message_telethon(geo_staff, doc, exc, count, send_docx: bool = False):
    """
    geo staff: shop place
    doc: docx file
    exc: Excel file
    count: count of files
    """

    try:
        # telethon
        api_id = 24073268
        api_hash = f"{API_HASH_LABELS}"
        # client_telethon = TelegramClient('session_name', api_id, api_hash)
        # client_telethon.start()

    except Exception as e:
        raise e
    async with TelegramClient('send_docx', api_id, api_hash) as client:
        if geo_staff == 'bronnicy':
                await client.send_file('printer_bronnicy_bot', doc)
        await client.send_file(247926132, doc)

        if exc:
            if count:
                await client.send_file('label_printer_br_bot', exc, caption=count)
                await client.send_file(247926132, exc, caption=count)
            else:
                await client.send_file(247926132, exc)

    return None


def main():
    print(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], )
    try:
        print("printed doc")
        asyncio.run(send_message_telethon(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]))
    except Exception as e:
        print(e)
    else:
        return True


if __name__ == '__main__':
    main()
