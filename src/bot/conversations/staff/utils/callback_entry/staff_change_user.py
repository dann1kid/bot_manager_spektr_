import json
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.models.pw_models import Client
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached, get_memcached_item
from src.bot.utils.message import send_message_enum, cancel_conversation, \
    send_message, get_chat_id, send_message_inline_enum

"""
Диалог для изменения полей юзера
"""

memory_subset = MemorySubset.change_user


class ConversationStates(Enum):
    SELECT_FIELD = {"txt": strings.prompt_select_field,
                    "buttons": [
                        strings.btn_name,
                        strings.btn_surname,
                        strings.btn_middle_name,
                        strings.txt_cancel
                    ]}
    INPUT_NAME = {"txt": strings.prompt_new_name,
                  "buttons": [
                      strings.txt_cancel,
                  ]}
    INPUT_SURNAME = {"txt": strings.prompt_new_surname,
                     "buttons": [
                         strings.txt_cancel,
                     ]}
    INPUT_MIDDLE_NAME = {"txt": strings.prompt_new_middle_name,
                         "buttons": [
                             strings.txt_cancel,
                         ]}


@access_level_wrapper(staff_status=StaffLevel.admin)
def fun_on_entry(update, context):
    chat_id = get_chat_id(update)
    update.callback_query.answer()
    send_message_inline_enum(update, context,
                             enum_state=ConversationStates.SELECT_FIELD)

    data = json.loads(update.callback_query.data.split("-")[-1])
    subset_key = Memcache(
        chat_id=chat_id,
        subset=memory_subset).get_subset()
    write_memcached(subset_key=subset_key, value=data["client"])

    return ConversationStates.SELECT_FIELD.name


@cancel_conversation
def parse_confirm(update, context):
    chat_id = update.message.chat_id
    message_text = update.message.text

    match message_text:
        case strings.btn_name:
            send_message_enum(update, context,
                              enum_state=ConversationStates.INPUT_NAME)
            state = ConversationStates.INPUT_NAME.name

        case strings.btn_surname:
            send_message_enum(update, context,
                              enum_state=ConversationStates.INPUT_SURNAME)
            state = ConversationStates.INPUT_SURNAME.name

        case strings.btn_middle_name:
            send_message_enum(update, context,
                              enum_state=ConversationStates.INPUT_MIDDLE_NAME)
            state = ConversationStates.INPUT_MIDDLE_NAME.name

        case _:
            send_message_enum(update, context,
                              enum_state=ConversationStates.SELECT_FIELD)

            state = ConversationStates.SELECT_FIELD.name

    return state


@cancel_conversation
def parse_name(update, context):
    message_text = update.message.text
    chat_id = update.message.chat_id
    item_id = get_memcached_item(chat_id, memory_subset)

    client = Client.get(Client.item_id == item_id)
    client.name = message_text
    client.save()
    send_message(update, context, message=strings.confirm_value_changed)

    return ConversationHandler.END


@cancel_conversation
def parse_middle_name(update, context):
    message_text = update.message.text
    chat_id = update.message.chat_id
    item_id = get_memcached_item(chat_id, memory_subset)

    client = Client.get(Client.item_id == item_id)
    client.middle_name = message_text
    client.save()
    send_message(update, context, message=strings.confirm_value_changed)

    return ConversationHandler.END


@cancel_conversation
def parse_surname(update, context):
    message_text = update.message.text
    chat_id = update.message.chat_id
    item_id = get_memcached_item(chat_id, memory_subset)

    client = Client.get(Client.item_id == item_id)
    client.surname = message_text
    client.save()
    send_message(update, context, message=strings.confirm_value_changed)

    return ConversationHandler.END


handler_change_users_fields = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.change_user.value)],
    states={
        ConversationStates.SELECT_FIELD.name: [
            MessageHandler(Filters.text, parse_confirm)
        ],
        ConversationStates.INPUT_NAME.name: [
            MessageHandler(Filters.text, parse_name)
        ],
        ConversationStates.INPUT_SURNAME.name: [
            MessageHandler(Filters.text, parse_surname)
        ],
        ConversationStates.INPUT_MIDDLE_NAME.name: [
            MessageHandler(Filters.text, parse_middle_name)
        ],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
