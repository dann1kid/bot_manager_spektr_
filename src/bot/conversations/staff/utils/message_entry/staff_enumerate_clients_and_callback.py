import json
from enum import Enum

import telegram
from telegram import ParseMode
from telegram.ext import ConversationHandler, MessageHandler, Filters, CallbackQueryHandler

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.MessageBuilder.SearchUsersInline import SearchUsersInline
from src.bot.MessageBuilder.UserMessageBuilder import UserMessageBuilder
from src.bot.MessageBuilder.UsersInline import UsersInline
from src.bot.entry_points.CallbackEntry import CallbackParser, CallbackEntry
from src.bot.entry_points.MessageEntry import EntryPoints
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Client, SearchQuery
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message_enum, cancel_conversation, send_message

ENTRY_POINT = Filters.regex(f"^{EntryPoints.admin_change_client_data.value}$")


class ConversationStates(Enum):
    SELECT = {"txt": strings.prompt_get_users_regime,
              "buttons": [strings.btn_select_user,
                          strings.btn_search_user,
                          strings.txt_cancel,
                          ]}
    SEARCH = {"txt": strings.select_users_fields,
              "buttons": [strings.txt_cancel,
                          ]
              }
    SELECTED = {"txt": "selection"}
    ASK_AGAIN = {"txt": strings.prompt_search_again,
                 "buttons": [
                     strings.txt_yes,
                     strings.txt_no,
                     strings.txt_cancel,
                 ],
                 }


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    """
        Входная функция выбор

    :param update:
    :param context:
    :return:
    """
    send_message_enum(update, context, ConversationStates.SELECT)

    return ConversationStates.SELECT.name


@cancel_conversation
def parse_selection(update, context):
    # collect data
    chat_id = update.message.chat_id
    text = update.message.text
    match text:
        case strings.btn_search_user:
            send_message_enum(update, context, ConversationStates.SEARCH)
            return ConversationStates.SEARCH.name

        case strings.btn_select_user:
            markup = UsersInline()
            send_message(update, context, message=strings.prompt_available_users,
                         reply=markup.paginator.markup)
        case _:
            return ConversationStates.SELECT.name

    return ConversationHandler.END


@cancel_conversation
def parse_query(update, context):
    text = update.message.text
    chat_id = update.message.chat_id

    search_query = SearchQuery.create(
        client=Client.get(Client.chat_id == chat_id),
        query_text=text,
    )
    # todo заменить обьект меню
    markup = SearchUsersInline(search_query=search_query)

    if markup.items.count() > 0:
        send_message(update, context, message=f"{strings.txt_search_result} {text}",
                     reply=markup.paginator.markup)
        send_message(update, context, message=strings.main_menu,
                     reply=hub_keyboard(chat_id))
        state = ConversationHandler.END

    else:
        send_message_enum(update, context, enum_state=ConversationStates.ASK_AGAIN)
        state = ConversationStates.ASK_AGAIN.name

    return state


@cancel_conversation
def parse_ask_again(update, context):
    chat_id = update.message.chat_id
    users_text = update.message.text

    match users_text:
        case strings.txt_yes:
            send_message_enum(update, context, enum_state=ConversationStates.SEARCH)
            state = ConversationStates.SEARCH.name
        case strings.txt_no:
            send_message(update, context, message=strings.main_menu, reply=hub_keyboard(chat_id=chat_id))
            state = ConversationHandler.END
        case _:
            send_message_enum(update, context, enum_state=ConversationStates.ASK_AGAIN)
            state = ConversationStates.ASK_AGAIN.name

    return state


staff_enumerate_users = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_on_entry)],

    states={
        ConversationStates.SELECT.name: [
            MessageHandler(Filters.text, parse_selection)
        ],
        ConversationStates.SEARCH.name: [
            MessageHandler(Filters.text, parse_query)
        ],
        ConversationStates.ASK_AGAIN.name: [
            MessageHandler(Filters.text, parse_ask_again)
        ],

    },
    fallbacks=fallback_handlers, allow_reentry=True
)

"""
    Inline buttons section
"""


def switch_page(update, context):
    print("SWITCH PAGE USERS SEARCH")
    data = CallbackParser.parse_callback(update.callback_query.data)
    markup = SearchUsersInline(search_query=SearchQuery.get(SearchQuery.item_id == data["query"]),
                               page=data["page"])

    try:
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


callback_handler_switch_page_user = CallbackQueryHandler(
    switch_page,
    pattern=f"^{CallbackEntry.paginator_page_user_search.value}"
)


def show_user(update, context):
    """Функция с показыванием объекта деталей и добавлением кнопок
    в зависимости от текущего контекста.
    Например, для деталей которые еще не заказаны, есть галка "Заказано" и
    кнопка "удалить".
    """
    chat_id = update.callback_query.message.chat.id,
    json_data = json.loads(update.callback_query.data.split("-")[-1])
    item = Client.get(Client.item_id == json_data["client"])

    raw_data = update.callback_query.data.split("-")[-1]

    builder = UserMessageBuilder(client=item, mode=UserMessageBuilder.BuilderMode.all.value)
    message = builder.create_message()
    reply = builder.create_buttons_inline(raw_data)

    # Меняем текст
    update.callback_query.edit_message_text(
        text=message,
        parse_mode=ParseMode.HTML,
    )
    # меняем кнопки
    update.callback_query.edit_message_reply_markup(reply_markup=reply)

    return ConversationHandler.END


callback_handler_show_user = CallbackQueryHandler(
    show_user,
    pattern=f"^{CallbackEntry.show_user.value}")


def back_to_enumerate_user(update, context):
    data = json.loads(update.callback_query.data.split("-")[-1])

    match UserMessageBuilder.BuilderMode(data["mode"]).value:
        case 1:
            markup = UsersInline(page=int(data["page"]), mode=int(data["mode"]))
        case 2:
            search_query = SearchQuery.get(SearchQuery.item_id == data["search_id"])
            markup = SearchUsersInline(search_query=search_query,
                                       page=int(data["page"]))
        case _:
            markup = UsersInline(page=int(data["page"]), mode=int(data["mode"]))

    try:
        update.callback_query.edit_message_text(
            text=strings.txt_available_details)
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


callback_handler_show_user_back = CallbackQueryHandler(
    back_to_enumerate_user,
    pattern=f"^{CallbackEntry.back_show_user.value}")


def show_user_search(update, context):
    """Функция с показыванием объекта клиента и добавлением кнопок
    в зависимости от текущего контекста.
    """
    chat_id = update.callback_query.message.chat.id,
    json_data = json.loads(update.callback_query.data.split("-")[-1])
    item = Client.get(Client.item_id == json_data["client"])
    raw_data = update.callback_query.data.split("-")[-1]

    builder = UserMessageBuilder(client=item, mode=UserMessageBuilder.BuilderMode.search.value)
    message = builder.create_message()
    reply = builder.create_buttons_inline(raw_data)

    # Меняем текст
    update.callback_query.edit_message_text(
        text=message,
        parse_mode=ParseMode.HTML,
    )
    # меняем кнопки
    update.callback_query.edit_message_reply_markup(reply_markup=reply)

    return ConversationHandler.END


callback_handler_show_user_search = CallbackQueryHandler(
    show_user_search,
    pattern=f"^{CallbackEntry.show_user_search.value}")


def back_to_enumerate_user_search(update, context):
    data = json.loads(update.callback_query.data.split("-")[-1])
    search_query = SearchQuery.get(SearchQuery.item_id == data["search_id"])
    markup = SearchUsersInline(search_query=search_query, page=int(data["page"]))
    try:
        update.callback_query.edit_message_text(
            text=f"{strings.txt_search_result} {search_query.query_text}")
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


callback_handler_search_user_back = CallbackQueryHandler(
    back_to_enumerate_user_search,
    pattern=f"^{CallbackEntry.back_show_user_search.value}")
