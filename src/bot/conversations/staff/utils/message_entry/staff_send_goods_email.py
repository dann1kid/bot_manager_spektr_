import os
import shutil
import smtplib
from email.message import EmailMessage
from enum import Enum

import xlsxwriter
from telegram.ext import ConversationHandler, MessageHandler, Filters

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusDetailMenu import StatusDetailMenu
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.config_bot import excel_file, EMAIL_AUTH, EMAIL_PASS
from src.bot.entry_points.MessageEntry import EntryPoints
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Detail, Order, DetailOrder
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message_enum, cancel_conversation, \
    send_message
from src.bot.utils.timestamp_parses import readable_data_from_datetime

ENTRY_POINT = Filters.regex(f"^{EntryPoints.staff_send_goods_email.value}$")
excel_chart = f'{excel_file}chart_pattern_1.xlsx'


class ConversationStates(Enum):
    WAIT_CONFIRM = {"txt": strings.prt_are_you_sure,
                    "buttons": [strings.txt_yes,
                                strings.txt_back,
                                strings.txt_cancel,
                                ]}


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    """
        Входная функция выбор

    :param update:
    :param context:
    :return:
    """
    send_message_enum(update, context, ConversationStates.WAIT_CONFIRM)

    return ConversationStates.WAIT_CONFIRM.name


@cancel_conversation
def parse_confirm(update, context):
    chat_id = update.message.chat_id
    message_text = update.message.text

    match message_text:
        case strings.txt_yes:
            # формируем три таблицы по форме
            make_excel()
            send_email()
            # отправляем на почту
            send_message(update, context, message=strings.txt_finished,
                         reply=hub_keyboard(chat_id))
            state = ConversationHandler.END

        case strings.txt_no:

            state = ConversationHandler.END

        case _:
            send_message_enum(update, context,
                              enum_state=ConversationStates.WAIT_CONFIRM)

            state = ConversationStates.WAIT_CONFIRM.name

    return state


def make_excel():
    pattern = f'{excel_file}chart_pattern.xlsx'

    shutil.copy(pattern, excel_chart)

    headings_goods = ['номер', "добавлен", "инфа", "цена", "предоплата", "имя",
                      "номер клиента"]
    width_cols_goods = [8, 34, 47, 5, 13, 16, 15]
    headings_goods_parts = ["номер", "добавлен", "инфо", "цена", "предоплата",
                            "имя",
                            "номер клиента"]
    width_cols_goods_parts = [8, 34, 47, 5, 13, 16, 15]
    headings_detail_order = ["номер заказа", "добавлен", "инфо", "цена",
                             "имя", "номер клиента"]
    width_cols_detail_orders = [8, 34, 47, 5, 16, 15]

    # Только товары
    goods = Detail.select().where(Detail.status == StatusDetailMenu.wttrdr.name). \
        where(Detail.is_detail == False).execute()

    # выборка из товаров в виде запчастей
    detail_parts = Detail.select().where(Detail.status == StatusDetailMenu.wttrdr.name). \
        where(Detail.is_detail == True).execute()

    # детали от ремонта1
    repair_details_parts = DetailOrder.select().join(Order).where(
        Order.status == StatusOrderMenu.iw.name).execute()

    workbook = xlsxwriter.Workbook(excel_chart)
    bold = workbook.add_format({'bold': 1})

    # товары

    # todo пересмотреть подход с либой openpyxl
    worksheet = workbook.add_worksheet(name="Товары")

    worksheet.write_row('A1', headings_goods, bold)
    for item, value in enumerate(width_cols_goods):
        worksheet.set_column(item, item, value)

    for index, item in enumerate(goods, start=2):
        worksheet.write_row(
            f'A{index}',
            [item.item_id, readable_data_from_datetime(item.added),
             f"Название: {item.detail_name}. \nКоммент: { item.comments[0].comment if item.comments else '' }", item.price,
             item.prepayment,
             item.client.name, item.client.numbs[0].number])
    #############################################################
    # запчасти
    worksheet = workbook.add_worksheet(name="Детали")
    worksheet.write_row('A1', headings_goods_parts, bold)
    for item, value in enumerate(width_cols_goods_parts):
        worksheet.set_column(item, item, value)

    for index, item in enumerate(detail_parts, start=2):
        worksheet.write_row(
            f'A{index}',
            [item.item_id, readable_data_from_datetime(item.added),
             f"Наименование: {item.detail_name}. \nКоммент: {item.comments[0].comment if item.comments else ''}", f"Цена: {item.price}",
             item.prepayment,
             item.client.name, item.client.numbs[0].number])

    ##############################################################
    # детали к ремонту
    worksheet = workbook.add_worksheet(name="запчасти к заказу")
    worksheet.write_row('A1', headings_detail_order, bold)
    for item, value in enumerate(width_cols_detail_orders):
        worksheet.set_column(item, item, value)

    for index, detail_order in enumerate(repair_details_parts, start=2):
        worksheet.write_row(
            f'A{index}',
            [
                detail_order.order_id.item_id,
                readable_data_from_datetime(detail_order.added),
                f"{detail_order.detail_name} {detail_order.text}",
                detail_order.price,
                detail_order.order_id.client.name,
                detail_order.order_id.client.numbs[-1].number,
            ])
    workbook.close()


def send_email():
    try:
        email_address = EMAIL_AUTH
        email_password = EMAIL_PASS

        if email_address is None or email_password is None:
            # no email address or password
            # something is not configured properly
            print("Did you set email address and password correctly?")
            return False

        # create email
        msg = EmailMessage()
        msg['Subject'] = "Список заказов"
        msg['From'] = email_address
        msg['To'] = "magazinspektr@mail.ru"
        msg.set_content("Список заказов деталей и товаров, 3 листа")

        # add attachments
        with open(excel_chart, 'rb') as content_file:
            content = content_file.read()
            msg.add_attachment(content, maintype='application', subtype='xls',
                               filename="details.xlsx")

        # send email
        with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
            smtp.login(email_address, email_password)
            smtp.send_message(msg)
            os.remove(excel_chart)
        return True
    except Exception as e:
        print("Problem during send email")
        print(str(e))
    return False


staff_send_goods_email_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_on_entry)],

    states={
        ConversationStates.WAIT_CONFIRM.name: [
            MessageHandler(Filters.text, parse_confirm)
        ],

    },
    fallbacks=fallback_handlers, allow_reentry=True
)
