import json
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    Filters, MessageHandler

from src.bot.Enumerators.Memcache import MemorySubset, Memcache
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Detail, Order, Client
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached, get_memcached_item
from src.bot.utils.message import only_digits, send_message, send_message_inline


class ConversationStates(Enum):
    TAKE_PAYMENTS = {"txt": strings.txt_sum_payment,
                     "buttons": [strings.txt_cancel, ]}


memory_subset = MemorySubset.detail_get_payment


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    # get data
    chat_id = update.callback_query.from_user.id
    data = json.loads(update.callback_query.data.split("-")[-1])

    # write paired order data to cache
    # in this case it
    subset_key = Memcache(
        chat_id=chat_id,
        subset=memory_subset).get_subset()

    write_memcached(subset_key=subset_key, value=data["detail"])
    item = Detail.get(Detail.item_id == data['detail'])

    reply = make_reply_keyboard(['0', strings.txt_cancel, item.price])

    send_message_inline(update, context,
                        message=ConversationStates.TAKE_PAYMENTS.value["txt"],
                        reply=reply)

    return ConversationStates.TAKE_PAYMENTS.name


@only_digits(next_state=ConversationStates.TAKE_PAYMENTS)
def take_payment(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id

    item = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)
    # save data
    client = Client.get(Client.chat_id == chat_id)
    detail = Detail.get(Detail.item_id == item)
    detail.client = client
    detail.payment = text
    detail.save()
    send_message(update, context, message=strings.prompt_payment_received,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


get_payments_receiver_detail = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.extradite_detail.value)
    ],

    states={
        ConversationStates.TAKE_PAYMENTS.name: [
            MessageHandler(Filters.text, take_payment)],
    },
    fallbacks=fallback_handlers, allow_reentry=True,
)
