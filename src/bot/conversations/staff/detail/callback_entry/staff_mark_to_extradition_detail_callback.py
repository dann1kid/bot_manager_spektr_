import json

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusDetailMenu import StatusDetailMenu
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Detail
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached, get_memcached_item
from src.bot.utils.message import send_message_inline, cancel_conversation, \
    send_message

WAIT_CONFIRM = 'wait_confirm'


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    update.callback_query.answer()
    send_message_inline(update, context, message=strings.prt_are_you_sure,
                        reply=make_reply_keyboard([strings.txt_yes,
                                                   strings.txt_no,
                                                   strings.txt_cancel]))

    chat_id = update.callback_query.message.chat_id
    data = json.loads(update.callback_query.data.split("-")[-1])
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.mark_detail_as_ordered). \
        get_subset()
    write_memcached(subset_key=subset_key, value=data["detail"])

    return WAIT_CONFIRM


@cancel_conversation
def parse_confirm(update, context):
    chat_id = update.message.chat_id
    message_text = update.message.text

    if message_text == strings.txt_no:
        send_message(update, context, message=strings.txt_cancelled,
                     reply=hub_keyboard(chat_id=chat_id))
        return ConversationHandler.END
    elif message_text == strings.txt_yes:
        item = get_memcached_item(chat_id, MemorySubset.mark_detail_as_ordered)
        Detail.update(
            {Detail.status: StatusDetailMenu.sldlvr.name}). \
            where(Detail.item_id == item).execute()
        send_message(update, context,
                     message=strings.note_detail_await_select_delivery,
                     reply=hub_keyboard(chat_id=chat_id))
        return ConversationHandler.END
    else:
        send_message(update, context, message=strings.txt_try_again)
        return WAIT_CONFIRM


handler_mark_detail_to_extradition = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.send_to_extradition_detail.value)],
    states={
        WAIT_CONFIRM: [MessageHandler(Filters.text, parse_confirm)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
