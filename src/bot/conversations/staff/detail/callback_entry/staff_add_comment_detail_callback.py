import json
from pprint import pprint

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Client, CommentDetail
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached, get_memcached_item
from src.bot.utils.message import send_message_inline, cancel_conversation, \
    send_message

WAIT_COMMENT = "wait_comment"

memory_subset = MemorySubset.new_comment_detail


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    chat_id = update.callback_query.message.chat_id
    send_message_inline(update, context, message=strings.txt_send_comment,
                        reply=make_reply_keyboard(
                            [strings.txt_cancel,
                             strings.txt_notice_important_mark]))
    data = json.loads(update.callback_query.data.split("-")[-1])
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_comment_detail).get_subset()
    write_memcached(subset_key=subset_key, value=data["detail"])
    update.callback_query.answer()

    return WAIT_COMMENT


@cancel_conversation
def add_comment(update, context):
    text = update.message.text
    chat_id = update.message.chat_id

    item = get_memcached_item(chat_id, memory_subset)
    print("detail id add comment", item)
    client = Client.get(Client.chat_id == chat_id)
    comment_detail = CommentDetail.create(detail=item, client=client,
                                          comment=text)
    pprint(comment_detail)
    send_message(update, context, message=strings.txt_comment_added,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


callback_handler_add_comment_detail = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.add_comment_detail.value)],
    states={
        WAIT_COMMENT: [MessageHandler(Filters.text, add_comment)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
