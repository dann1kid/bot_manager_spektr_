import json
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusDetailMenu import StatusDetailMenu
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Detail
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached, get_memcached_item
from src.bot.utils.message import send_message_enum, cancel_conversation, \
    send_message, get_chat_id, send_message_inline_enum

"""
Стейты удаления заказа товара (Detail)
    
    предупреждаем ->  если ок то вешаем статус завершено 
                    

"""
memory_subset = MemorySubset.delete_detail


class ConversationStates(Enum):
    WAIT_CONFIRM = {"txt": strings.prt_are_you_sure,
                    "buttons": [
                        strings.txt_cancel,
                        strings.txt_back,
                        strings.txt_yes,
                    ]}


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    chat_id = get_chat_id(update)
    update.callback_query.answer()
    send_message_inline_enum(update, context,
                             enum_state=ConversationStates.WAIT_CONFIRM)

    data = json.loads(update.callback_query.data.split("-")[-1])
    subset_key = Memcache(
        chat_id=chat_id,
        subset=memory_subset).get_subset()
    write_memcached(subset_key=subset_key, value=data["detail"])

    return ConversationStates.WAIT_CONFIRM.name


@cancel_conversation
def parse_confirm(update, context):
    chat_id = update.message.chat_id
    message_text = update.message.text
    item_id = get_memcached_item(chat_id, memory_subset)

    match message_text:
        case strings.txt_yes:
            Detail.update(status=StatusDetailMenu.finished.name).where(
                Detail.item_id == item_id).execute()

            send_message(update, context,
                         message=strings.txt_detail_was_deleted,
                         reply=hub_keyboard(chat_id))
            state = ConversationHandler.END

        case _:
            send_message_enum(update, context,
                              enum_state=ConversationStates.WAIT_CONFIRM)

            state = ConversationStates.WAIT_CONFIRM.name

    return state


handler_delete_detail_callback = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.delete_detail_detail.value)],
    states={
        ConversationStates.WAIT_CONFIRM.name: [
            MessageHandler(Filters.text, parse_confirm)
        ],
    },
    fallbacks=fallback_handlers, allow_reentry=True

)
