#!/usr/bin/env python
# -*- coding: utf-8 -*-
# status - abandoned
from enum import Enum

from telegram import KeyboardButton
from telegram.ext import Filters, ConversationHandler, MessageHandler, \
    CallbackQueryHandler, CommandHandler

from src.bot.Enumerators.Deeplink import DeeplinkEnum
from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Menus.SelectUserMenu import SelectUserMenu
from src.bot.entry_points.CallbackEntry import CallbackEntry, CallbackParser
from src.bot.entry_points.MessageEntry import EntryPoints
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.memcaсhed_models import TempDetail, \
    create_temp_detail
from src.bot.models.pw_models import ContactClient, Client, Detail, \
    CommentDetail, GeoPositionDetail, AddressDetail
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.cancel import cancel
from src.bot.utils.contact_processing import InputNumber
from src.bot.utils.memcashed import get_memcached, write_memcached, \
    get_memcached_item, write_memcached_item
from src.bot.utils.message import send_message, send_message_inline, \
    cancel_conversation, back_conversation, \
    send_file, only_digits, skip_step, send_message_enum, send_message_to_target
from src.bot.utils.qr_processing import qr_generate_link_file

entry_phrase = EntryPoints.staff_add_detail.value
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")


class ConversationStates(Enum):
    NUMBER = {"txt": strings.str_prompt_number_receiver,
              "buttons": [strings.txt_cancel]}
    PROFILE = {"txt": 'profile',
               "buttons": [strings.txt_cancel, strings.txt_back,
                           strings.txt_yes]}
    NAME = {"txt": strings.txt_prompt_name,
            "buttons": [strings.txt_cancel, strings.txt_back]}
    SURNAME = {"txt": strings.txt_prompt_surname,
               "buttons": [strings.txt_cancel, strings.txt_back]}
    DETAIL = {"txt": strings.txt_detail_name,
              "buttons": [
                  strings.txt_cancel,
                  strings.txt_back,
              ]}
    IS_DETAIL = {"txt": strings.txt_is_detail_part,
                 "buttons": [
                     strings.txt_yes,
                     strings.txt_no,
                     strings.txt_cancel,
                     strings.txt_back,
                 ]}
    COMMENT = {"txt": strings.prt_additional_info,
               "buttons": [
                   strings.txt_cancel,
                   strings.txt_back,
                   strings.txt_btn_skip,

               ]}
    PRICE = {"txt": strings.txt_prompt_price,
             "buttons": [strings.txt_cancel,
                         strings.txt_back,
                         strings.txt_btn_skip,
                         ]}
    PREPAYMENT = {"txt": strings.txt_prepayment,
                  "buttons": [
                      strings.txt_cancel,
                      strings.txt_back,
                      strings.txt_btn_skip,
                  ]}
    SELECT_DELIVERY = {"txt": strings.txt_prompt_delivery,
                       "buttons": [strings.txt_cancel, strings.txt_back,
                                   strings.txt_no,
                                   KeyboardButton(text=strings.btn_send_geo,
                                                  request_location=True),
                                   ]
                       }


memory_subset = MemorySubset.new_detail_staff


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    """
        Входная функция выбор

    :param update:
    :param context:
    :return:
    """
    send_message_enum(update, context, enum_state=ConversationStates.NUMBER)
    return ConversationStates.NUMBER.name


@cancel_conversation
@only_digits(next_state=ConversationStates.NUMBER)
def checking_number(update, context) -> str:
    """Проверяет номер в стейте и сохраняет его, используя модель"""
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id

    if len(users_text) <= 9:
        send_message(update, context, message=strings.txt_prompt_number_error)

        return ConversationStates.NUMBER.name

    number = InputNumber(users_text=users_text)
    number.make_number()
    if number.num_possible:
        contact = ContactClient.get_or_none(
            ContactClient.number == number.num_formatted)
        if contact:
            keyboard = SelectUserMenu(
                formatted_number=number.num_formatted).get_menu()
            send_message(update, context,
                         message=strings.prompt_select_which_profile,
                         reply=make_reply_keyboard(
                             ConversationStates.NUMBER.value["buttons"]))
            create_temp_detail(update, number, model=TempDetail())
            update.message.reply_text(text=strings.txt_prompt_found_profiles,
                                      reply_markup=keyboard)
            return ConversationStates.PROFILE.name

        elif not contact:
            create_temp_detail(update, number, model=TempDetail())
            send_message(update, context, message=strings.txt_prompt_name,
                         reply=make_reply_keyboard(
                             [strings.txt_cancel, strings.txt_back]))
            return ConversationStates.NAME.name

    else:
        # same reply
        send_message(update, context, ConversationStates.NUMBER.value["txt"],
                     reply=make_reply_keyboard(
                         ConversationStates.NUMBER.value["buttons"]))

        return ConversationStates.NUMBER.name


# todo добавить враппер на длину сообщения номера


@cancel_conversation
@back_conversation(next_state=ConversationStates.NUMBER)
def parse_name(update, context):
    """for text field parse with name"""
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id

    # save data
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    temp_detail = get_memcached(subset_key=subset_key)
    temp_detail.name = users_text
    temp_detail.new_client = True
    write_memcached(subset_key=subset_key, value=temp_detail)
    send_message(update, context,
                 message=strings.txt_prompt_surname,
                 reply=make_reply_keyboard(
                     [strings.txt_cancel, strings.txt_back]))

    return ConversationStates.SURNAME.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.NAME)
def parse_surname(update, context):
    """for text field parse with surname"""
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id

    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()

    temp_detail = get_memcached(subset_key=subset_key)
    temp_detail.profile = Client.create(name=temp_detail.name,
                                        surname=temp_detail.surname,
                                        )
    ContactClient.create(number=temp_detail.number,
                         client=temp_detail.profile)

    write_memcached(subset_key=subset_key, value=temp_detail)
    send_message(update, context,
                 message=strings.txt_detail_name,
                 reply=make_reply_keyboard(
                     [strings.txt_cancel, strings.txt_back]))

    return ConversationStates.DETAIL.name


# todo убрать эти функции в утилиты для конверсейшенов
def parse_profile(update, context):
    """case if callback button used to select profile"""
    chat_id = update.callback_query.from_user.id
    data = CallbackParser()
    data = data.parse_callback(callback_data=update.callback_query.data)
    client = Client.get(Client.item_id == data['client_id'])

    # notification
    text = f"{strings.txt_selected_profile} {client.name} {client.surname}"
    update.callback_query.answer(text=text, show_alert=True)
    update.callback_query.edit_message_text(text=text)

    # save data
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    temp_detail = get_memcached(subset_key=subset_key)
    temp_detail.profile = client
    write_memcached(subset_key=subset_key, value=temp_detail)

    # message
    send_message_inline(update, context,
                        message=ConversationStates.DETAIL.value["txt"],
                        reply=make_reply_keyboard(
                            ConversationStates.DETAIL.value["buttons"])
                        )

    return ConversationStates.DETAIL.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.NUMBER)
def parse_detail(update, context):
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id

    # message for next_state
    send_message(update, context,
                 message=ConversationStates.COMMENT.value['txt'],
                 reply=make_reply_keyboard(
                     ConversationStates.COMMENT.value['buttons']))

    # save data
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    temp_detail = get_memcached(subset_key=subset_key)
    temp_detail.detail = users_text
    write_memcached(subset_key=subset_key, value=temp_detail)

    return ConversationStates.COMMENT.name


@cancel_conversation
@skip_step(next_state=ConversationStates.IS_DETAIL)
@back_conversation(next_state=ConversationStates.DETAIL)
def parse_detail_comment(update, context):
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id

    # save data
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    temp_detail = get_memcached(subset_key=subset_key)
    temp_detail.comment = users_text
    write_memcached(subset_key=subset_key, value=temp_detail)
    # message for next state
    send_message_enum(update, context, ConversationStates.IS_DETAIL)

    return ConversationStates.IS_DETAIL.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.COMMENT)
def parse_is_detail(update, context):
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id

    # save data
    temp_item = get_memcached_item(chat_id=chat_id,
                                   memory_subset=memory_subset)
    if users_text == strings.txt_yes:
        print("1")
        temp_item.is_detail_part = True

        print("2")
        write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                             value=temp_item)

    print("3")
    send_message_enum(update, context, ConversationStates.PRICE)
    print("4")

    return ConversationStates.PRICE.name


@cancel_conversation
@skip_step(next_state=ConversationStates.PREPAYMENT)
@back_conversation(next_state=ConversationStates.IS_DETAIL)
@only_digits(next_state=ConversationStates.PRICE)
def parse_detail_price(update, context):
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id

    # save data
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    temp_detail = get_memcached(subset_key=subset_key)
    temp_detail.price = users_text
    write_memcached(subset_key=subset_key, value=temp_detail)
    # message for next state
    send_message(update, context,
                 message=ConversationStates.PREPAYMENT.value["txt"],
                 reply=make_reply_keyboard(
                     ConversationStates.PREPAYMENT.value["buttons"]))

    return ConversationStates.PREPAYMENT.name


@cancel_conversation
@skip_step(next_state=ConversationStates.SELECT_DELIVERY)
@back_conversation(next_state=ConversationStates.PRICE)
@only_digits(next_state=ConversationStates.PRICE)
def parse_detail_prepayment(update, context):
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id

    # save data
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    temp_detail = get_memcached(subset_key=subset_key)
    temp_detail.prepayment = users_text
    write_memcached(subset_key=subset_key, value=temp_detail)
    # message for next state
    send_message(update, context,
                 message=ConversationStates.SELECT_DELIVERY.value["txt"],
                 reply=make_reply_keyboard(
                     ConversationStates.SELECT_DELIVERY.value["buttons"]))

    return ConversationStates.SELECT_DELIVERY.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.PREPAYMENT)
def parse_delivery_address(update, context):
    # todo разнести на два вызова (локейшн и текст)
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    temp_detail = get_memcached(subset_key=subset_key)

    if users_text == strings.txt_no:
        # skip all branches and goto question
        pass

    else:
        temp_detail.address = users_text
        temp_detail.address_check = True

    write_memcached(subset_key=subset_key, value=temp_detail)

    detail_id, owner_id = save_data(chat_id)

    receiver_workspace = Client.get(Client.chat_id == chat_id)

    # send registration
    qr_file = qr_generate_link_file(
        enum_type=DeeplinkEnum.register_user_detail,
        type_id=detail_id,
        client_id=owner_id)
    send_file(update, context, caption=f"{strings.txt_caption_registration}",
              file=qr_file)

    # message for next state
    send_message(update, context, message=strings.txt_finished,
                 reply=hub_keyboard(chat_id))
    send_message_to_target(context, chat_id="223781831",
                           text=f"Новый заказ на товар #{detail_id}"
                                f" создан работником {receiver_workspace.name} ")

    return ConversationHandler.END


@cancel_conversation
@back_conversation(next_state=ConversationStates.SELECT_DELIVERY)
def parse_delivery_location(update, context):
    # todo разнести на два вызова (локейшн и текст)
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    temp_detail = get_memcached(subset_key=subset_key)

    if users_text == strings.txt_no:
        # skip all branches and goto question
        pass

    else:
        temp_detail.geo_pos = update.message.location
        temp_detail.geo_check = True

    write_memcached(subset_key=subset_key, value=temp_detail)
    detail_id, owner_id = save_data(chat_id)
    # send registration
    qr_file = qr_generate_link_file(enum_type=DeeplinkEnum.register_user_detail,
                                    type_id=detail_id,
                                    client_id=owner_id)
    send_file(update, context, caption=f"{strings.txt_caption_registration}",
              file=qr_file)

    # message for next state
    send_message(update, context, message=strings.txt_finished,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


def save_data(creator_chat_id):
    """save data from temp table to target tables"""
    subset_key = Memcache(chat_id=creator_chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    temp_detail = get_memcached(
        subset_key=subset_key)
    """
    if temp_detail.new_client:
        client = Client.create(name=temp_detail.name,
                               surname=temp_detail.surname,
                               )
        number = ContactClient.create(number=temp_detail.number, client=client)
        detail_owner_client_id = client.item_id
    else:
        detail_owner_client_id = temp_detail.profile.item_id
    """

    creator = Client.get(Client.chat_id == creator_chat_id)
    detail = Detail.create(detail_name=temp_detail.detail,
                           client=temp_detail.profile,
                           text=temp_detail.comment,
                           status=temp_detail.status,
                           price=temp_detail.price,
                           prepayment=temp_detail.prepayment,
                           creator=creator
                           )

    # перенос комментария
    if temp_detail.comment != "":
        CommentDetail.create(detail=detail.item_id,
                             client=temp_detail.profile,
                             comment=temp_detail.comment)

    if temp_detail.geo_check:
        GeoPositionDetail.create(detail_id=detail.item_id,
                                 geo_pos=temp_detail.geo_pos)
    else:
        AddressDetail.create(detail_id=detail.item_id,
                             geo_pos=temp_detail.address)

    return detail.item_id, temp_detail.profile


@cancel_conversation
@back_conversation(next_state=ConversationStates.NUMBER)
def parse_profile_text(update, context):
    # collect data
    users_text = update.message.text

    if users_text == strings.txt_new_client:
        send_message(update, context,
                     message=strings.str_prompt_number_receiver,
                     reply=make_reply_keyboard(
                         [strings.txt_cancel, strings.txt_back]))
        return ConversationStates.NAME.name

    # if any_text
    else:
        send_message(update, context, message=strings.prompt_correct_data,
                     reply=make_reply_keyboard(
                         [strings.txt_cancel, strings.txt_back, ]))
        return ConversationStates.NUMBER.name


staff_add_detail_CH = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_on_entry)],

    states={
        ConversationStates.NUMBER.name: [
            MessageHandler(Filters.text, checking_number)],

        ConversationStates.PROFILE.name: [
            CallbackQueryHandler(parse_profile,
                                 pattern=f"^{CallbackEntry.select_users_profile.value}"),
            MessageHandler(Filters.text, parse_profile_text)],

        ConversationStates.NAME.name: [
            MessageHandler(Filters.text, parse_name)],
        ConversationStates.SURNAME.name: [
            MessageHandler(Filters.text, parse_surname)],
        ConversationStates.DETAIL.name: [
            MessageHandler(Filters.text, parse_detail)],
        ConversationStates.COMMENT.name: [
            MessageHandler(Filters.text, parse_detail_comment)],
        ConversationStates.IS_DETAIL.name: [
            MessageHandler(Filters.text, parse_is_detail)],
        ConversationStates.PRICE.name: [
            MessageHandler(Filters.text, parse_detail_price)],
        ConversationStates.PREPAYMENT.name: [
            MessageHandler(Filters.text, parse_detail_prepayment)],
        ConversationStates.SELECT_DELIVERY.name: [
            MessageHandler(Filters.text, parse_delivery_address),
            MessageHandler(Filters.location, parse_delivery_location)
        ]

    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=True
)
