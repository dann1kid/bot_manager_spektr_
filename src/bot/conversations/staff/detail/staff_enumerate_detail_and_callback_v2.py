import json
from enum import Enum

import telegram
from telegram import ParseMode
from telegram.ext import ConversationHandler, MessageHandler, Filters, \
    CallbackQueryHandler

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusDetailMenu import StatusDetailMenu
from src.bot.MessageBuilder.DetailMessageBuilder import DetailMessageBuilder
from src.bot.MessageBuilder.SearchDetailInline import SearchDetailsInline
from src.bot.MessageBuilder.StatusDetailsInline import StatusDetailsInline
from src.bot.entry_points.CallbackEntry import CallbackEntry, CallbackParser
from src.bot.entry_points.MessageEntry import EntryPoints
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Detail, SearchQuery
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message, cancel_conversation

"""
    Отвечает за конверсейшн выдачи нужной пагинации для показа заказов
    Здесь указаны кнопки и колбеки для всех кнопок, показываемых в листинге 
    заказов
"""

"""

"""


class ConvStates(Enum):
    SELECT = {"txt": strings.prompt_select_statuses_orders,
              "buttons": [
                  StatusDetailMenu.on_eval.value,
                  StatusDetailMenu.wttrdr.value,
                  StatusDetailMenu.ordrd.value,
                  StatusDetailMenu.od.value,
                  StatusDetailMenu.sldlvr.value,
                  StatusDetailMenu.odwc.value,
                  StatusDetailMenu.osldlvr.value,
                  strings.txt_cancel,
              ]}
    SELECTED = {"txt": "selection", }


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    """
        Входная функция выбор

    :param update:
    :param context:
    :return:
    """
    orders_by_status = {status.value: len(Detail.select().
                                          where(Detail.status == f"{status.name}").execute())
                        for status in StatusDetailMenu}

    buttons = [f"{status}: {count}" for status, count in orders_by_status.items() if count > 0]
    buttons.append(strings.txt_cancel)
    send_message(update=update, context=context,
                 message=strings.txt_prompt_select_menu_enumerate,
                 reply=make_reply_keyboard(buttons))

    return ConvStates.SELECTED.name


@cancel_conversation
def parse_selection(update, context):
    # collect data
    chat_id = update.message.chat_id
    text = update.message.text

    for status in StatusDetailMenu:
        if text.startswith(status.value):
            text = status.value

    try:
        status = StatusDetailMenu(text).name
    except (AttributeError, ValueError):
        send_message(update, context,
                     message=strings.txt_no_orders_with_selected_status)
        return ConvStates.SELECTED.name
    else:
        markup = StatusDetailsInline(status=status)
        print("detail markup", markup.paginator.markup)
        if markup.items.count() > 0:
            send_message(update, context, message=strings.txt_available_details,
                         reply=markup.paginator.markup)
            send_message(update, context,
                         message=strings.main_menu,
                         reply=hub_keyboard(chat_id))
        else:
            send_message(update, context,
                         message=strings.txt_no_orders_with_selected_status,
                         reply=hub_keyboard(chat_id))

        return ConversationHandler.END


staff_enum_details_v2 = ConversationHandler(
    entry_points=[
        MessageHandler(
            Filters.regex(f"^{EntryPoints.staff_enumerate_detail.value}$"),
            fun_on_entry)],

    states={
        ConvStates.SELECTED.name: [
            MessageHandler(Filters.text, parse_selection)]
    },
    fallbacks=fallback_handlers, allow_reentry=True
)


def show_detail(update, context):
    """Функция с показыванием объекта деталей и добавлением кнопок
    в зависимости от текущего контекста.
    Например, для деталей которые еще не заказаны, есть галка "Заказано" и
    кнопка "удалить".
    """
    chat_id = update.callback_query.message.chat.id,
    json_data = json.loads(update.callback_query.data.split("-")[-1])
    item = Detail.get(Detail.item_id == json_data["detail"])

    raw_data = update.callback_query.data.split("-")[-1]

    builder = DetailMessageBuilder(detail=item, users_chat_id=chat_id, mode=DetailMessageBuilder.BuilderMode.status)
    message = builder.create_message()
    reply = builder.create_buttons_inline(raw_data)

    # Меняем текст
    update.callback_query.edit_message_text(
        text=message,
        parse_mode=ParseMode.HTML,
    )
    # меняем кнопки
    update.callback_query.edit_message_reply_markup(reply_markup=reply)

    return ConversationHandler.END


def show_detail_search(update, context):
    """Функция с показыванием объекта деталей и добавлением кнопок
    в зависимости от текущего контекста.
    Например, для деталей которые еще не заказаны, есть галка "Заказано" и
    кнопка "удалить".
    """
    chat_id = update.callback_query.message.chat.id,
    json_data = json.loads(update.callback_query.data.split("-")[-1])
    item = Detail.get(Detail.item_id == json_data["detail"])
    raw_data = update.callback_query.data.split("-")[-1]

    builder = DetailMessageBuilder(detail=item, users_chat_id=chat_id, mode=DetailMessageBuilder.BuilderMode.search)
    message = builder.create_message()
    reply = builder.create_buttons_inline(raw_data)

    # Меняем текст
    update.callback_query.edit_message_text(
        text=message,
        parse_mode=ParseMode.HTML,
    )
    # меняем кнопки
    update.callback_query.edit_message_reply_markup(reply_markup=reply)

    return ConversationHandler.END


def back_to_enumerate_search(update, context):
    data = json.loads(update.callback_query.data.split("-")[-1])
    search_query = SearchQuery.get(SearchQuery.item_id == data["search_id"])
    markup = SearchDetailsInline(search_query=search_query, page=int(data["page"]))
    try:
        update.callback_query.edit_message_text(
            text=f"{strings.txt_search_result} {search_query.query_text}")
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


def switch_page_search(update, context):
    data = CallbackParser.parse_callback(update.callback_query.data)
    markup = SearchDetailsInline(search_query=SearchQuery.get(SearchQuery.item_id == data["query"]),
                                 page=data["page"])
    try:
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


def switch_page_details(update, context):
    data = CallbackParser.parse_callback(update.callback_query.data)
    markup = StatusDetailsInline(status=StatusDetailMenu[data["status"]].name,
                                 page=data["page"])
    try:
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


def back_to_enumerate_detail(update, context):
    data = json.loads(update.callback_query.data.split("-")[-1])
    print("data in back to enumerate detail", data)

    match DetailMessageBuilder.BuilderMode(data["m"]).value:
        case 1:
            markup = StatusDetailsInline(status=StatusDetailMenu[data["status"]].name,
                                         page=int(data["page"]))
        case 2:
            search_query = SearchQuery.get(SearchQuery.item_id == data["search_id"])
            markup = SearchDetailsInline(search_query=search_query,
                                         page=int(data["page"]))
        case _:
            markup = StatusDetailsInline(status=StatusDetailMenu[data["status"]].name,
                                         page=int(data["page"]))

    try:
        update.callback_query.edit_message_text(
            text=strings.txt_available_details)
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


callback_handler_switch_page_details = CallbackQueryHandler(
    switch_page_details,
    pattern=f"^{CallbackEntry.paginator_page_detail.value}")

callback_handler_show_detail = CallbackQueryHandler(
    show_detail,
    pattern=f"^{CallbackEntry.show_detail.value}")

callback_handler_show_detail_back = CallbackQueryHandler(
    back_to_enumerate_detail,
    pattern=f"^{CallbackEntry.back_show_detail.value}")

callback_handler_show_detail_search = CallbackQueryHandler(
    show_detail_search,
    pattern=f"^{CallbackEntry.show_detail_search.value}")

callback_handler_search_detail_back = CallbackQueryHandler(
    back_to_enumerate_search,
    pattern=f"^{CallbackEntry.back_show_detail_search.value}")

callback_handler_switch_page_detail_search = CallbackQueryHandler(
    switch_page_search,
    pattern=f"^{CallbackEntry.paginator_page_detail_search.value}"
)
