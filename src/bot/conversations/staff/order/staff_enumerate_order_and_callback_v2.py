import json
from enum import Enum

import telegram
from telegram import ParseMode
from telegram.ext import ConversationHandler, MessageHandler, Filters, \
    CallbackQueryHandler

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.MessageBuilder.SearchOrderInline import SearchOrdersInline
from src.bot.MessageBuilder.StatusOrdersInline import StatusOrdersInline
from src.bot.MessageBuilder.OrderMessageBuilder import OrderMessageBuilder
from src.bot.entry_points.CallbackEntry import CallbackEntry, CallbackParser
from src.bot.entry_points.MessageEntry import EntryPoints
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Order, SearchQuery, Client
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message, cancel_conversation, \
    hide_keyboard, show_keyboard

"""
    Отвечает за конверсейшн выдачи нужной пагинации для показа заказов
    Здесь указаны кнопки и колбеки для всех кнопок, показываемых в листинге 
    заказов
"""


# todo разнести колбек и диалог в разные файлы

class ConvStates(Enum):
    SELECT = {"txt": strings.prompt_select_statuses_orders,
              "buttons": [StatusOrderMenu.oe.value,  # на оценке у приемщика
                          StatusOrderMenu.wcc.value,  # клиент жде любого курьера
                          StatusOrderMenu.wdc.value,  # ожидает решения клиента, отправлять?
                          StatusOrderMenu.wc.value,  # ожидает прибытия курьером
                          StatusOrderMenu.iw.value,  # в работе
                          StatusOrderMenu.odw.value,  # на доставке в мастерскую
                          StatusOrderMenu.dc.value,  # на доставке клиенту с мастерской
                          StatusOrderMenu.dwc.value,  # на выдаче, ожидает курьера
                          StatusOrderMenu.wsd.value,  # ожидает решение приемщика по доставке
                          StatusOrderMenu.sd.value,  # ожидает самовывоза
                          StatusOrderMenu.fin.value,  # завершенные
                          strings.txt_cancel, strings.btn_hide_keyboard]}
    SELECTED = {"txt": "selection"}


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    """
        Входная функция выбор

    :param update:
    :param context:
    :return:
    """
    orders_by_status = {status.value: len(Order.select().
                                          where(Order.status == f"{status.name}").execute())
                        for status in StatusOrderMenu}

    buttons = [f"{status}: {count}" for status, count in orders_by_status.items() if count > 0]
    buttons.append(strings.txt_cancel)
    send_message(update=update, context=context,
                 message=strings.txt_prompt_select_menu_enumerate,
                 reply=make_reply_keyboard(buttons))

    return ConvStates.SELECTED.name


@hide_keyboard
@show_keyboard(next_state=ConvStates.SELECT)
@cancel_conversation
def parse_selection(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id

    for status in StatusOrderMenu:
        if text.startswith(status.value):
            text = status.value

    try:
        status = StatusOrderMenu(text).name

    except (AttributeError, ValueError):
        send_message(update, context,
                     message=strings.txt_no_orders_with_selected_status,
                     reply=hub_keyboard(chat_id))
        return ConversationHandler.END

    markup = StatusOrdersInline(status=status, chat_id=chat_id)

    if markup.items.count() > 0:
        send_message(update, context, message=strings.txt_available_orders_status.format(StatusOrderMenu(text).value),
                     reply=markup.paginator.markup)
        send_message(update, context,
                     message=strings.main_menu,
                     reply=hub_keyboard(chat_id))

    else:
        send_message(update, context,
                     message=strings.txt_no_orders_with_selected_status,
                     reply=hub_keyboard(chat_id))

    return ConversationHandler.END


staff_enum_orders_v2 = ConversationHandler(
    entry_points=[
        MessageHandler(
            Filters.regex(f"^{EntryPoints.staff_enumerate_orders.value}$"),
            fun_on_entry)],

    states={
        ConvStates.SELECTED.name: [
            MessageHandler(Filters.text, parse_selection)]
    },
    fallbacks=fallback_handlers,
    allow_reentry=True,
    name="staff_enumerate_order_and_callback_v2",
)


def switch_page(update, context):
    data = CallbackParser.parse_callback(update.callback_query.data)
    chat_id = update.callback_query.from_user.id
    markup = StatusOrdersInline(status=StatusOrderMenu[data["status"]].name,
                                page=data["page"], chat_id=chat_id)
    try:
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


def show_order(update, context):
    chat_id = update.callback_query.message.chat.id
    json_data = json.loads(update.callback_query.data.split("-")[-1])
    order = Order.get(Order.item_id == json_data["order"])
    raw_data = update.callback_query.data.split("-")[-1]

    builder = OrderMessageBuilder(order=order,
                                  users_chat_id=chat_id,
                                  mode=OrderMessageBuilder.BuilderMode.status,
                                  update=update,
                                  context=context)
    message = builder.create_message()
    reply = builder.create_buttons_inline(raw_data)

    # Меняем текст
    update.callback_query.edit_message_text(
        text=message,
        parse_mode=ParseMode.HTML,
    )
    # меняем кнопки
    try:
        update.callback_query.edit_message_reply_markup(reply_markup=reply)
    except Exception as e:
        raise (e)

    return ConversationHandler.END


def back_to_enumerate(update, context):
    data = json.loads(update.callback_query.data.split("-")[-1])
    chat_id = update.callback_query.message.chat.id
    print("data in back to enumerate order", data)
    match OrderMessageBuilder.BuilderMode(data["mode"]).value:
        case 1:
            markup = StatusOrdersInline(status=StatusOrderMenu[data["status"]].name,
                                        page=int(data["page"]), chat_id=chat_id)
        case 2:
            search_query = SearchQuery.get(SearchQuery.item_id == data["search_id"])
            markup = SearchOrdersInline(search_query=search_query,
                                        page=int(data["page"]))
        case _:
            markup = StatusOrdersInline(status=StatusOrderMenu[data["status"]].name,
                                        page=int(data["page"]), chat_id=chat_id)

    try:
        update.callback_query.edit_message_text(
            text=strings.txt_available_orders)
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


def back_to_enumerate_search(update, context):
    data = json.loads(update.callback_query.data.split("-")[-1])
    search_query = SearchQuery.get(SearchQuery.item_id == data["search_id"])
    markup = SearchOrdersInline(search_query=search_query,
                                page=int(data["page"]))
    try:
        update.callback_query.edit_message_text(
            text=f"{strings.txt_search_result} {search_query.query_text}")
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


def show_order_search(update, context):
    # TODO: Объединить с show_order разграничив условием из метаданных
    chat_id = update.callback_query.message.chat.id
    json_data = json.loads(update.callback_query.data.split("-")[-1])
    order = Order.get(Order.item_id == json_data["order"])
    raw_data = update.callback_query.data.split("-")[-1]

    builder = OrderMessageBuilder(order=order, users_chat_id=chat_id, mode=OrderMessageBuilder.BuilderMode.search)
    message = builder.create_message()
    reply = builder.create_buttons_inline(raw_data)
    print("reply in show order SEARCH ", reply)

    # Меняем текст
    update.callback_query.edit_message_text(
        text=message,
        parse_mode=ParseMode.HTML,
    )
    # меняем кнопки
    try:
        update.callback_query.edit_message_reply_markup(reply_markup=reply)
    except Exception as e:
        raise (e)

    return ConversationHandler.END


def switch_page_search(update, context):
    data = CallbackParser.parse_callback(update.callback_query.data)
    print("swtch page search", data)
    markup = SearchOrdersInline(search_query=SearchQuery.get(SearchQuery.item_id == data["query"]),
                                page=data["page"])
    try:
        update.callback_query.edit_message_reply_markup(markup.paginator.markup)
    except telegram.error.BadRequest:
        update.callback_query.answer()

    return ConversationHandler.END


callback_handler_switch_page_order = CallbackQueryHandler(
    switch_page,
    pattern=f"^{CallbackEntry.paginator_page_order.value}"
)

callback_handler_show_order = CallbackQueryHandler(
    show_order,
    pattern=f"^{CallbackEntry.show_order.value}")

callback_handler_show_order_back = CallbackQueryHandler(
    back_to_enumerate,
    pattern=f"^{CallbackEntry.back_show_order.value}")

callback_handler_search_order_back = CallbackQueryHandler(
    back_to_enumerate_search,
    pattern=f"^{CallbackEntry.back_show_order_search.value}")

callback_handler_show_order_search = CallbackQueryHandler(
    show_order_search,
    pattern=f"^{CallbackEntry.show_order_search.value}")

callback_handler_switch_page_order_search = CallbackQueryHandler(
    switch_page_search,
    pattern=f"^{CallbackEntry.paginator_page_order_search.value}"
)
