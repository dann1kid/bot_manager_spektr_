# диалог для выдачи клиенту аппарата
# вызывается через callback


# states
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Order
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.load_json_callback import get_json_callback
from src.bot.utils.memcashed import write_memcached, get_memcached_item
from src.bot.utils.message import send_message_inline, only_digits, send_message, cancel_conversation, \
    send_message_to_target


class ConversationStates(Enum):
    TAKE_PAYMENTS = {"txt": strings.txt_sum_payment,
                     "buttons": [strings.txt_cancel, ]}


memory_subset = MemorySubset.order_get_payment


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    # release the button
    update.callback_query.answer()
    # get data
    json_data = get_json_callback(update)
    chat_id = update.callback_query.from_user.id
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.order_get_payment). \
        get_subset()
    item = Order.get(Order.item_id == json_data['order'])

    write_memcached(subset_key=subset_key, value=item.item_id)
    reply = make_reply_keyboard(['0', item.predefined_price, item.price, strings.txt_cancel])

    send_message_inline(update, context,
                        message=strings.txt_sum_payment,
                        reply=reply)

    return ConversationStates.TAKE_PAYMENTS.name


@cancel_conversation
@only_digits(next_state=ConversationStates.TAKE_PAYMENTS)
def save_payment(update, context):
    chat_id = update.message.chat_id
    text = update.message.text
    order = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)

    order = Order.get(Order.item_id == order)
    order.payment = text
    order.status = StatusOrderMenu.fin.name
    order.save()

    send_message(update, context, strings.txt_finished_order_num.format(order_id=order))
    send_message(update, context, message=strings.main_menu,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


handler_extradite_and_take_payment_order = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.extradite_order.value)
    ],

    states={
        ConversationStates.TAKE_PAYMENTS.name: [
            MessageHandler(Filters.text, save_payment)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
