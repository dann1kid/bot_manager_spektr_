import json
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    Filters, MessageHandler

from src.bot.Enumerators.Memcache import MemorySubset, Memcache
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Order, Client
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached, get_memcached_item
from src.bot.utils.message import send_message_inline, send_message, only_digits, cancel_conversation


# TODO: удалить, дубликат staff_extradite_order_callback
class ConversationStates(Enum):
    TAKE_PAYMENTS = {"txt": strings.txt_sum_payment,
                     "buttons": [strings.txt_cancel, ]}


memory_subset = MemorySubset.order_get_payment


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    # get data
    chat_id = update.callback_query.from_user.id
    data = json.loads(update.callback_query.data.split("-")[-1])

    # release the button
    update.callback_query.answer()
    print("im on entry on extraditions")
    # write paired order data to cache
    # in this case it
    subset_key = Memcache(
        chat_id=chat_id,
        subset=memory_subset).get_subset()

    write_memcached(subset_key=subset_key, value=data["order"])
    item = Order.get(Order.item_id == data['order'])

    reply = make_reply_keyboard(['0', strings.txt_cancel, item.price])
    # standard prices + noted price

    send_message_inline(update, context,
                        message=ConversationStates.TAKE_PAYMENTS.value["txt"],
                        reply=reply)

    return ConversationStates.TAKE_PAYMENTS.name


@cancel_conversation
@only_digits(next_state=ConversationStates.TAKE_PAYMENTS)
def take_payment(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id

    # save data
    item = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)
    # save data
    client = Client.get(Client.chat_id == chat_id)
    order = Order.get(Order.item_id == item)
    order.client = client
    order.payment = text
    order.save()

    send_message(update, context, message=strings.prompt_payment_received,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


get_payments_self_delivery_order = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.staff_payment_order.value)
    ],

    states={
        ConversationStates.TAKE_PAYMENTS.name: [
            MessageHandler(Filters.text, take_payment)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
