from telegram.ext import ConversationHandler, CallbackQueryHandler

from src.bot.Enumerators.Deeplink import DeeplinkEnum
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.models.pw_models import Order, Client
from src.bot.resources import strings
from src.bot.rights.checking import access_level
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.load_json_callback import get_json_callback
from src.bot.utils.message import send_file_to_target, send_message_inline
from src.bot.utils.qr_processing import qr_generate_link_file


def fun_on_entry(update, context):
    chat_id = update.callback_query.from_user.id

    if not access_level(chat_id, required_level=StaffLevel.courier):
        update.callback_query.answer(text=strings.err_access_denied,
                                     show_alert=True)
        return ConversationHandler.END

    order_button_data = get_json_callback(update)
    order = Order.get(Order.item_id == order_button_data['order'])

    # case order not in valid state (wait decision client)
    if order.status != StatusOrderMenu.wc.name:
        send_message_inline(update, context, message=strings.err_order_in_another_status)

        return ConversationHandler.END

    client = Client.get(Client.chat_id == chat_id)

    # create qr
    qr_file = qr_generate_link_file(enum_type=DeeplinkEnum.transmit_order_from_client_courier,
                                    type_id=order.item_id,
                                    client_id=client.item_id)
    # send qr
    send_file_to_target(context, chat_id=order.client.chat_id,
                        caption=strings.txt_qr_caption_for_client_to_courier.format(order.item_id),
                        file=qr_file)

    update.callback_query.answer()
    # then add function to transmit order once
    # then update record with qr mark expired true

    return ConversationHandler.END


send_qr_to_transmit_order_from_client_to_courier = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.courier_get_order_from_client.value)],
    states={
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
