import json
from enum import Enum

import telegram
from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.MessageBuilder.StatusOrdersInline import StatusOrdersInline
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Order
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached, get_memcached_item
from src.bot.utils.message import send_message_enum, \
    cancel_conversation, send_message, get_chat_id, send_message_inline_enum


class ConversationStates(Enum):
    WAIT_CONFIRM = {"txt": strings.prt_are_you_sure,
                    "buttons": [strings.txt_cancel, strings.txt_back,
                                strings.txt_yes]}

    # todo переделать на сущность или статический класс, чтобы
    # избежать множественных определений в условиях диалога

    # TODO Заменить в диалогах send_message на send_message_enum


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    chat_id = get_chat_id(update)
    update.callback_query.answer()
    message_id = update.callback_query.message.message_id
    send_message_inline_enum(update, context,
                             enum_state=ConversationStates.WAIT_CONFIRM)

    data = json.loads(update.callback_query.data.split("-")[-1])
    # create set of items to parse in next function
    items = (data["order"], data, message_id)
    subset_key = Memcache(
        chat_id=chat_id,
        subset=MemorySubset.order_mark_to_extradition).get_subset()
    write_memcached(subset_key=subset_key, value=items)

    return ConversationStates.WAIT_CONFIRM.name


@cancel_conversation
def parse_confirm(update, context):
    chat_id = update.message.chat_id
    message_text = update.message.text

    match message_text:
        case strings.txt_yes:
            print("отправляю на ожидание решения по выдаче")
            items = get_memcached_item(chat_id,
                                       MemorySubset.order_mark_to_extradition)
            Order.update(
                status=StatusOrderMenu.wsd.name).where(
                Order.item_id == items[0]).execute()
            send_message(update, context, message=strings.txt_finished,
                         reply=hub_keyboard(chat_id))

            # возвращаем менюшку назад так как заказ уже исчез из этого статуса

            data = items[1]
            message_id = items[2]
            markup = StatusOrdersInline(status=StatusOrderMenu[data["status"]].name,
                                        page=int(data["page"]), chat_id=chat_id)
            try:
                context.bot.edit_message_text(chat_id=chat_id,
                                              message_id=message_id,
                                              text=strings.txt_available_orders,
                                              reply_markup=markup.paginator.markup)
            except telegram.error.BadRequest:
                context.bot.answer()

            state = ConversationHandler.END

        case strings.txt_no:
            send_message(update, context, message=strings.txt_cancelled,
                         reply=hub_keyboard(chat_id))
            state = ConversationHandler.END

        case _:
            send_message_enum(update, context,
                              enum_state=ConversationStates.WAIT_CONFIRM)

            state = ConversationStates.WAIT_CONFIRM.name

    return state


handler_mark_order_to_delivery = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.send_to_extradition_order.value)],
    states={
        ConversationStates.WAIT_CONFIRM.name: [
            MessageHandler(Filters.text, parse_confirm)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
