import json
import subprocess
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.config_bot import MAIN_PATH
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Order
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached_item, get_memcached_item
from src.bot.utils.message import cancel_conversation, \
    send_message_inline_enum, only_digits, send_message
from src.bot.utils.print import create_label

memory_subset = MemorySubset.print_labels


class ConversationStates(Enum):
    LABELS = {"txt": strings.txt_labels_print_question,
              "buttons": [
                  strings.txt_cancel,
              ]}


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    chat_id = update.callback_query.from_user.id
    # mark button pressed
    update.callback_query.answer()

    # parse data
    data = json.loads(update.callback_query.data.split("-")[-1])
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset, value=data["order"])
    send_message_inline_enum(update, context, enum_state=ConversationStates.LABELS)

    return ConversationStates.LABELS.name


@cancel_conversation
@only_digits(next_state=ConversationStates.LABELS)
def print_labels(update, context):
    # collect data
    text = int(update.message.text)
    chat_id = update.message.chat_id
    if text > 0:
        order_id = get_memcached_item(chat_id, memory_subset)
        order = Order.get(Order.item_id == order_id)
        label = create_label(order)
        subprocess.run(
            ["py",
             f'{MAIN_PATH}\\send_label.py',
             f'{label}',
             f'{text}'
             ])

    send_message(update, context, message=strings.txt_labels_printed, reply=hub_keyboard(chat_id))

    return ConversationHandler.END


handler_print_labels = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.staff_print_labels.value)],
    states={
        ConversationStates.LABELS.name: [
            MessageHandler(Filters.text, print_labels)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
