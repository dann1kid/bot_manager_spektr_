from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.bot.Enumerators.Memcache import MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import make_reply_keyboard
from src.bot.models.pw_models import Order
from src.bot.resources import strings
from src.bot.rights.checking import access_level
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.load_json_callback import get_json_callback
from src.bot.utils.memcashed import write_memcached_item, get_memcached_item
from src.bot.utils.message import send_message_inline, send_message_to_target, notify_staff, \
    cancel_conversation

take_payment = 1
memory_subset = MemorySubset.courier_take_payment


def fun_on_entry(update, context):
    chat_id = update.callback_query.from_user.id

    update.callback_query.answer()

    if not access_level(chat_id, required_level=StaffLevel.courier):
        update.callback_query.answer(text=strings.err_access_denied,
                                     show_alert=True)
        return ConversationHandler.END

    order_button_data = get_json_callback(update)
    order_id = order_button_data['order']
    order = Order.get(Order.item_id == order_id)

    # case order not in valid state (wait decision client)
    if order.status != StatusOrderMenu.wp.name:
        send_message_inline(update, context, message=strings.err_order_in_another_status)

        return ConversationHandler.END

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=order_id)

    send_message_inline(update, context,
                        message=f"{strings.txt_order_transmitted_to_client}.\n"
                                f"{strings.note_courier_payment.format(order.price)}",
                        reply=make_reply_keyboard([f"{order.price}", strings.txt_cancel]))

    return take_payment


@cancel_conversation
def parse_payment(update, context):
    chat_id = update.message.chat_id
    order_id = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)
    text = update.message.text
    order = Order.get(Order.item_id == order_id)
    order.payment = text
    order.status = StatusOrderMenu.fin.name
    order.save()

    send_message_to_target(context,
                           chat_id=order.client.chat_id,
                           text=strings.note_client_order_finished.format(order.item_id))

    notify_staff(context, item=order, staff_level=StaffLevel.receiver, message=strings.order_has_been_paid)

    return ConversationHandler.END


handler_take_payment_courier = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.courier_take_payment.value)],
    states={
        take_payment: [MessageHandler(Filters.text, parse_payment)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
