from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

# states
from src.bot.Enumerators.Memcache import MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.MessageBuilder.OrderMessageBuilder import OrderMessageBuilder
from src.bot.entry_points.CallbackEntry import CallbackEntry, CallbackParser
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Order
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.load_json_callback import get_json_callback
from src.bot.utils.memcashed import get_memcached_item, write_memcached_item
from src.bot.utils.message import cancel_conversation, \
    send_message, send_message_to_target, send_message_inline_enum, only_digits, send_message_enum, back_conversation


#
# Приемщик - оценка заказа. Вызывается контекстным inline-меню через callback
#


class ConversationStates(Enum):
    GET_PREPRICE = {"txt": strings.txt_get_preprice,
                    "buttons": [
                        strings.txt_cancel,
                    ]}
    GET_ORIENTED_TIME = {"txt": strings.txt_send_oriented_time,
                         "buttons": [
                             strings.txt_cancel,
                             strings.txt_back,
                         ]}


memory_subset = MemorySubset.evaluate_order


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    json_data = get_json_callback(update)
    chat_id = update.callback_query.from_user.id
    update.callback_query.answer()

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset, value=json_data)

    send_message_inline_enum(update, context, enum_state=ConversationStates.GET_PREPRICE)

    return ConversationStates.GET_PREPRICE.name


@only_digits(next_state=ConversationStates.GET_PREPRICE)
@cancel_conversation
def save_preprice(update, context):
    chat_id = update.message.chat_id
    data = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)
    text = update.message.text
    get_memcached_item(chat_id, memory_subset=memory_subset)

    data["preprice"] = text
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset, value=data)

    send_message_enum(update, context, enum_state=ConversationStates.GET_ORIENTED_TIME)

    return ConversationStates.GET_ORIENTED_TIME.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.GET_PREPRICE)
def save_oriented_data(update, context):
    chat_id = update.message.chat_id
    text = update.message.text
    data = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)
    data["oriented_data"] = text

    order = Order.get(Order.item_id == data["order"])
    save_data(order=order, data=data)
    do_notifications(update, context, order)

    return ConversationHandler.END


def save_data(order: Order = None, data: dict = None):
    order.predefined_price = data["preprice"]
    order.oriented_timestamp = data["oriented_data"]
    order.status = StatusOrderMenu.wdc.name
    order.save()


def do_notifications(update, context, order):
    chat_id = update.message.chat_id
    send_message(update, context,
                 message=strings.txt_change_price_order_confirmed,
                 reply=hub_keyboard(chat_id))

    # отправка сообщения клиенту
    builder = OrderMessageBuilder(order=order,
                                  users_chat_id=order.client.chat_id,
                                  mode=OrderMessageBuilder.BuilderMode.status)

    raw_data = CallbackParser.make_paginator_order(order_id=order.item_id,
                                                   status=order.status,
                                                   page=1,
                                                   mode=OrderMessageBuilder.BuilderMode.status.value)

    send_message_to_target(context, chat_id=order.client.chat_id,
                           text=f"{strings.notif_evaluated_order.format(order.item_id, order.predefined_price)} "
                                f"{strings.wait_decision_client}")

    # send message with button
    send_message_to_target(context, chat_id=order.client.chat_id,
                           text=builder.create_message(), reply=builder.create_buttons_inline(raw_data))


receiver_eval_order_conv = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.evaluate_order.value)
    ],

    states={
        ConversationStates.GET_PREPRICE.name: [MessageHandler(Filters.text, save_preprice)],
        ConversationStates.GET_ORIENTED_TIME.name: [MessageHandler(Filters.text, save_oriented_data)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
