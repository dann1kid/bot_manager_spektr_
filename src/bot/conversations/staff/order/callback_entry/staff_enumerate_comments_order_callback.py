import json

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    CommandHandler

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.models.pw_models import CommentOrder
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.cancel import cancel
from src.bot.utils.message import mono_inline_keyboard, concat_comments


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    # данные для поиска в бд
    data = json.loads(update.callback_query.data.split("-")[-1])
    button_data = update.callback_query.data.split("-")[-1]
    print("in comments back button data", button_data)

    #
    comments = CommentOrder.select().where(
        CommentOrder.order == int(data["order"])).execute()
    # переопределяем дату для дальнейшей передачи

    message = concat_comments(comments)

    if message == "":
        message = strings.txt_no_comments

    # update message and inline keyboard
    update.callback_query.message.edit_text(text=message)
    update.callback_query.edit_message_reply_markup(
        reply_markup=mono_inline_keyboard(text=strings.txt_back,
                                          callback_data=CallbackEntry.show_order.value + button_data))

    return ConversationHandler.END


handler_enumerate_comments_order = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.show_comments_order.value)],
    states={
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=True
)
