import json
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Client, Order, \
    ContactClient
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message_inline, cancel_conversation, \
    send_message


class ConvStates(Enum):
    wait_confirm = 1
    wait_price = 2


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    # mark button pressed
    update.callback_query.answer()

    # parse data
    data = json.loads(update.callback_query.data.split("-")[-1])

    # confirm enter by sending message
    order = Order.get(Order.item_id == data["order"])
    numbs = order.client.numbs.order_by(ContactClient.added).limit(5)
    str_numbs = ""
    for num in numbs:
        str_numbs += f"{num.number}, "

    send_message_inline(update, context,
                        message=f"{strings.prompt_sure_change_price}.\n "
                                f"Последние номера для указанного клиента {str_numbs[:-2]}",
                        # drop ", "
                        reply=make_reply_keyboard(
                            [strings.txt_yes, strings.txt_no]))

    # write selected order to params
    # todo поменяоть на мемкаш
    Client.update(params=data["order"]).execute()

    return ConvStates.wait_confirm.name


@cancel_conversation
def check_confirm(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id
    if text == strings.txt_no:
        send_message(update, context,
                     message=strings.txt_change_price_order_cancelled,
                     reply=hub_keyboard(chat_id))

        return ConversationHandler.END

    elif text == strings.txt_yes:
        send_message(update, context, message=strings.txt_new_price_order,
                     reply=make_reply_keyboard([strings.txt_cancel]))

    return ConvStates.wait_price.name


@cancel_conversation
def save_price(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id
    # except Non integer
    try:
        text = int(text)
    except ValueError:
        send_message(update, context,
                     message=strings.txt_new_price_order,
                     reply=make_reply_keyboard([strings.txt_cancel]))
        return ConvStates.wait_price.name

    # update db
    client = Client.get(Client.chat_id == chat_id)
    Order.update(predefined_price=text).where(
        Order.item_id == client.params).execute()

    # send next question
    send_message(update, context,
                 message=strings.txt_change_price_order_confirmed,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


handler_add_update_price_order = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.change_price.value)],
    states={
        ConvStates.wait_confirm.name: [
            MessageHandler(Filters.text, check_confirm)],
        ConvStates.wait_price.name: [MessageHandler(Filters.text, save_price)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
