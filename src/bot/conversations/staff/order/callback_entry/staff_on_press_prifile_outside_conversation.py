from telegram.ext import ConversationHandler, CallbackQueryHandler

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message_inline


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    # данные для поиска в бд
    update.callback_query.answer()
    send_message_inline(update, context, message=f"{strings.err_access_denied}. \n"
                                                 f"{strings.err_outside_of_conversation}")

    return ConversationHandler.END


handler_on_press_select_profile_outside_conversation = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.select_users_profile.value)],
    states={
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
