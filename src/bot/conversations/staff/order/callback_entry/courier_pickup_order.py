import json
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    Filters, MessageHandler

from src.bot.Enumerators.Memcache import MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Order, Client
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import get_memcached_item, write_memcached_item
from src.bot.utils.message import send_message_inline, send_message, cancel_conversation, \
    send_message_inline_enum


# TODO: удалить, дубликат staff_extradite_order_callback
class ConversationStates(Enum):
    CONFIRM = {"txt": strings.prt_are_you_sure,
               "buttons": [strings.txt_cancel, strings.txt_yes, strings.txt_no ]}


memory_subset = MemorySubset.courier_pickup_order
answer_yes = Filters.regex("^Да$")
answer_no = Filters.regex("^Нет$")


@access_level_wrapper(staff_status=StaffLevel.courier)
def fun_on_entry(update, context):
    # get data
    chat_id = update.callback_query.from_user.id
    data = json.loads(update.callback_query.data.split("-")[-1])
    order = Order.get(Order.item_id == data["order"])
    update.callback_query.answer()

    # checking valid states
    if order.status != StatusOrderMenu.wcc.name:
        send_message_inline(update, context, message=strings.err_access_denied)
        return ConversationHandler.END

    elif order.courier is not None:
        send_message_inline(update, context, message=strings.err_already_wait_courier)
        return ConversationHandler.END

    # release the button
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=data["order"])

    send_message_inline_enum(update, context, enum_state=ConversationStates.CONFIRM)

    return ConversationStates.CONFIRM.name


@cancel_conversation
def take_order(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id

    if text != strings.txt_yes:
        send_message(update, context, message=strings.txt_cancelled,
                     reply=hub_keyboard(chat_id))
        return ConversationHandler.END

    item = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)
    order = Order.get(Order.item_id == item)

    # checking valid states
    if order.status != StatusOrderMenu.wcc.name:
        send_message_inline(update, context, message=strings.err_access_denied)

    elif order.courier is not None:
        send_message_inline(update, context, message=strings.err_already_wait_courier)
        return ConversationHandler.END

    client = Client.get(Client.chat_id == chat_id)
    order.status = StatusOrderMenu.wc.name
    order.courier = client
    order.save()

    send_message(update, context, message=strings.txt_order_picked,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


courier_handler_pickup_order = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.courier_pickup_order_from_client.value)
    ],

    states={
        ConversationStates.CONFIRM.name: [
            MessageHandler(Filters.text, take_order)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
