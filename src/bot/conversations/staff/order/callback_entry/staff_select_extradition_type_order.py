import json
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Order, AddressOrder
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached, get_memcached_item
from src.bot.utils.message import send_message_enum, cancel_conversation, \
    send_message, send_message_inline_enum, back_conversation, notify_staff, send_message_to_target

"""
стейты выдачи:

отравка на выдачу - > чекаем обратный адрес -> 
    
    предупреждаем -> обратный адрес есть или нет ->
    если есть адрес -> Добавляем кнопки -> отправить по имеющемся адресу
                                        -> отправить на самовывоз
                                        -> указать новый адрес

    если адреса нет -> добавляем кнопки -> указать новый адрес
                                        -> отправить на самовывоз  
                                        

этот диалог привязан к кнопке по колбеку. У этого колбека всегда есть дата, 
указывающая на обьект, с которым идет воздействие диалога
"""
memory_subset = MemorySubset.order_send_to_extradition


class ConversationStates(Enum):
    WAIT_CONFIRM = {"txt": strings.prt_are_you_sure,
                    "buttons": [
                        strings.txt_cancel,
                        strings.txt_back,
                        strings.txt_yes,
                    ]}

    SEND_HAS_ADDRESS = {
        "txt": f"{strings.note_back_address_select_delivery} {strings.txt_ask_delivery_type}",
        "buttons": [
            strings.btn_send_delivery_self_delivery,
            strings.btn_send_delivery_by_address,
            strings.btn_send_delivery_by_add_address,
            strings.txt_cancel,
            strings.txt_back,
        ]}

    SEND_HAS_NOT_ADDRESS = {
        "txt": f"{strings.note_no_back_address_select_delivery} {strings.txt_ask_delivery_type}",
        "buttons": [
            strings.btn_send_delivery_self_delivery,
            strings.btn_send_delivery_by_add_address,
            strings.txt_cancel,
            strings.txt_back,
        ]}

    SEND_HAS_NOT_CHAT_ID = {
        "txt": f"{strings.err_no_client_chat_id}",
        "buttons": [
            strings.btn_send_delivery_self_delivery,
            strings.txt_cancel,
            strings.txt_back,
        ]}

    PARSE_DELIVERY_SELECTION = {
        "txt": strings.note_back_address_select_delivery,
        "buttons": [
            strings.btn_send_delivery_self_delivery,
            strings.btn_send_delivery_by_add_address,
            strings.txt_cancel,
            strings.txt_back,
        ]}

    WAIT_ADD_ADDRESS = {
        "txt": strings.btn_pls_add_address,
        "buttons": [
            strings.txt_cancel,
            strings.txt_back,
        ]}


def parse_delivery_address(update, context, chat_id):
    item_id = get_memcached_item(chat_id,
                                 memory_subset)

    item = Order.get(Order.item_id == item_id)

    if item.address_check or item.geo_check or item.delivery_check:
        send_message_enum(update, context,
                          enum_state=ConversationStates.SEND_HAS_ADDRESS)
    elif item.client.chat_id == 0:
        send_message_enum(update, context,
                          enum_state=ConversationStates.SEND_HAS_NOT_CHAT_ID)
    else:
        send_message_enum(update, context,
                          enum_state=ConversationStates.SEND_HAS_NOT_ADDRESS)


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    chat_id = update.callback_query.message.chat_id
    update.callback_query.answer()
    send_message_inline_enum(update, context,
                             enum_state=ConversationStates.WAIT_CONFIRM)

    data = json.loads(update.callback_query.data.split("-")[-1])
    subset_key = Memcache(
        chat_id=chat_id,
        subset=memory_subset).get_subset()
    write_memcached(subset_key=subset_key, value=data["order"])

    return ConversationStates.WAIT_CONFIRM.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.WAIT_CONFIRM)
def parse_confirm(update, context):
    chat_id = update.message.chat_id
    message_text = update.message.text

    match message_text:
        case strings.txt_yes:
            parse_delivery_address(update, context, chat_id)

            state = ConversationStates.PARSE_DELIVERY_SELECTION.name

        case _:
            send_message_enum(update, context,
                              enum_state=ConversationStates.WAIT_CONFIRM)

            state = ConversationStates.WAIT_CONFIRM.name

    return state


@cancel_conversation
@back_conversation(next_state=ConversationStates.WAIT_CONFIRM)
def parse_delivery(update, context):
    chat_id = update.message.chat_id
    message_text = update.message.text
    item_id = get_memcached_item(chat_id, memory_subset)
    item = Order.get(Order.item_id == item_id)

    match message_text:
        case strings.btn_send_delivery_self_delivery:
            item.status = StatusOrderMenu.sd.name
            item.save()

            send_message(update, context, message=strings.txt_finished,
                         reply=hub_keyboard(chat_id))

            if item.client.chat_id:
                send_message_to_target(context, chat_id=item.client.chat_id,
                                       text=strings.note_your_order_on_extradition.format(item.item_id))

            state = ConversationHandler.END

        case strings.btn_send_delivery_by_address:
            item.status = StatusOrderMenu.dwc.name
            item.save()

            send_message(update, context, message=strings.txt_finished,
                         reply=hub_keyboard(chat_id))
            notify_staff(context=context, item=item,
                         staff_level=StaffLevel.receiver,
                         message=strings.txt_order_wait_courier)
            state = ConversationHandler.END

        case strings.btn_send_delivery_by_add_address:
            send_message_enum(update, context,
                              ConversationStates.WAIT_ADD_ADDRESS)

            state = ConversationStates.WAIT_ADD_ADDRESS.name

        case _:
            send_message_enum(update, context,
                              ConversationStates.PARSE_DELIVERY_SELECTION)
            state = ConversationStates.PARSE_DELIVERY_SELECTION.name

    return state


@cancel_conversation
def parse_add_address(update, context):
    chat_id = update.message.chat_id
    message_text = update.message.text
    item_id = get_memcached_item(chat_id, memory_subset)
    order = Order.get(Order.item_id == item_id)

    if not order.client.chat_id:
        send_message(update, context, message=f"{strings.txt_finished}. "
                                              f"{strings.txt_no_chat_id}",
                     reply=hub_keyboard(chat_id))
        order.delivery_check = False
        order.address_check = False
        order.status = StatusOrderMenu.sd.name
        notify_staff(context=context, item=order,
                     staff_level=StaffLevel.receiver,
                     message=strings.txt_on_extradition_f.format(order.item_id))

    else:
        # send message to client
        send_message_to_target(context,
                               chat_id=order.client.chat_id,
                               text=strings.txt_order_wait_courier)
        AddressOrder.create(order=item_id, address=message_text)

        order.delivery_check = True
        order.address_check = True
        order.status = StatusOrderMenu.dwc.name

        notify_staff(context=context, item=order,
                     staff_level=StaffLevel.courier,
                     message=strings.txt_order_wait_courier)

    order.save()

    return ConversationHandler.END


handler_select_delivery_order = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.select_delivery_type_order.value)],
    states={
        ConversationStates.WAIT_CONFIRM.name: [
            MessageHandler(Filters.text, parse_confirm)
        ],

        ConversationStates.PARSE_DELIVERY_SELECTION.name: [
            MessageHandler(Filters.text, parse_delivery)
        ],

        ConversationStates.WAIT_ADD_ADDRESS.name: [
            MessageHandler(Filters.text, parse_add_address)
        ]
    },
    fallbacks=fallback_handlers, allow_reentry=True

)
