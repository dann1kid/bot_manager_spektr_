import json
from enum import Enum

from telegram import ParseMode, InputMediaPhoto
from telegram.ext import ConversationHandler, CallbackQueryHandler

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.MessageBuilder.OrderMessageBuilder import OrderMessageBuilder
from src.bot.entry_points.CallbackEntry import CallbackEntry, CallbackParser
from src.bot.models.pw_models import Order
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_photo_inline, send_message_inline


# просто отправить все медиа из базы,
# если не отправляются то загрузить и отправить фото согласно указанному пути в базе


class ShowPicsDialogStates(Enum):
    SHOW_PICS = {"txt": strings.txt_showing_pics,
                 "buttons": [strings.txt_cancel]
                 }


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    # mark button pressed
    update.callback_query.answer()
    # collect data
    chat_id = update.callback_query.from_user.id
    message_id = update.callback_query.message.message_id

    # parse data
    data = json.loads(update.callback_query.data.split("-")[-1])
    # send info message
    send_message_inline(update, context, message=strings.txt_showing_pics)
    # confirm enter by sending message
    order = Order.get(Order.item_id == data["order"])
    """
    for photo in order.photos:
        context.bot.send_photo(chat_id=update.callback_query.from_user.id, photo=photo.pic_id, caption=f"{strings.txt_order} №{order.item_id}",
                               reply_markup=reply, parse_mode=ParseMode.HTML)
    """
    # parse photos
    media_list = [InputMediaPhoto(photo.pic_id) for photo in order.photos[0:10]]
    # photo_list = [photo.pic_id for photo in order.photos]
    # file = context.bot.get_file(photo_list[0])
    # edit message
    context.bot.send_media_group(chat_id=chat_id, media=media_list, reply_to_message_id=message_id)
    # media = InputMediaPhoto(media=photo_list[0], caption=text, parse_mode=ParseMode.HTML)

    return ConversationHandler.END


handler_show_all_pics_order = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry, pattern=CallbackEntry.show_pics_order.value)],
    states={
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
