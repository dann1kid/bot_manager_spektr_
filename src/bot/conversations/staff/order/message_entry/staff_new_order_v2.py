#!/usr/bin/env python
# -*- coding: utf-8 -*-
# todo: перебрать все диалоги и повесить на входе проверку по уровню
import subprocess
from enum import Enum

from telegram import KeyboardButton, ParseMode
from telegram.ext import CallbackQueryHandler
from telegram.ext import ConversationHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler

from src.bot import config_bot
from src.bot.Enumerators.Deeplink import DeeplinkEnum
from src.bot.Enumerators.Memcache import MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Menus.SelectUserMenu import SelectUserMenu
from src.bot.config_bot import MAIN_PATH
from src.bot.entry_points.CallbackEntry import CallbackEntry, CallbackParser
from src.bot.entry_points.MessageEntry import EntryPoints
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.keyboards.TG_keyboards import make_reply_keyboard
from src.bot.models.memcaсhed_models import TempOrder, create_temp_order
from src.bot.models.pw_models import ContactClient, CommentOrder, \
    GeoPositionOrder, AddressOrder, \
    Client, Order, Device
# conv_handler constants
from src.bot.resources import strings, lists
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.contact_processing import InputNumber
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import get_memcached_item, write_memcached_item
from src.bot.utils.message import send_message, send_file, send_message_inline, \
    cancel_conversation, \
    back_conversation, only_digits, send_message_enum, skip_step, \
    send_message_to_target
from src.bot.utils.print import save_document, create_label
from src.bot.utils.qr_processing import qr_generate_link_file

entry_phrase = EntryPoints.staff_add_order.value
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

# conversation_handler presets
answer_yes = Filters.regex("^Да$")
answer_no = Filters.regex("^Нет$")

memory_subset = MemorySubset.new_order_staff


class ConversationStates(Enum):
    NUMBER = {"txt": strings.str_prompt_number_receiver,
              "buttons": [
                  strings.txt_cancel,
                  strings.txt_back,
              ]}
    PROFILE = {"txt": 'profile',
               "buttons": [
                   strings.txt_cancel,
                   strings.txt_back,
                   strings.txt_yes,
               ]}
    NAME = {"txt": strings.txt_prompt_name,
            "buttons": [
                strings.txt_btn_skip,
                strings.txt_cancel,
                strings.txt_back,
            ]}
    SURNAME = {"txt": strings.txt_prompt_surname,
               "buttons": [
                   strings.txt_btn_skip,
                   strings.txt_cancel,
                   strings.txt_back,
               ]}
    BRAND = {"txt": strings.txt_prompt_brand,
             "buttons": lists.btn_default_brands}
    MODEL = {"txt": strings.txt_prompt_model,
             "buttons": [
                 strings.txt_btn_skip,
                 strings.txt_cancel,
                 strings.txt_back,
                 strings.txt_noname_model,

             ]}
    TROUBLE = {"txt": strings.prompt_trouble,
               "buttons":
                   lists.btn_default_trouble
               }
    IMEI = {"txt": strings.txt_prompt_imei,
            "buttons": [
                strings.txt_btn_skip,
                strings.txt_cancel,
                strings.txt_back,
            ]}
    COMMENT = {"txt": strings.txt_prompt_commentary,
               "buttons": [strings.txt_cancel, strings.txt_back,
                           strings.txt_btn_skip,
                           strings.txt_no_data, strings.txt_charge_device,
                           strings.txt_cover,
                           ]}
    PRICE = {"txt": strings.txt_prompt_price,
             "buttons": lists.btn_default_prices}
    PREPAYMENT = {"txt": strings.txt_prepayment,
                  "buttons": [
                      strings.txt_btn_skip,
                      strings.txt_cancel,
                      strings.txt_back,
                  ]}
    SELECT_DELIVERY = {"txt": strings.txt_prompt_delivery,
                       "buttons": [
                           strings.txt_cancel,
                           strings.txt_back,
                           KeyboardButton(text=strings.btn_send_geo,
                                          request_location=True),
                           strings.txt_no,
                       ]
                       }
    LABELS_PRINT = {"txt": strings.txt_labels_print_question,
                    "buttons": [
                        strings.txt_labels_count_1,
                        strings.txt_labels_count_2,
                        strings.txt_labels_count_3,
                        strings.txt_back,
                        strings.txt_cancel,
                        strings.txt_btn_skip,
                    ]
                    }
    PRINT = {"txt": strings.txt_print_question,
             "buttons": [
                 strings.txt_cancel,
                 strings.txt_back,
                 strings.txt_yes,
                 strings.txt_no,
             ]
             }


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    """
        Входная функция выбор

    :param update:
    :param context:
    :return:
    """
    send_message(update, context,
                 message=ConversationStates.NUMBER.value["txt"],
                 reply=make_reply_keyboard(
                     ConversationStates.NUMBER.value["buttons"]))

    return ConversationStates.NUMBER.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.NUMBER)
@only_digits(next_state=ConversationStates.NUMBER)
def checking_number(update, context):
    """Проверяет номер в стейте и сохраняет его, используя модель"""
    # collect data
    users_text = update.message.text

    number = InputNumber(users_text=users_text)
    number.make_number()
    temp_order = TempOrder()

    # TODO: момент с моделью и временным заказом неясен, переписать
    if number.num_parsed and number.num_possible:
        contact = ContactClient.get_or_none(
            ContactClient.number == number.num_formatted)

        if contact:
            keyboard = SelectUserMenu(
                formatted_number=number.num_formatted).get_menu()
            send_message(update, context,
                         message=strings.prompt_select_which_profile,
                         reply=make_reply_keyboard(
                             ConversationStates.NUMBER.value["buttons"]))

            create_temp_order(update, number, model=temp_order)
            update.message.reply_text(text=strings.txt_prompt_found_profiles,
                                      reply_markup=keyboard)
            return ConversationStates.PROFILE.name

        elif contact is None:
            create_temp_order(update, number, model=temp_order)
            send_message(update, context, message=strings.txt_prompt_name,
                         reply=make_reply_keyboard(
                             [strings.txt_cancel, strings.txt_back]))
            return ConversationStates.NAME.name

    else:
        send_message_enum(update, context, ConversationStates.NUMBER)

        return ConversationStates.NUMBER.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.NUMBER)
def parse_name(update, context):
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.name = users_text
    temp_order.profile = Client.create(name=temp_order.name,
                                       surname=temp_order.surname,
                                       )
    ContactClient.create(number=temp_order.number,
                         client=temp_order.profile.item_id)

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    send_message(update, context,
                 message=ConversationStates.SURNAME.value["txt"],
                 reply=make_reply_keyboard(
                     ConversationStates.SURNAME.value["buttons"]))

    return ConversationStates.SURNAME.name


@cancel_conversation
@skip_step(next_state=ConversationStates.BRAND)
@back_conversation(next_state=ConversationStates.NAME)
def parse_surname(update, context):
    # collect data
    users_text = update.message.text
    chat_id = update.message.chat_id
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.surname = users_text

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    send_message(update, context,
                 message=ConversationStates.BRAND.value["txt"],
                 reply=make_reply_keyboard(
                     ConversationStates.BRAND.value["buttons"]))

    return ConversationStates.BRAND.name


def parse_profile(update, context):
    # collect data
    chat_id = update.callback_query.from_user.id
    data = CallbackParser()
    data = data.parse_callback(callback_data=update.callback_query.data)
    client = Client.get(Client.item_id == data['client_id'])

    # notification
    text = f"{strings.txt_selected_profile} {client.name} {client.surname}"
    update.callback_query.answer(text=text, show_alert=True)
    update.callback_query.edit_message_text(text=text)

    # save data
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.profile = client
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    # message
    send_message_inline(update, context,
                        message=ConversationStates.BRAND.value["txt"],
                        reply=make_reply_keyboard(
                            ConversationStates.BRAND.value["buttons"]))

    return ConversationStates.BRAND.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.NUMBER)
def parse_brand(update, context) -> str:
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    # message for model
    send_message(update, context,
                 message=ConversationStates.MODEL.value["txt"],
                 reply=make_reply_keyboard(
                     ConversationStates.MODEL.value["buttons"]))

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.brand_name = users_text
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    return ConversationStates.MODEL.name


@cancel_conversation
@skip_step(next_state=ConversationStates.IMEI)
@back_conversation(next_state=ConversationStates.BRAND)
def parse_model(update, context):
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    # message
    send_message_enum(update, context, enum_state=ConversationStates.IMEI,
                      parse_mode=ParseMode.HTML)

    # save data

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.model_name = users_text
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    return ConversationStates.IMEI.name


@cancel_conversation
@skip_step(next_state=ConversationStates.TROUBLE)
@back_conversation(next_state=ConversationStates.MODEL)
def parse_imei(update, context):
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    # save data
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.imei = users_text
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    send_message_enum(update, context,
                      enum_state=ConversationStates.TROUBLE)

    return ConversationStates.TROUBLE.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.IMEI)
def parse_trouble(update, context):
    chat_id = update.message.chat_id
    users_text = update.message.text

    send_message_enum(update, context, ConversationStates.PRICE)
    # save data
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.trouble = users_text
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    return ConversationStates.PRICE.name


@cancel_conversation
@skip_step(next_state=ConversationStates.PREPAYMENT)
@back_conversation(next_state=ConversationStates.TROUBLE)
@only_digits(next_state=ConversationStates.PRICE)
def parse_price(update, context):
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    send_message(update, context,
                 message=ConversationStates.PREPAYMENT.value["txt"],
                 reply=make_reply_keyboard(
                     ConversationStates.PREPAYMENT.value["buttons"]))

    # save data
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.predefined_price = int(users_text)
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    return ConversationStates.PREPAYMENT.name


@cancel_conversation
@skip_step(next_state=ConversationStates.COMMENT)
@back_conversation(next_state=ConversationStates.PRICE)
@only_digits(next_state=ConversationStates.PREPAYMENT)
def parse_prepayment(update, context):
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    # message
    send_message(update, context,
                 message=ConversationStates.COMMENT.value["txt"],
                 reply=make_reply_keyboard(
                     ConversationStates.COMMENT.value["buttons"]))

    # save data
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.prepayment = int(users_text)
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    return ConversationStates.COMMENT.name


@cancel_conversation
@skip_step(next_state=ConversationStates.SELECT_DELIVERY)
@back_conversation(next_state=ConversationStates.PREPAYMENT)
def parse_commentary(update, context):
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    # message
    send_message(update, context,
                 message=ConversationStates.SELECT_DELIVERY.value["txt"],
                 reply=make_reply_keyboard(
                     ConversationStates.SELECT_DELIVERY.value['buttons']))

    # save data
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.comment = users_text
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    return ConversationStates.SELECT_DELIVERY.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.COMMENT)
def parse_delivery_address(update, context):
    # todo разнести на два вызова (локейшн и текст)
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)

    if users_text == strings.txt_no:
        # skip all branches and goto question
        pass

    else:
        temp_order.address = users_text
        temp_order.address_check = True

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)
    send_message_enum(update, context, ConversationStates.LABELS_PRINT)

    return ConversationStates.LABELS_PRINT.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.COMMENT)
def parse_delivery_location(update, context):
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)

    if users_text == strings.txt_no:
        # skip all branches and goto question
        pass

    else:
        temp_order.geo_pos = update.message.location
        temp_order.geo_check = True
        write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                             value=temp_order)

    send_message_enum(update, context, ConversationStates.LABELS_PRINT)

    return ConversationStates.LABELS_PRINT.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.SELECT_DELIVERY)
def parse_labels(update, context):
    chat_id = update.message.chat_id
    users_text = update.message.text

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    if users_text not in [
        strings.txt_labels_count_1,
        strings.txt_labels_count_2,
        strings.txt_labels_count_3,
    ]:
        pass

    else:
        temp_order.labels = int(users_text)
        write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                             value=temp_order)

    send_message_enum(update, context, ConversationStates.PRINT)

    return ConversationStates.PRINT.name


@cancel_conversation
@back_conversation(next_state=ConversationStates.LABELS_PRINT)
def parse_print(update, context):
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text
    receiver_workspace = Client.get(Client.chat_id == chat_id)
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)

    order, owner_id = save_data(chat_id)
    work_geo = receiver_workspace.workspace_geo
    if work_geo == "":
        work_geo = "bronnicy"

    if users_text == strings.txt_no:
        pass

    elif users_text == strings.txt_yes:
        # before continue save and parse data
        try:
            # todo перенести сохранение файла на асинхронный вызов через вызов ниже
            doc = save_document(order.item_id, geo=work_geo)
            if temp_order.labels > 0:
                exc = create_label(order)
            else:
                exc = None
            try:  # в этот раз мне вынесло мозг то, что субпроцесс не
                # видел интерпретатора, а он в винде через "py",
                # хотя он раньше работал!!!!
                # if platform.system().lower() == "windows":
                #    python = 'py'
                # else:
                #    python = 'python'
                subprocess.run(
                    ["py",
                     f'{MAIN_PATH}\\telethon_call.py',
                     f'{work_geo}',
                     f'{doc}',
                     f'{exc}',
                     f'{temp_order.labels}'
                     ])
            except Exception as e:
                send_message_to_target(context,
                                       chat_id=config_bot.TG_ADMIN_TEST,
                                       text="проблемы с печатью",
                                       )
                raise
        except Exception as e:
            send_message(update, context, message="Проблемы с печатью документа {e}")
            raise
    else:
        send_message_enum(update, context, ConversationStates.PRINT)

        return ConversationStates.PRINT.name

    # send registration
    if order.client.chat_id == 0:
        qr_file = qr_generate_link_file(enum_type=DeeplinkEnum.register_user_order,
                                        type_id=order.item_id,
                                        client_id=owner_id)
        send_file(update, context, caption=f"{strings.txt_caption_registration}",
                  file=qr_file)

    # return hub
    send_message(update, context, message=strings.txt_finished_order_num.format(
        order_id=order.item_id))
    send_message(update, context, message=strings.txt_finished,
                 reply=hub_keyboard(chat_id))

    # inform admin
    send_message_to_target(context, chat_id="223781831",
                           text=f"Новый заказ на ремонт #{order.item_id}"
                                f" создан работником {receiver_workspace.name} ")

    return ConversationHandler.END


# todo добавить враппер для типов данных
def save_data(creator_chat_id):
    temp_order = get_memcached_item(chat_id=creator_chat_id,
                                    memory_subset=memory_subset)
    """
    if temp_order.new_client:
        client = Client.create(name=temp_order.name,
                               surname=temp_order.surname,
                               )
        number = ContactClient.create(number=temp_order.number, client=client)

        order_owner_client_id = client.item_id
    else:
        order_owner_client_id = temp_order.profile
        """
    creator = Client.get(Client.chat_id == creator_chat_id)

    order = Order.create(
        client_id=temp_order.profile,
        trouble=temp_order.trouble,
        pic_path=temp_order.pic_path,
        pic_id=temp_order.pic_id,
        status=temp_order.status,
        predefined_price=temp_order.predefined_price,
        prepayment=temp_order.prepayment,
        creator=creator,
        address_check=temp_order.address_check,
        geo_check=temp_order.geo_check,
    )

    device = Device.create(
        order=order,
        brand_name=temp_order.brand_name,
        model_name=temp_order.model_name,
        imei=temp_order.imei,
        color=temp_order.color,
    )

    if temp_order.comment != "":
        # перенос комментария
        comment = CommentOrder.create(order_id=order.item_id,
                                      client=temp_order.profile,
                                      comment=temp_order.comment)

    if temp_order.geo_check:
        GeoPositionOrder.create(order_id=order.item_id,
                                geo_pos=temp_order.geo_pos)
    else:
        AddressOrder.create(order_id=order.item_id, geo_pos=temp_order.address)

    return order, temp_order.profile


@cancel_conversation
@back_conversation(next_state=ConversationStates.NUMBER)
def parse_profile_text(update, context):
    # collect data
    users_text = update.message.text

    if users_text == strings.txt_new_client:
        send_message(update, context,
                     message=strings.str_prompt_number_receiver,
                     reply=make_reply_keyboard(
                         [strings.txt_cancel, strings.txt_back]))
        return ConversationStates.NAME.name

    # if any_text
    else:
        send_message(update, context, message=strings.prompt_correct_data,
                     reply=make_reply_keyboard(
                         [strings.txt_cancel, strings.txt_back]))
        return ConversationStates.NUMBER.name


order_receive_handler_receiver_v2 = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_on_entry)],

    states={
        ConversationStates.NUMBER.name: [
            MessageHandler(Filters.text, checking_number)
        ],

        ConversationStates.PROFILE.name: [
            CallbackQueryHandler(
                parse_profile,
                pattern=f"^{CallbackEntry.select_users_profile.value}"
            ),
            MessageHandler(Filters.text, parse_profile_text)
        ],

        ConversationStates.NAME.name: [
            MessageHandler(Filters.text, parse_name)
        ],
        ConversationStates.SURNAME.name: [
            MessageHandler(Filters.text, parse_surname)
        ],
        ConversationStates.BRAND.name: [
            MessageHandler(Filters.text, parse_brand)
        ],
        ConversationStates.MODEL.name: [
            MessageHandler(Filters.text, parse_model)
        ],
        ConversationStates.IMEI.name: [
            MessageHandler(Filters.text, parse_imei)
        ],
        ConversationStates.TROUBLE.name: [
            MessageHandler(Filters.text, parse_trouble)
        ],
        ConversationStates.PRICE.name: [
            MessageHandler(Filters.text, parse_price)
        ],
        ConversationStates.PREPAYMENT.name: [
            MessageHandler(Filters.text, parse_prepayment)
        ],
        ConversationStates.COMMENT.name: [
            MessageHandler(Filters.text, parse_commentary)
        ],
        ConversationStates.SELECT_DELIVERY.name: [
            MessageHandler(Filters.text, parse_delivery_address),
            MessageHandler(Filters.location, parse_delivery_location)
        ],
        ConversationStates.LABELS_PRINT.name: [
            MessageHandler(Filters.text, parse_labels)
        ],
        ConversationStates.PRINT.name: [
            MessageHandler(Filters.text, parse_print)
        ],

    },
    fallbacks=fallback_handlers,
    allow_reentry=True
)
