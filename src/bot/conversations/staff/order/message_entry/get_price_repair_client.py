#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler

from src.db_manager import create_ticket
from src.db_manager import full_search_sqlite_in_orders
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, inline_kbd_ticket
from src.utils.cancel import cancel
from utils.message import send_message
from utils.message import send_message_inline
from config_bot import ADMIN_CHAT_ID

# conv_handler constants
entry_phrase = "Узнать стоимость ремонта"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

# states
TAKE_QUESTION = "question"
CALL_ADMIN = "call_admin"
SEND_MSG_ADMIN = "send_msg_admin"
OK = "ok"



def replace_substring_and(text):
    """
    Удаляет пробелы и добавляет AND для полнотекстового запроса в бд
    :param text: пользовательский ввод
    :return: форматированную строку
    """
    return text.replace(' ', ' AND ')


def fun_entry(update, context):
    message = "Отправьте название или модель аппарата "
    send_message(update, context, message=message, reply="remove")

    return TAKE_QUESTION


def bot_answer(update, context):
    users_text = replace_substring_and(update.message.text)
    data, columns = full_search_sqlite_in_orders(table='repair_price',
                                                 request=users_text)

    rows = []
    try:
        for row in data:
            rows.append(dict(zip(columns, row)))
    except Exception as e:
        raise e

    try:
        item_price = rows[0]['repair_price_id'] is None
    except IndexError:
        message = "Извините, ничего не нашла в таблице с готовыми ответами ☹️.\n" \
                  "Спросить у администратора?"
        buttons = [
            "Спросить у администратора",
            "Позвонить",
        ]
        reply_keyboard = make_reply_keyboard(buttons)
        send_message_inline(update, context,
                            message=message, reply=reply_keyboard)

        return CALL_ADMIN

    else:
        message = f"Цена работы:\n {item_price} "
        send_message_inline(update, context, message=message)

        return ConversationHandler.END


def fun_call_admin(update, context):
    """отправляет меседж одмену"""
    users_text = update.message.text.lower()

    if users_text == "спросить у администратора":
        # Отправка сообщения
        message = "Отправьте ваш вопрос"
        send_message(update, context,
                     message=message, reply='remove')

        return SEND_MSG_ADMIN

    elif users_text == "позвонить":
        # отправка сообщения
        message = "Номер для связи: +7(929)905-51-05"
        reply = hub_keyboard(update.message.chat_id)
        send_message_inline(update, context,
                            message=message, reply=reply)

        return ConversationHandler.END


def fun_send_msg_admin(update, context):
    """
        Формирует тикет и отправляет соотвествующее сообщение клиенту
    """
    # извлечение данных с апдейта
    users_text = update.message.text
    chat_id = update.message.chat_id
    # создаем тикет в бд
    ticket_id = create_ticket(theme=users_text, chat_id=chat_id, status="new")
    # формируем клавиатуру
    keyboard = inline_kbd_ticket(ticket_id=ticket_id)
    # отправка сообщение админу с этой клавиатурой
    context.bot.send_message(chat_id=ADMIN_CHAT_ID, text=users_text, reply_markup=keyboard)

    # возврат начальной клавиатуры
    hub = hub_keyboard(update.message.chat_id)
    # ответ юзеру
    message = "Сообщение отправлено администратору"
    send_message(update, context, message=message, reply=hub)

    return ConversationHandler.END


get_price_client_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_entry)],

    states={
        TAKE_QUESTION: [MessageHandler(Filters.text, bot_answer)],
        CALL_ADMIN: [MessageHandler(Filters.text, fun_call_admin)],
        SEND_MSG_ADMIN: [MessageHandler(Filters.text, fun_send_msg_admin)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=True
)
