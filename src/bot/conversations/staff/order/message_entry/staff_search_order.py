from enum import Enum

from telegram.ext import ConversationHandler, MessageHandler, Filters

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.MessageBuilder.SearchOrderInline import SearchOrdersInline
from src.bot.entry_points.MessageEntry import EntryPoints
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import SearchQuery, Client
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message_enum, cancel_conversation, send_message


# ask for query
# send data
# await new query until cancel

class ConversationStates(Enum):
    ASK_QUERY = {"txt": strings.prompt_search,
                 "buttons": [
                     strings.txt_cancel,
                 ],
                 }

    SEARCHING_QUERY = {"txt": strings.prompt_search,
                       "buttons": [
                           strings.txt_cancel,
                       ],
                       }
    ASK_AGAIN = {"txt": strings.prompt_search_again,
                 "buttons": [
                     strings.txt_yes,
                     strings.txt_no,
                     strings.txt_cancel,
                 ],
                 }


@access_level_wrapper(staff_status=StaffLevel.receiver)
def fun_on_entry(update, context):
    """
        Входная функция

    :param update:
    :param context:
    :return:
    """
    send_message_enum(update, context, enum_state=ConversationStates.ASK_QUERY)

    return ConversationStates.SEARCHING_QUERY.name


@cancel_conversation
def parse_query(update, context):
    text = update.message.text
    chat_id = update.message.chat_id

    search_query = SearchQuery.create(
        client=Client.get(Client.chat_id == chat_id),
        query_text=text,
    )
    markup = SearchOrdersInline(search_query=search_query)

    if markup.items.count() > 0:
        send_message(update, context, message=f"{strings.txt_search_result} {text}",
                     reply=markup.paginator.markup)
        send_message(update, context, message=strings.main_menu,
                     reply=hub_keyboard(chat_id))
        state = ConversationHandler.END

    else:
        send_message_enum(update, context, enum_state=ConversationStates.ASK_AGAIN)
        state = ConversationStates.ASK_AGAIN.name

    return state


@cancel_conversation
def parse_ask_again(update, context):
    chat_id = update.message.chat_id
    users_text = update.message.text

    match users_text:
        case strings.txt_yes:
            send_message_enum(update, context, enum_state=ConversationStates.ASK_QUERY)
            state = ConversationStates.ASK_QUERY.name
        case strings.txt_no:
            send_message(update, context, message=strings.main_menu, reply=hub_keyboard(chat_id=chat_id))
            state = ConversationHandler.END
        case _:
            send_message_enum(update, context, enum_state=ConversationStates.ASK_AGAIN)
            state = ConversationStates.ASK_AGAIN.name

    return state


staff_search_orders = ConversationHandler(
    entry_points=[
        MessageHandler(
            Filters.regex(f"^{EntryPoints.staff_search_orders.value}$"),
            fun_on_entry)],

    states={

        ConversationStates.ASK_QUERY.name: [
            MessageHandler(Filters.text, fun_on_entry)
        ],
        ConversationStates.SEARCHING_QUERY.name: [
            MessageHandler(Filters.text, parse_query)
        ],
        ConversationStates.ASK_AGAIN.name: [
            MessageHandler(Filters.text, parse_ask_again)
        ],
    },
    fallbacks=fallback_handlers,
    allow_reentry=True,
    name="search_orders",
)
