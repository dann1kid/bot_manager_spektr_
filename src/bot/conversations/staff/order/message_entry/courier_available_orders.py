# conv_handler constants

from telegram.ext import Filters, ConversationHandler, MessageHandler

from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.MessageBuilder.StatusOrdersInline import StatusOrdersInline
from src.bot.keyboards.TG_keyboards import hub_keyboard, make_reply_keyboard
from src.bot.models.pw_models import Order, Client
from src.bot.resources import strings
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message

entry_phrase = strings.btn_available_orders
ENTRY_POINT = Filters.regex(f"^{entry_phrase}")

# states
ON_SELECT = 'selecting'


def entry(update, context):
    chat_id = update.message.chat_id
    client = Client.get(Client.chat_id == chat_id)

    common_courier_statuses = [
        StatusOrderMenu.wcc,
        StatusOrderMenu.dwc,
    ]

    statuses_by_courier = [
        StatusOrderMenu.odw,
        StatusOrderMenu.dc,
        StatusOrderMenu.wc,
        StatusOrderMenu.wp,

    ]
    common_orders = {status.value: len(
        Order.select().where(Order.status == f"{status.name}").execute())
                     for status in common_courier_statuses}

    by_courier_orders = {status.value: len(
        Order.select().where((Order.status == f"{status.name}") & (Order.courier == client)).execute())
        for status in statuses_by_courier}

    buttons_common = [f"{status}: {count}" for status, count in common_orders.items()]
    buttons_courier = [f"{status}: {count}" for status, count in by_courier_orders.items()]

    buttons_common.extend(buttons_courier)

    buttons_common.append(strings.txt_cancel)

    send_message(update=update, context=context,
                 message=strings.txt_prompt_select_menu_enumerate,
                 reply=make_reply_keyboard(buttons_common))

    return ON_SELECT


def on_select(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id

    for status in StatusOrderMenu:
        if text.startswith(status.value):
            text = status.value

    try:
        status = StatusOrderMenu(text).name

    except (AttributeError, ValueError):
        send_message(update, context,
                     message=strings.txt_no_orders_with_selected_status,
                     reply=hub_keyboard(chat_id))
        return ConversationHandler.END

    statuses_by_courier = [
        StatusOrderMenu.odw,
        StatusOrderMenu.dc,
        StatusOrderMenu.wc,
        StatusOrderMenu.wp,

    ]

    markup = StatusOrdersInline(status=status, chat_id=chat_id)

    if markup.items.count() > 0:
        send_message(update, context, message=strings.txt_available_orders_status.format(StatusOrderMenu(text).value),
                     reply=markup.paginator.markup)
        send_message(update, context,
                     message=strings.main_menu,
                     reply=hub_keyboard(chat_id))

    else:
        send_message(update, context,
                     message=strings.txt_no_orders_with_selected_status,
                     reply=hub_keyboard(chat_id))

    return ConversationHandler.END


get_all_orders_courier_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, entry)],

    states={
        ON_SELECT: [MessageHandler(Filters.text, on_select)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
