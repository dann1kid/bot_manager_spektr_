import json
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import Memcache, MemorySubset
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Client, DetailOrder
from src.bot.resources import strings
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached, get_memcached
from src.bot.utils.message import send_message_inline, cancel_conversation, \
    send_message, only_digits


class ConvStates(Enum):
    wait_detail = 1
    wait_price = 2
    # TODO в колбеках хранить только метки.
    #  В редисе хранить значение для по энумератору
    # TODO перенести сохранение детали В КОНЕЦ


def entry_add_detail_order(update, context):
    # mark button pressed
    update.callback_query.answer()
    chat_id = update.callback_query.message.chat.id
    # confirm enter by sending message
    send_message_inline(update, context, message=strings.prompt_explain_detail,
                        reply=make_reply_keyboard([strings.txt_cancel]))
    # parse data

    data = json.loads(update.callback_query.data.split("-")[-1])
    # write selected order to params
    subset_key = Memcache(chat_id=chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    write_memcached(subset_key=subset_key, value=data["order"])

    return ConvStates.wait_detail.value


@cancel_conversation
def parse_name_detail(update, context):
    # collect data
    text = update.message.text
    subset_key = Memcache(chat_id=update.message.chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    order_id = get_memcached(subset_key=subset_key)
    client = Client.get(Client.chat_id == update.message.chat_id)
    # update db
    detail = DetailOrder.create(order_id=order_id, client=client, text=text)
    write_memcached(subset_key=subset_key, value=detail.item_id)
    # send next question

    send_message(update, context, message=strings.prompt_price_detail,
                 reply=make_reply_keyboard([strings.txt_cancel]))

    return ConvStates.wait_price.value


@cancel_conversation
@only_digits(next_state=ConvStates.wait_price)
def parse_detail_price(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id
    subset_key = Memcache(chat_id=update.message.chat_id,
                          subset=MemorySubset.new_detail_staff).get_subset()
    detail_id = get_memcached(subset_key=subset_key)
    # update db
    DetailOrder.update(price=text).where(
        DetailOrder.item_id == detail_id).execute()
    # send next question
    send_message(update, context, message=strings.txt_detail_added,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


handler_add_detail_order = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(entry_add_detail_order,
                             pattern=CallbackEntry.add_detail.value)],
    states={
        ConvStates.wait_detail.value: [
            MessageHandler(Filters.text, parse_name_detail)],
        ConvStates.wait_price.value: [
            MessageHandler(Filters.text, parse_detail_price)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
