# диалог для перечисления деталей для указанного заказа
import json

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    CommandHandler

from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.models.pw_models import DetailOrder
from src.bot.resources import strings
from src.bot.utils.cancel import cancel
from src.bot.utils.message import mono_inline_keyboard
from src.bot.utils.timestamp_parses import readable_data_from_datetime


def fun_on_entry(update, context):
    # в контроллерах обозначить контроллер заказа и при нажатии кнопки сущность
    # должна изменять обьект согласно логике
    # collect data
    data = json.loads(update.callback_query.data.split("-")[-1])
    button_data = update.callback_query.data.split("-")[-1]
    # get details by order
    details = DetailOrder.select().where(
        DetailOrder.order_id == int(data["order"])).execute()
    # send detail info one by one
    message = ''
    mark_approved = lambda a: strings.txt_yes if a else strings.txt_no
    for detail in details:
        message += f"\n{detail.text}.\n" \
                   f"Закупочная цена: {detail.price}\n" \
                   f"Цена для клиента: {detail.approved_price}\n" \
                   f"Согласована: {(mark_approved)(detail.mark_approved)}\n" \
                   f"Добавлена: {readable_data_from_datetime(detail.added)}\n"

    if message == "":
        message = strings.txt_no_details

    update.callback_query.message.edit_text(text=message)
    update.callback_query.edit_message_reply_markup(
        reply_markup=mono_inline_keyboard(text=strings.txt_back,
                                          callback_data=CallbackEntry.show_order.value +
                                                        button_data))

    return ConversationHandler.END


handler_enumerate_details_order = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.show_details_order.value)],
    states={
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=True
)
