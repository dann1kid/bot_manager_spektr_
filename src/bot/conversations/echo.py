from peewee import DoesNotExist
from telegram.ext import MessageHandler, Filters, ConversationHandler

from src.bot.keyboards.TG_keyboards import hub_keyboard, make_reply_keyboard
from src.bot.models.pw_models import Client
from src.bot.resources import strings
from src.bot.utils.message import send_message


def echo(update, context):
    """
        echo tree any context
    :param update:
    :param context:
    :return: None
    """

    chat_id = update.message.chat.id

    try:
        client = Client.get(Client.chat_id == chat_id)
    except DoesNotExist:
        send_message(update, context, message=strings.txt_error_to_start_prompt,
                     reply=make_reply_keyboard(
                         strings.btn_fulfill_profile_prompt))
    else:
        send_message(update, context, message=strings.main_menu,
                     reply=hub_keyboard(chat_id))
    return ConversationHandler.END


echo_handler = MessageHandler(Filters.all, echo)
