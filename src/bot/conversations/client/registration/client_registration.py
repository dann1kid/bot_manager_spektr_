import telegram
from telegram import ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import Filters, ConversationHandler, MessageHandler, \
    CommandHandler

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.keyboards.TG_keyboards import make_reply_keyboard, hub_keyboard
from src.bot.models.pw_models import Client, ContactClient
from src.bot.resources import strings
from src.bot.utils.contact_processing import InputNumber
from src.bot.utils.message import send_message

ENTRY_POINT = Filters.regex(f"^{strings.txt_fulfill_profile_prompt}$")

STATUS_AWAIT_NUMBER = "await_number"
STATUS_CHECKING_NUMBER = "check_number"
STATUS_CHECKING_NAME = "check_name"


def cancel(update: telegram.Update, context):
    print("context type", type(context))
    user = update.message.from_user
    update.message.reply_text(strings.txt_cancel,
                              reply_markup=make_reply_keyboard(
                                  strings.btn_fulfill_profile_prompt))

    return ConversationHandler.END


def awaiting_number(update, context) -> str:
    """ При нажатии кнопки поделиться отправляет сообщение
    пользователю, что у него есть варианты как поделится

    :type update: object
    :param context:

    :return STATUS: передает state для conversation handler

    """
    get_tg_contact_kbd = [[telegram.KeyboardButton(
        text=strings.txt_account_number, request_contact=True)]]
    reply = ReplyKeyboardMarkup(get_tg_contact_kbd)
    send_message(update, context, message=strings.prompt_number, reply=reply)

    return STATUS_CHECKING_NUMBER  # returns name of the next state


def fill_client(update, context, number):
    try:
        account_name = update.message.from_user.first_name
    except TypeError:
        send_message(update, context, message=strings.txt_prompt_name,
                     reply=ReplyKeyboardRemove())
        return STATUS_CHECKING_NAME
    client = Client.create(chat_id=update.message.chat_id)
    ContactClient().create(number=number.num_formatted, client_id=client)
    client.name = account_name
    client.current_state = strings.txt_status_registered
    client.staff_status = StaffLevel.client
    client.save()


def checking_text_number(update, context) -> str | int:
    """Проверяет номер в стейте и сохраняет его, используя модель"""

    # init
    number = update.message.text  # update text what can contains the contact
    chat_id = update.message.chat_id
    number = InputNumber(users_text=number)
    number.make_number()
    if number.num_parsed is None:
        update.message.reply_text(strings.txt_prompt_number_error,
                                  reply_markup=ReplyKeyboardRemove())
        return STATUS_CHECKING_NUMBER  # Goto send number again until it done

    else:
        fill_client(update, context, number)

    return ConversationHandler.END


def checking_contact_number(update, context) -> str | int:
    """Проверяет номер в стейте и сохраняет его, используя модель"""
    chat_id = update.message.chat_id
    number = update.effective_message.contact['phone_number']

    number = InputNumber(users_text=number)
    number.make_number()
    if not number.num_parsed:
        update.message.reply_text(strings.txt_prompt_number_error,
                                  reply_markup=ReplyKeyboardRemove())

    fill_client(update, context, number)
    send_message(update, context, message=strings.txt_success_profile,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


def checking_name(update, context) -> str | int:
    # init
    name = update.message.text
    chat_id = update.message.chat_id

    Client.update(name=name.capitalize(),
                  current_state=strings.txt_status_registered,
                  staff_status=strings.client,
                  ).where(Client.chat_id == chat_id).execute()
    send_message(update, context, message=strings.txt_success_profile)
    send_message(update, context, message=strings.main_menu,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


client_registration = ConversationHandler(

    entry_points=[MessageHandler(ENTRY_POINT, awaiting_number)],

    states={
        # alone state =(
        STATUS_CHECKING_NUMBER: [
            MessageHandler(Filters.text, checking_text_number),
            MessageHandler(Filters.contact, checking_contact_number)
        ],
        STATUS_CHECKING_NAME: [
            MessageHandler(Filters.text | Filters.contact, checking_name)],

    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=True
)
