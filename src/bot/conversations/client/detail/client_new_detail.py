from enum import Enum

from telegram.ext import Filters

from src.bot.entry_points.MessageEntry import EntryPoints
from src.bot.resources import strings, lists

entry_phrase = EntryPoints.client_new_detail.value
ENTRY_POINT = Filters.regex(f"^{entry_phrase}")


class ConvStates(Enum):
    STATUS_BRAND = {"txt": strings.txt_prompt_brand,
                    "buttons": lists.btn_default_brands}
    STATUS_MODEL = {"txt": strings.txt_prompt_model,
                    "buttons": [strings.txt_cancel, strings.txt_back]}
    STATUS_PROBLEM = {"txt": strings.prompt_trouble,
                      "buttons": lists.btn_default_trouble}
    STATUS_COLOR = {"txt": strings.prompt_color,
                    "buttons": lists.btn_default_colors}
    STATUS_PHOTO = {"txt": strings.txt_prompt_photo,
                    "buttons": [strings.txt_cancel, strings.txt_back]}
    STATUS_IMEI = {"txt": strings.txt_prompt_imei,
                   "buttons": [strings.txt_cancel, strings.txt_back]}