# смена статуса заказа и уточнение на тему адреса доставки у клиента
from enum import Enum

from telegram import KeyboardButton, ParseMode
from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    Filters, MessageHandler

from src.bot.Enumerators.Memcache import MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.MessageBuilder.OrderMessageBuilder import OrderMessageBuilder
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import GeoPositionOrder, Order, AddressOrder
from src.bot.resources import strings
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.load_json_callback import get_json_callback
from src.bot.utils.memcashed import write_memcached_item, get_memcached_item
from src.bot.utils.message import send_message_inline_enum, cancel_conversation, send_message, send_message_enum, \
    notify_staff, mono_inline_keyboard

memory_subset = MemorySubset.client_commit_order


# states
class CommitDialogStates(Enum):
    CONFIRM_DELIVERY = {"txt": strings.txt_confirm_order,
                        "buttons": [strings.txt_cancel]
                        }
    SELECT_DELIVERY = {"txt": strings.txt_prompt_delivery_client,
                       "buttons": [strings.txt_cancel, strings.txt_no,
                                   KeyboardButton(text=strings.btn_send_geo,
                                                  request_location=True),
                                   ]
                       }
    ADDRESS_APPROVED = {"txt": f"{strings.approve_order} \n "
                               f"{strings.wait_for_courier}",
                        "buttons": []
                        }


def fun_on_entry(update, context):
    chat_id = update.callback_query.from_user.id

    order_button_data = get_json_callback(update)
    order = Order.get(Order.item_id == order_button_data['order'])

    # case order not in valid state (wait decision client)
    if order.status != StatusOrderMenu.wdc.name:

        builder = OrderMessageBuilder(order=order,
                                      users_chat_id=chat_id,
                                      mode=OrderMessageBuilder.BuilderMode.status)
        update.callback_query.edit_message_text(
            text=builder.create_message(),
            reply_markup=mono_inline_keyboard(text=strings.note_order_committed,
                                              callback_data="1"), parse_mode=ParseMode.HTML)
        return ConversationHandler.END

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=order_button_data)
    update.callback_query.answer()
    send_message_inline_enum(update, context, enum_state=CommitDialogStates.SELECT_DELIVERY)

    return CommitDialogStates.SELECT_DELIVERY.name


@cancel_conversation
def parse_delivery_address(update, context):
    # collect data
    chat_id = update.message.chat_id
    users_text = update.message.text
    order_button_data = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)
    order = Order.get(Order.item_id == order_button_data['order'])

    # case user disagreed price return and decline order
    if users_text.lower() == strings.txt_no.lower():
        order.status = StatusOrderMenu.cld.name

        send_message(update, context,
                     message=f"{strings.approve_order}\n "
                             f"{strings.txt_store_address}",
                     reply=hub_keyboard(chat_id=chat_id))

    # case user sent address
    else:
        AddressOrder.create(order=order, address=users_text)
        order.address_check = True
        order.status = StatusOrderMenu.wcc.name

        send_message_enum(update, context, CommitDialogStates.ADDRESS_APPROVED)
        send_message(update, context, message=strings.note_order_committed,
                     reply=hub_keyboard(chat_id))

    order.save()

    return ConversationHandler.END


@cancel_conversation
def parse_delivery_location(update, context):
    # collect data
    chat_id = update.message.chat_id
    order_button_data = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)

    # order data update
    order = Order.get(Order.item_id == order_button_data['order'])
    order.status = StatusOrderMenu.wcc.name
    order.geo_check = True
    order.save()

    # save geo data
    geo = update.message.location
    GeoPositionOrder.create(order=order, geo_pos=geo)

    send_message_enum(update, context, CommitDialogStates.ADDRESS_APPROVED)
    send_message(update, context, message=strings.note_order_committed,
                 reply=hub_keyboard(chat_id))
    notify_staff(context=context, item=order,
                 staff_level=StaffLevel.receiver,
                 message=strings.note_new_order_from_client)
    notify_staff(context=context, item=order,
                 staff_level=StaffLevel.courier,
                 message=strings.note_new_order_from_client)

    return ConversationHandler.END


commit_order_conv_handler = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.client_commit_order.value)],
    states={
        CommitDialogStates.SELECT_DELIVERY.name: [
            MessageHandler(Filters.text, parse_delivery_address),
            MessageHandler(Filters.location, parse_delivery_location)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
