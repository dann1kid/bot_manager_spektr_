# conv_handler constants

from telegram.ext import Filters, ConversationHandler, MessageHandler

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.models.pw_models import Client
from src.bot.resources import strings
# entry points
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message

entry_phrase = strings.btn_help
ENTRY_POINT = Filters.regex(f"^{entry_phrase}")


# states

def entry(update, context):
    chat_id = update.message.chat_id
    client = Client.get(Client.chat_id == chat_id)
    match client.staff_status:
        case StaffLevel.client.value:
            send_message(update=update, context=context,
                         message=strings.txt_client_help,
                         )
        case _:
            ...
    return ConversationHandler.END


handler_help = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, entry)],

    states={
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
