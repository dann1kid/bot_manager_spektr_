from telegram import ParseMode
from telegram.ext import Filters, ConversationHandler, MessageHandler, \
    CommandHandler

from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.MessageBuilder.ClientOrderMessageBuilder import \
    ClientOrderMessageBuilder  # type: ignore
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Order
from src.bot.models.pw_models import Client
from src.bot.resources.strings import btn_active_orders as entry_phrase
from src.bot.resources.strings import txt_prompt_end_list
from src.bot.utils.cancel import back_to_main_menu
from src.bot.utils.message import send_message

# entry points

ENTRY_POINT = Filters.regex(f"^{entry_phrase}")

# states
ON_SELECT = 'selecting'


def entry(update, context):
    enumerate_active_orders(update, context)

    return ConversationHandler.END


def enumerate_active_orders(update, context):
    client = Client.select().where(Client.chat_id == update.message.chat_id)[0]
    orders = Order.select(). \
        where(Order.client_id == client). \
        where(Order.status != StatusOrderMenu.cld.name). \
        where(Order.status != StatusOrderMenu.fin.name)

    for order in orders:
        send_message(update, context,
                     message=ClientOrderMessageBuilder
                     (Order.get(Order.item_id == order)).create(),
                     parse_mode=ParseMode.HTML)

    send_message(update, context, message=txt_prompt_end_list,
                 reply=hub_keyboard(update.message.chat_id))


active_orders_clients_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, entry)],

    states={
    },
    fallbacks=[CommandHandler('cancel', back_to_main_menu)], allow_reentry=True
)
