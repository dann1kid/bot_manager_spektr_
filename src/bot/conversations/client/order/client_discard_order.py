# смена статуса заказа и уточнение на тему адреса доставки у клиента
import json
from enum import Enum

from telegram.ext import ConversationHandler, CallbackQueryHandler, \
    MessageHandler, Filters

from src.bot.Enumerators.Memcache import MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.entry_points.CallbackEntry import CallbackEntry
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.pw_models import Order
from src.bot.resources import strings
from src.bot.rights.checking import access_level_wrapper
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached_item, get_memcached_item
from src.bot.utils.message import send_message_inline_enum, cancel_conversation, send_message, notify_staff


class ConversationStates(Enum):
    CONFIRM = {"txt": strings.prt_are_you_sure,
               "buttons": [strings.txt_cancel, strings.txt_yes, strings.txt_no]}


memory_subset = MemorySubset.client_cancel_order


@access_level_wrapper(staff_status=StaffLevel.client)
def fun_on_entry(update, context):
    # get data
    chat_id = update.callback_query.from_user.id
    data = json.loads(update.callback_query.data.split("-")[-1])
    update.callback_query.answer()

    data = {'order_id': data["order"], "message_id": update.callback_query.message.message_id}
    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=data)

    send_message_inline_enum(update, context, enum_state=ConversationStates.CONFIRM)

    return ConversationStates.CONFIRM.name


@cancel_conversation
def cancel_order(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id

    item = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)
    context.bot.delete_message(chat_id=chat_id, message_id=item['message_id'])
    order = Order.get(Order.item_id == item['order_id'])

    if text != strings.txt_yes:
        send_message(update, context, message=strings.txt_order_was_not_cancelled.format(order.item_id),
                     reply=hub_keyboard(chat_id))
        return ConversationHandler.END

    # checking valid states
    if order.status == StatusOrderMenu.wc.name:
        order.status = StatusOrderMenu.dc.name

    else:
        order.status = StatusOrderMenu.cld.name

    notify_staff(context=context, item=order,
                 staff_level=StaffLevel.courier,
                 message=strings.txt_order_cancelled_by_client.format(order.item_id))
    notify_staff(context=context, item=order,
                 staff_level=StaffLevel.receiver,
                 message=strings.txt_order_cancelled_by_client.format(order.item_id))

    order.save()
    send_message(update, context, message=strings.txt_order_cancelled,
                 reply=hub_keyboard(chat_id))

    return ConversationHandler.END


handle_cancel_order_by_client = ConversationHandler(
    entry_points=[
        CallbackQueryHandler(fun_on_entry,
                             pattern=CallbackEntry.client_discard_order.value)],
    states={
        ConversationStates.CONFIRM.name: [MessageHandler(Filters.text, cancel_order)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
