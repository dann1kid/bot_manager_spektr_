# conv_handler constants
from enum import Enum

from telegram.ext import Filters, ConversationHandler, MessageHandler

from src.bot.Enumerators.Memcache import MemorySubset
from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.config_bot import photo_downloads
from src.bot.entry_points.MessageEntry import EntryPoints
from src.bot.keyboards.TG_keyboards import hub_keyboard
from src.bot.models.memcaсhed_models import TempOrder
from src.bot.models.pw_models import Client, Order, Photo, Device
from src.bot.resources import strings, lists
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.memcashed import write_memcached_item, get_memcached_item
from src.bot.utils.message import send_message, cancel_conversation, \
    back_conversation, send_message_enum, notify_staff, skip_step

entry_phrase = EntryPoints.client_order_eval.value
ENTRY_POINT = Filters.regex(f"^{entry_phrase}")


class ConvStates(Enum):
    STATUS_BRAND = {"txt": strings.txt_prompt_brand,
                    "buttons": lists.btn_default_brands}
    STATUS_MODEL = {"txt": strings.txt_prompt_model,
                    "buttons": [strings.txt_cancel, strings.txt_back]}
    STATUS_PROBLEM = {"txt": strings.prompt_trouble,
                      "buttons": lists.btn_default_trouble}
    STATUS_COLOR = {"txt": strings.prompt_color,
                    "buttons": lists.btn_default_colors}
    STATUS_PHOTO = {"txt": strings.txt_prompt_photo,
                    "buttons": [strings.txt_cancel, strings.txt_back]}

    STATUS_ADD_PHOTO = {"txt": strings.txt_prompt_photo_add,
                        "buttons": [strings.txt_cancel, strings.txt_back, strings.btn_finish]}

    STATUS_IMEI = {"txt": strings.txt_prompt_imei,
                   "buttons": [strings.txt_cancel, strings.txt_btn_skip,
                               strings.txt_back]}

    # стоит ли оставлять предпросмотр заказа или нет...
    # так по сути этот завершающий стейт вовсе не нужен
    STATUS_CONTINUE = {"txt": strings.txt_continue_add_photo,
                       "buttons": [strings.btn_add_next_photo, strings.btn_finish,
                                   strings.txt_back]}


memory_subset = MemorySubset.new_order_client

# conversation_handler presets
answer_yes = Filters.regex("^Да$")
answer_no = Filters.regex("^Нет$")


@cancel_conversation
def brand_question(update, context):
    send_message_enum(update, context, ConvStates.STATUS_BRAND)

    return ConvStates.STATUS_BRAND.name


@cancel_conversation
@back_conversation(next_state=ConvStates.STATUS_BRAND)
def model_question(update, context) -> str:
    # собираем данные
    chat_id = update.message.chat_id
    users_text = update.message.text

    temp_order = TempOrder()
    temp_order.brand = users_text

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    # отправляем сообщение
    send_message_enum(update, context, ConvStates.STATUS_MODEL)

    return ConvStates.STATUS_MODEL.name


@cancel_conversation
@back_conversation(next_state=ConvStates.STATUS_MODEL)
def color_question(update, context) -> str:
    chat_id = update.message.chat_id
    users_text = update.message.text

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.model = users_text

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    send_message_enum(update, context, ConvStates.STATUS_COLOR)

    return ConvStates.STATUS_COLOR.name


@cancel_conversation
@back_conversation(next_state=ConvStates.STATUS_COLOR)
def problem_question(update, context) -> str:
    chat_id = update.message.chat_id
    users_text = update.message.text

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.color = users_text

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    send_message_enum(update, context, ConvStates.STATUS_PROBLEM)

    return ConvStates.STATUS_PROBLEM.name


@cancel_conversation
@back_conversation(next_state=ConvStates.STATUS_PROBLEM)
def imei_question(update, context) -> str:
    chat_id = update.message.chat_id
    users_text = update.message.text

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.trouble = users_text

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    send_message_enum(update, context, ConvStates.STATUS_IMEI, parse_mode="HTML")

    return ConvStates.STATUS_IMEI.name


@cancel_conversation
@back_conversation(next_state=ConvStates.STATUS_IMEI)
@skip_step(next_state=ConvStates.STATUS_PHOTO)
def request_photo(update, context) -> str:
    chat_id = update.message.chat_id
    users_text = update.message.text

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)
    temp_order.imei = users_text

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)

    send_message_enum(update, context, ConvStates.STATUS_PHOTO)

    return ConvStates.STATUS_PHOTO.name


# сначала задается вопрос о фото
# затем проверяется есть ли фото, если оно есть то добавляется в список. Если его нет то попросить прислать
# если фото пришло, то нужно задать вопрос, есть ли еще фото. и отправляется список кнопок.
# затем проверяется, если нажата кнопка "еще", то присылается сообщение, что жду фото
# затем идет вызов того же стейта что и после второго сохранения фото

# сначала статус photo, затем add photo, на обоих должен стоять валидатор parse_invalid_data,
# который вернет на первую часть получения фото, если их вообще нет, либо на вторую, в которой уже будет
# кнопка завершить  (и кнопка добавить еще фото)
# не забудь добавить ограничение на 5 фото
@cancel_conversation
def parse_photo(update, context):
    # первый степ, без проверки количества фото
    chat_id = update.message.chat_id
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)

    temp_order.photo_ids.append(update.message.photo[-1]['file_id'])

    write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                         value=temp_order)
    if not update.message.media_group_id:
        send_message_enum(update, context, ConvStates.STATUS_CONTINUE)

    return ConvStates.STATUS_CONTINUE.name


@cancel_conversation
def parse_invalid_data(update, context):
    # поместить функцию для отсеивания любой даты (с чат жипити возьми код) со статуса фото и адд фото
    chat_id = update.message.chat_id
    temp_order = get_memcached_item(chat_id=chat_id, memory_subset=memory_subset)

    send_message(update, context, message=strings.err_please_send_photo)
    photos_count = len(temp_order.photo_ids)
    if 0 > photos_count < 5:
        return ConvStates.STATUS_ADD_PHOTO.name
    elif photos_count >= 5:
        temp_order.photo_ids.append(update.message.photo[-1]['file_id'])
        write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                             value=temp_order)
        save_data(update, context)
        return ConversationHandler.END
    else:
        return ConvStates.STATUS_PHOTO.name


@cancel_conversation
def parse_continue(update, context):
    users_text = update.message.text

    if users_text == strings.btn_add_next_photo:
        send_message_enum(update, context, enum_state=ConvStates.STATUS_ADD_PHOTO)
        state = ConvStates.STATUS_ADD_PHOTO.name

    elif users_text == strings.btn_finish:
        save_data(update, context)
        state = ConversationHandler.END

    else:
        send_message(update, context, message=strings.err_not_recognize)
        send_message_enum(update, context, enum_state=ConvStates.STATUS_ADD_PHOTO)
        state = ConvStates.STATUS_ADD_PHOTO.name

    return state


def parse_add_photo(update, context):
    chat_id = update.message.chat_id
    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)

    photos_count = len(temp_order.photo_ids)
    if 0 < photos_count < 4:
        temp_order.photo_ids.append(update.message.photo[-1]['file_id'])
        write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                             value=temp_order)
        if not update.message.media_group_id:
            send_message_enum(update, context, enum_state=ConvStates.STATUS_ADD_PHOTO)

        state = ConvStates.STATUS_ADD_PHOTO.name

    else:
        temp_order.photo_ids.append(update.message.photo[-1]['file_id'])
        write_memcached_item(chat_id=chat_id, memory_subset=memory_subset,
                             value=temp_order)
        save_data(update, context)

        state = ConversationHandler.END

    return state


def save_data(update, context) -> str:
    chat_id = update.message.chat_id

    temp_order = get_memcached_item(chat_id=chat_id,
                                    memory_subset=memory_subset)

    order = Order.create(client_id=Client.get(Client.chat_id == chat_id),
                         trouble=temp_order.trouble,
                         status=StatusOrderMenu.oe.name,
                         creator=Client.get(Client.chat_id == chat_id),
                         )
    order.save()
    Device.create(order=order,
                  brand_name=temp_order.brand,
                  model_name=temp_order.model,
                  imei=temp_order.imei,
                  color=temp_order.color,
                  )

    send_message(update, context, message=strings.txt_prompt_order_finished,
                 reply=hub_keyboard(chat_id))

    for photo_id in temp_order.photo_ids:
        photo_file = context.bot.get_file(photo_id)
        photo_path = f"{photo_downloads}/{photo_id}.jpg"
        photo_file.download(photo_path)
        Photo.create(order_id=order, pic_path=photo_path, pic_id=photo_id)

    # выделяем всех приемщиков и отправляем им сообщение
    notify_staff(context=context, item=order,
                 staff_level=StaffLevel.receiver,
                 message=strings.note_new_order_to_eval
                 )

    return ConversationHandler.END  # type: ignore


order_receive_handler_client = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, brand_question)],
    # здесь выяснять какая роль у chat_id
    # здесь выставить выбор дерева для диалога (pre-states)
    states={
        # if status - > handler [filter | filter, function ]
        ConvStates.STATUS_BRAND.name: [
            MessageHandler(Filters.text, model_question)],
        ConvStates.STATUS_MODEL.name: [
            MessageHandler(Filters.text, color_question)],
        ConvStates.STATUS_COLOR.name: [
            MessageHandler(Filters.text, problem_question)],
        ConvStates.STATUS_PROBLEM.name: [
            MessageHandler(Filters.text, imei_question)],
        ConvStates.STATUS_IMEI.name: [
            MessageHandler(Filters.text, request_photo)],
        ConvStates.STATUS_PHOTO.name: [
            MessageHandler(Filters.photo, parse_photo, ), MessageHandler(~Filters.photo, parse_invalid_data), ],
        ConvStates.STATUS_CONTINUE.name: [MessageHandler(Filters.photo, parse_add_photo, ),
                                          MessageHandler(Filters.text, parse_continue)],
        ConvStates.STATUS_ADD_PHOTO.name: [
            MessageHandler(Filters.photo, parse_add_photo, ), MessageHandler(~Filters.photo, parse_continue), ],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
