# conv_handler constants

from telegram.ext import Filters, ConversationHandler, MessageHandler

from src.bot.Enumerators.StatusOrderMenu import StatusOrderMenu
from src.bot.MessageBuilder.StatusOrdersInline import StatusOrdersInline
from src.bot.keyboards.TG_keyboards import hub_keyboard, make_reply_keyboard
from src.bot.models.pw_models import Order, Client
from src.bot.resources import strings
# entry points
from src.bot.resources.strings import client_orders_menu as entry_phrase
from src.bot.utils.conversation_handlers_states_manager import fallback_handlers
from src.bot.utils.message import send_message, cancel_conversation

ENTRY_POINT = Filters.regex(f"^{entry_phrase}")

# states
REJECTED_ORDERS = 'rejected'
UNDER_EVALUATION = 'eval'
IN_WORK = 'in_work'
READY_TO_BACK_TO_CLIENT = 'ready_for_return_to_client'
FINISHED = 'finished'
ON_SELECT = 'selecting'


def entry(update, context):
    # выделяем все заказы с заданным статусом
    chat_id = update.message.chat_id
    client = Client.get(Client.chat_id == chat_id)
    orders_by_status = {status.value: len(Order.select().
                                          where((Order.status == f"{status.name}")
                                                & (Order.client == client)).execute())

                        for status in StatusOrderMenu}

    buttons = [f"{status}: {count}" for status, count in orders_by_status.items() if count > 0]
    buttons.append(strings.txt_cancel)
    send_message(update=update, context=context,
                 message=strings.txt_prompt_select_menu_enumerate,
                 reply=make_reply_keyboard(buttons))

    return ON_SELECT


@cancel_conversation
def on_select(update, context):
    # collect data
    text = update.message.text
    chat_id = update.message.chat_id

    for status in StatusOrderMenu:
        if text.startswith(status.value):
            text = status.value

    try:
        status = StatusOrderMenu(text).name

    except (AttributeError, ValueError):
        send_message(update, context,
                     message=strings.txt_no_orders_with_selected_status,
                     reply=hub_keyboard(chat_id))
        return ConversationHandler.END

    markup = StatusOrdersInline(status=status, chat_id=chat_id)
    print("status order inline markup", markup.paginator.markup)

    if markup.items.count() > 0:
        send_message(update, context, message=strings.txt_available_orders_status.format(StatusOrderMenu(text).value),
                     reply=markup.paginator.markup)
        send_message(update, context,
                     message=strings.main_menu,
                     reply=hub_keyboard(chat_id))

    else:
        send_message(update, context,
                     message=strings.txt_no_orders_with_selected_status,
                     reply=hub_keyboard(chat_id))

    return ConversationHandler.END


get_all_orders_clients_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, entry)],

    states={
        ON_SELECT: [MessageHandler(Filters.text, on_select)],
    },
    fallbacks=fallback_handlers, allow_reentry=True
)
