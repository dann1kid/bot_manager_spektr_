import json
from enum import Enum

from src.bot.entry_points.MessageEntry import EntryPoints


class CallbackEntry(Enum):
    # todo добавить депарсер для колбека
    """
    Я отмечаю тире как разделитель
    Синглтон для меток для входа в conversation handlers.
    Все метки должны быть UNIQUE (тобиш уникальны)!
    Вообще должны быть просто инты, но для отладки я оставил сокращения
    todo: добавить методы под каждый случай колбек энтри
    todo: поменять на числа
    """
    # info buttons
    # Order type
    paginator_page = 'pap-'  # todo везде убрать и заменить на paginator_page_order
    paginator_page_order = 'papo-'
    show_order = 'so-'
    back_show_order = 'b_s_o-'
    show_comments_order = 'sc-'
    paginator_page_parser_order = 'pp-'
    show_details_order = 'sdo-'
    show_pics_order = 'spo-'
    transmit_order_from_work_to_courier = 'toc-'
    courier_pickup_order_from_client = 'cpoc-'
    courier_cancel_pickup_order_from_client = 'ccpoc-'
    courier_get_order_from_client = 'cgoc-'
    courier_client_didnt_take_order = 'cdto-'
    transmit_order_to_receiver_from_courier = 'tor-'
    transmit_order_to_client_from_courier = 'ocfc-'
    courier_take_payment = 'ctp-'
    staff_print_labels = 'spl-'

    client_commit_order = 'cco-'
    client_discard_order = 'cdo-'

    show_order_search = 'srs-'
    back_show_order_search = 'bsos-'
    paginator_page_order_search = 'papos-'

    # Detail type (will be renamed later on something else...
    # delivery and order any stuff or smth)
    paginator_page_detail = 'papd-'
    show_detail = 'sd-'
    back_show_detail = 'b_s_d-'
    show_comments_detail = 'scd-'
    paginator_page_parser_detail = 'ppd-'
    show_details_detail = 'sdd-'
    delete_detail_detail = 'ddd-'
    extradite_detail_detail = 'edd-'

    show_detail_search = 'sds-'
    back_show_detail_search = 'bsds-'
    paginator_page_detail_search = 'papds-'

    # start new dialog
    # orders
    add_comment_order = 'adco-'
    change_price = 'cp-'
    staff_payment_order = 'rtpo-'
    staff_payment_detail = 'rtpd-'
    evaluate_order = 'evo-'
    select_delivery_type_order = "sdto-"
    extradite_order = 'exo-'
    add_service_order = 'aaso-'
    commit_order = 'cor-'  # принять заказ
    send_to_extradition_order = 'sexo-'

    # details
    add_comment_detail = 'adcd-'
    evaluate_detail = 'evd-'
    select_delivery_type_detail = "sdtd-"
    extradite_detail = 'exd-'
    add_detail = 'ad-'
    remove_detail_order = 'remdo-'
    mark_detail_as_ordered = 'mdao-'
    send_to_extradition_detail = 'sexd-'

    # other (inner in CH buttons etc.)
    select_users_profile = 'sup-'
    cancel_order = 'car-'  # отменить заказ todo check this EnPt

    # Users
    paginator_page_user = 'papu-'
    paginator_page_user_search = 'papus-'
    show_user = 'su-'
    show_user_search = 'sus-'
    back_show_user = 'bsu-'
    back_show_user_search = 'bsus-'
    change_user = 'chu-'

    info = [paginator_page,
            paginator_page_order,
            show_order,
            back_show_order,
            show_comments_order,
            paginator_page_parser_order,
            show_details_order,
            show_order_search,
            back_show_order_search,
            paginator_page_order_search,
            courier_take_payment,
            ]

    dialog = [transmit_order_from_work_to_courier,
              courier_pickup_order_from_client,
              courier_cancel_pickup_order_from_client,
              courier_client_didnt_take_order,
              client_commit_order,
              client_discard_order,
              mark_detail_as_ordered,
              add_comment_order,
              add_comment_detail,
              add_detail,
              change_price,
              staff_payment_order,
              staff_payment_detail,
              evaluate_order,
              extradite_order,
              send_to_extradition_order,
              send_to_extradition_detail,
              commit_order,
              add_service_order,
              remove_detail_order,
              evaluate_detail,
              select_delivery_type_detail,
              extradite_detail,
              select_delivery_type_order,
              delete_detail_detail,

              EntryPoints.staff_add_detail.value,
              EntryPoints.staff_add_order.value,
              EntryPoints.staff_enumerate_detail.value,
              EntryPoints.staff_enumerate_orders.value,
              EntryPoints.staff_send_goods_email.value,
              EntryPoints.staff_search_orders.value,
              EntryPoints.staff_search_detail.value,
              extradite_detail_detail

              ]
    other = [select_users_profile,
             cancel_order
             ]


# todo Заменить на статик методы, один хрен юзаю как набор функций
class CallbackParser:

    @staticmethod
    def paginator_gen_order_status(status=None):
        return f"{CallbackEntry.paginator_page_order.value}{status}-"

    @staticmethod
    def paginator_gen_order_search(query=None):
        return f"{CallbackEntry.paginator_page_order_search.value}{query}-"

    @staticmethod
    def paginator_gen_detail(status=None):
        return f"{CallbackEntry.paginator_page_detail.value}{status}-"

    @staticmethod
    def paginator_gen_detail_search(query=None):
        return f"{CallbackEntry.paginator_page_detail_search.value}{query}-"

    @staticmethod
    def userprofile_gen(cls, client_id=None):
        return f"{CallbackEntry.select_users_profile.value}{client_id}"

    @staticmethod
    def make_userprofile_callback(self, client_id=None):
        return f"{CallbackEntry.select_users_profile.value}{client_id}"

    @staticmethod
    def make_paginator_order(order_id, status, page, mode):
        callback_data = json.dumps(
            {'order': order_id,
             'status': status,
             'page': page,
             'mode': mode,
             }
        )
        return f'{CallbackEntry.show_order.value}{callback_data}'

    @staticmethod
    def make_paginator_order_search(order_id, search_id, page, mode):
        callback_data = json.dumps(
            {'order': order_id,
             'search_id': search_id,
             'page': page,
             'mode': mode,
             }
        )
        return f'{CallbackEntry.show_order_search.value}{callback_data}'

    @staticmethod
    def make_paginator_detail_search(detail_id, search_id, page, mode):
        callback_data = json.dumps(
            {'detail': detail_id,
             'search_id': search_id,
             'page': page,
             'mode': mode,
             }
        )
        return f'{CallbackEntry.show_detail_search.value}{callback_data}'

    @staticmethod
    def make_paginator_detail(detail_id, status, page, mode):
        callback_data = json.dumps(
            {'detail': detail_id,
             'status': status,
             'page': page,
             'm': mode,
             }
        )
        return f'{CallbackEntry.show_detail.value}{callback_data}'

    @staticmethod
    def parse_callback(callback_data=None):
        """Специальный метод для разделения вызовов
        по перечислителю."""

        callback_data = callback_data.split("-")
        print("CALLBACK_DATA", callback_data)

        match f"{callback_data[0]}-":
            case CallbackEntry.select_users_profile.value:

                return {"client_id": callback_data[1]}

            case CallbackEntry.paginator_page_order.value:

                return {"page": callback_data[2].replace("#", ""),
                        "status": callback_data[1]}

            case CallbackEntry.paginator_page_detail.value:

                return {"page": callback_data[2].replace("#", ""),
                        "status": callback_data[1]}

            case CallbackEntry.paginator_page_order_search.value:

                return {"page": callback_data[2].replace("#", ""),
                        "query": callback_data[1]}

            case CallbackEntry.paginator_page_detail_search.value:

                return {"page": callback_data[2].replace("#", ""),
                        "query": callback_data[1]}

            case CallbackEntry.paginator_page_user.value:

                return {"page": callback_data[2].replace("#", ""),
                        "query": callback_data[1]}

            case CallbackEntry.paginator_page_user_search.value:

                return {"page": callback_data[2].replace("#", ""),
                        "query": callback_data[1]}

    @staticmethod
    def paginator_gen_user():
        return f"{CallbackEntry.paginator_page_user.value}-"

    @staticmethod
    def paginator_gen_user_search(query=None):
        return f"{CallbackEntry.paginator_page_user_search.value}{query}-"

    @staticmethod
    def make_paginator_users(client_id, page, mode):
        callback_data = json.dumps(
            {'client': client_id,
             'page': page,
             'mode': mode,
             }
        )
        return f'{CallbackEntry.show_user.value}{callback_data}'

    @staticmethod
    def make_paginator_user_search(client_id, search_id, page, mode):
        callback_data = json.dumps(
            {'client': client_id,
             'search_id': search_id,
             'page': page,
             'mode': mode,
             }
        )
        return f'{CallbackEntry.show_user_search.value}{callback_data}'

    @staticmethod
    def is_new_dialog_button(update):
        """
        Показывает, принадлежит ли колбек к началу нового диалога
        """
        callback_entry = update.callback_query.data.split("-")[0]
        callback_entry = f"{callback_entry[0]}-"
        if callback_entry in CallbackEntry.info.value:
            return True
