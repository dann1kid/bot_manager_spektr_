from dataclasses import dataclass
from enum import Enum

from telegram.error import BadRequest

from src.bot.Enumerators.StaffLevel import StaffLevel
from src.bot.resources import strings


class EntryPoints(Enum):
    staff_add_order = strings.txt_add_order
    staff_add_detail = strings.txt_add_detail
    staff_enumerate_orders = strings.txt_available_orders
    staff_enumerate_detail = strings.txt_enumerate_detail
    staff_send_goods_email = strings.btn_reveal_goods_to_order
    client_order_detail = strings.txt_order_detail
    client_order_eval = strings.str_get_order_eval
    client_change_profile = strings.str_change_profile
    staff_search_orders = strings.txt_search_orders
    staff_search_detail = strings.txt_search_details
    client_enumerate_orders = strings.client_orders_menu
    client_enumerate_active_orders = strings.btn_active_orders
    courier_enumerate_orders = strings.btn_available_orders
    client_help = strings.btn_help
    admin_change_client_data = strings.btn_change_user_data
    err_buttons = strings.btn_err_buttons
    # todo добавить все необходимые элементы в меню клиента


@dataclass
class UserMenu:
    """Buttons builder for all types of clients"""

    def build_buttons(self, level: StaffLevel):
        try:
            match level:
                case StaffLevel.client.value:
                    return self.client_buttons()

                case StaffLevel.receiver.value:
                    return self.receiver_buttons()

                case StaffLevel.courier.value:
                    return self.courier_buttons()

                case StaffLevel.admin.value:
                    return self.admin_buttons()

                case _:
                    return self.error_buttons()

        except BadRequest:
            return self.error_buttons()

    @staticmethod
    def error_buttons():
        return [
            EntryPoints.err_buttons.value,
        ]

    @staticmethod
    def client_buttons():
        return [
            # EntryPoints.client_order_detail.value,
            EntryPoints.client_order_eval.value,
            EntryPoints.client_enumerate_active_orders.value,
            # EntryPoints.client_change_profile.value,
            EntryPoints.client_enumerate_orders.value,


            EntryPoints.client_help.value,
        ]

    @staticmethod
    def receiver_buttons():
        return [
            EntryPoints.staff_add_detail.value,
            EntryPoints.staff_add_order.value,
            EntryPoints.staff_enumerate_detail.value,
            EntryPoints.staff_enumerate_orders.value,
            EntryPoints.staff_search_detail.value,
            EntryPoints.staff_search_orders.value,

        ]

    @staticmethod
    def admin_buttons():
        return [
            EntryPoints.staff_add_detail.value,
            EntryPoints.staff_add_order.value,
            EntryPoints.staff_enumerate_detail.value,
            EntryPoints.staff_enumerate_orders.value,
            EntryPoints.staff_search_detail.value,
            EntryPoints.staff_search_orders.value,
            EntryPoints.staff_send_goods_email.value,
            EntryPoints.admin_change_client_data.value,
        ]

    @staticmethod
    def courier_buttons():
        return [
            EntryPoints.courier_enumerate_orders.value,
        ]
