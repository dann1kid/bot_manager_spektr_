:: set main interpreter path (if it contains specific packets)
set PYTHONPATH=;

:: deeplink for qr system
set TG_DEEPLINK=;
set TG_BOT_TOKEN=;
set TG_ADMIN_TEST=;
set TEST_MSG=Reconnecting;

::  db setup
set DB_NAME=telegram_spektr_bot;
set DB_HOST=127.0.0.1;
set DB_USER=postgres;
set DB_PASS=;
set DB_MEMCACHED=127.0.0.1;

:: auto email system
set EMAIL_AUTH=;
:: gmail pass token
set EMAIL_PASS=;

:: docs output system
set LOGS=;
set DEEPLINK_VAULT=;
set PRINTED_ORDERS_VAULT=;
set TEMPLATE_VAULT=;
set PRINTER_TEMPLATE=;
set EXCEL_OUT=;

:: webhook certs
set PEM=;
set KEY=;

:: network setup
set LISTENER_IP=0.0.0.0;
set WEBHOOK_IP=;
:: be sure port not bound already
set WEBHOOK_PORT=88;

:: run mode polling or webhook
set MODE=1;


start path_to_interpreter path_to_server/SERVER.py
