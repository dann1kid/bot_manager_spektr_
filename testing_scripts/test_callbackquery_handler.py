#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram.ext import ConversationHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler

from src import get_client_id
from src import get_clients_current_order
from src import save_current_status
from src.bot.keyboards import hub_keyboard
from src import Client, ContactClient, connector_mysql
from config_bot import DATABASE_TABLE_CLIENTS_NAME as table_clients
from config_bot import DATABASE_TABLE_ORDERS_NAME as table_orders
from config_bot import DB_NAME as db

# conv_handler constants

entry_phrase = "^Test callback"
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

# conv_handler states
WAIT_CALLBACK = 'wait_callback'

# conversation_handler presets
answer_yes = Filters.regex("^Да$")
answer_no = Filters.regex("^Нет$")


# funcs
def cancel(update, context, current_status):
    """
		Текущий заказ помечается как завершенный неоконченный.

	:param current_status: текущий статус
	:param context:
	:type update: object
	"""
    update.message.reply_text('Передумали? Возвращаем в начало...',
                              reply_markup=hub_keyboard)
    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    current_order = get_clients_current_order(db, table_orders, client_id,
                                              current_status)

    save_current_status(
        db,
        table_orders,
        client_id,
        order_id=current_order,
        current_status=CURRENT_STATUS_UNSUCCESSFUL_END,
    )

    return ConversationHandler.END


def fun_on_entry(update, context) -> str:
    """
    Отвечает пользователю началом диалога добавления заказа при соотвествующем триггере

    :param update:
    :type context: object
    :return status: str
    """
    # задать вопрос про номер
    chat_id = update.message.chat_id
    clients = Client.select().join(ContactClient).where(
        ContactClient.number == '56435646')
    print(clients.count())
    buttons = []

    button = [InlineKeyboardButton(
        text=f'Просто текст, не забудь перехватить колбек дату или встроенными инструментами'
             f'', callback_data=f"prompt", )]
    buttons.append(button)
    for client in clients:
        buttons.append([InlineKeyboardButton(text=f'{client.name}',
                                             callback_data=f"{client.item_id}_{chat_id}", )])
    print(buttons)
    keyboard = InlineKeyboardMarkup(buttons)
    update.message.reply_text(text="ыв", reply_markup=keyboard)
    connector_mysql.close()
    return WAIT_CALLBACK


def parse_profile(update, context) -> str:
    """
    десь поменять обработку вызова инлайна
    """
    # если номер есть у разных клиентов, то создать
    chat_id = update.callback_query.from_user.id
    pattern = "^[\d]{1,99}"
    client_id = re.search(pattern, update.callback_query.order).group()
    client = Client.get(Client.item_id == client_id)

    text = f"{update.callback_query.order}__{str(chat_id)}__selected user is {client.name}"
    update.callback_query.answer(text=text, show_alert=True)

    connector_mysql.close()
    return WAIT_CALLBACK


# questions


test_callback_handler_receiver = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_on_entry)],

    states={
        WAIT_CALLBACK: [CallbackQueryHandler(parse_profile)]
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=True
)
