def main():
    try:
        connector_mysql.create_tables()
    except Exception as e:
        raise e
    else:
        print("tables created")
    finally:
        connector_mysql.close()


if __name__ == '__main__':
    main()
